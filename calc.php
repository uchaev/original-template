<?php
/**
 * Template Name: Шаблон страницы калькулятора
 * @package wordpress
 * @subpackage origin
 * @since 1.0
 */
get_header() ?>
<script>
  $(document).ready(function () {
    Kashirovka('none')
    Tonirovka(0)
    $('#calc-button').click(function () {
      Parametri()
    })

    $('[js-window-input]').on('change', function () {
      param_door_close()
      findStv()
    })
  })
</script>

<div class="list-sections">
    <div class="list-sections__list">
        <div class="list-sections__list-item list-sections__list-item_calc">
            <div class="calc block">
                <div class="calc__wrapper block-wrapper">
                    <div js-calc-mobile class="calc__mobile"></div>
                    <div class="calc__major">
                        <div class="calc__title">Калькулятор</div>
                        <div class="calc__tabs">
                            <div class="calc__tabs-item calc__tabs-item_active">
                                <div class="calc__tabs-caption">
                                    Пластиковые окна
                                </div>
                            </div>
                            <div class="calc__tabs-item">
                                <div class="calc__tabs-caption">
                                    Остекление балкона
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="calc__minor">
                        <div class="calc__minor-wrapper">
                            <div class="calc__main">
                                <form js-calc type="get" action="#" class="calc__page">
                                    <div class="calc-group calc__group">
                                        <div class="calc-group__main">
                                            <div class="windows">
                                                <input id="tipInfo" type="text" value="1" hidden js-window-input>
                                                <div js-windows class="windows__list">
                                                    <div class="windows__list-item">
                                                        <div js-windows-item class="windows-item">
                                                            <div class="windows-item__side">
                                                                <div js-windows-img-main-box
                                                                     class="windows-item__img is-active">
                                                                    <div js-img data-id="1">
                                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/win1-1.jpg">
                                                                    </div>
                                                                </div>
                                                                <div class="windows-item__link"></div>
                                                            </div>
                                                            <div class="windows-item__main">
                                                                <div class="windows-item__list">
                                                                    <div class="windows-item__list-item">
                                                                        <div class="windows-item__list-item">
                                                                            <div js-windows-img-additional-box
                                                                                 class="windows-item__img is-active">
                                                                                <div js-img data-id="1">
                                                                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/win1-1.jpg">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="windows-item__list-item">
                                                                        <div js-windows-img-additional-box
                                                                             class="windows-item__img">
                                                                            <div js-img data-id="11">
                                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/win1-2.jpg">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="windows__list-item">
                                                        <div js-windows-item class="windows-item">
                                                            <div class="windows-item__side">
                                                                <div js-windows-img-main-box
                                                                     class="windows-item__img">
                                                                    <div js-img data-id="2">
                                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/win2-1.jpg">
                                                                    </div>
                                                                </div>
                                                                <div class="windows-item__link"></div>
                                                            </div>
                                                            <div class="windows-item__main windows-item__main_2">
                                                                <div class="windows-item__list">
                                                                    <div class="windows-item__list-item">
                                                                        <div js-windows-img-additional-box
                                                                             class="windows-item__img">
                                                                            <div js-img data-id="2">
                                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/win2-1.jpg">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="windows-item__list-item">
                                                                        <div js-windows-img-additional-box
                                                                             class="windows-item__img">
                                                                            <div js-img data-id="21">
                                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/win2-2.jpg">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="windows-item__list-item">
                                                                        <div js-windows-img-additional-box
                                                                             class="windows-item__img">
                                                                            <div js-img data-id="22">
                                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/win2-3.png">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="windows__list-item">
                                                        <div js-windows-item class="windows-item">
                                                            <div class="windows-item__side">
                                                                <div js-windows-img-main-box
                                                                     class="windows-item__img">
                                                                    <div js-img data-id="3">
                                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/win3-1.jpg">
                                                                    </div>
                                                                </div>
                                                                <div class="windows-item__link"></div>
                                                            </div>
                                                            <div class="windows-item__main windows-item__main_3">
                                                                <div class="windows-item__list">
                                                                    <div class="windows-item__list-item">
                                                                        <div js-windows-img-additional-box
                                                                             class="windows-item__img">
                                                                            <div js-img data-id="3">
                                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/win3-1.jpg">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="windows-item__list-item">
                                                                        <div js-windows-img-additional-box
                                                                             class="windows-item__img">
                                                                            <div js-img data-id="31">
                                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/win3-2.jpg">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="windows-item__list-item">
                                                                        <div js-windows-img-additional-box
                                                                             class="windows-item__img">
                                                                            <div js-img data-id="32">
                                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/win3-3.jpg">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="windows__list-item">
                                                        <div js-windows-item
                                                             class="windows-item windows-item_single">
                                                            <div class="windows-item__side">
                                                                <div js-windows-img-main-box
                                                                     class="windows-item__img">
                                                                    <div js-img data-id="45">
                                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/win4.jpg">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="calc-group calc__group">
                                        <div class="calc-group__side">
                                            <div class="calc-group__title">
                                                Тип окна
                                            </div>
                                        </div>
                                        <div class="calc-group__main">
                                            <div class="fields-group">

                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Ширина окна
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item fields-item_small">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <div class="field">
                                                                        <input js-width-window name="width" id="shirina" type="number"
                                                                               class="field__input"
                                                                               onchange="findStv()"
                                                                               required>
                                                                    </div>
                                                                </div>
                                                                <div class="fields-item__main">
                                                                    <div class="fields-item__caption">
                                                                        мм
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Высота окна
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item fields-item_small">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <div class="field">
                                                                        <input js-height-window name="height" id="visota" type="number"
                                                                               class="field__input"
                                                                               required>
                                                                    </div>
                                                                </div>
                                                                <div class="fields-item__main">
                                                                    <div class="fields-item__caption">
                                                                        мм
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div js-fields-size class="fields-error">
                                                    Ширина окна, высота окна – только цифры, больше 700 и меньше 2000
                                                </div>
                                                <div id="stvorka2-wrapepr" class="fields-group__item hidden">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Ширина створки
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item fields-item_small">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <div class="field">
                                                                        <input name="width_sash" id="stvorka2" type="number"
                                                                               class="field__input"
                                                                               required>
                                                                    </div>
                                                                </div>
                                                                <div class="fields-item__main">
                                                                    <div class="fields-item__caption">
                                                                        мм
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="bbglu1-wrapepr" class="fields-group__item hidden">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Высота окна "А"
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item fields-item_small">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <div class="field">
                                                                        <input id="bbglu1" type="number"
                                                                               class="field__input"
                                                                               required>
                                                                    </div>
                                                                </div>
                                                                <div class="fields-item__main">
                                                                    <div class="fields-item__caption">
                                                                        мм
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="calc-group calc__group">
                                        <div class="calc-group__side">
                                            <div class="calc-group__title">
                                                Параметры
                                            </div>
                                        </div>
                                        <div class="calc-group__main">
                                            <div class="fields-group">
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Профиль
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="profile" id="prof" class="fancy-select"
                                                                            onchange="MkSP(this.selectedIndex)">
                                                                        <option value="b">
                                                                            Ortex (3 камеры)
                                                                        </option>

                                                                        <option value="i">
                                                                            Ortex (5 камер)
                                                                        </option>

                                                                        <option value="d">
                                                                            Exprof Practica (3 камеры)
                                                                        </option>

                                                                        <option value="f">
                                                                            Exprof ProWin (3 камеры)
                                                                        </option>

                                                                        <option value="h">
                                                                            Exprof Profecta (5 камер)
                                                                        </option>
                                                                        <option value="g">
                                                                            Exprof Experta (6 камер)
                                                                        </option>
                                                                        <option value="c">
                                                                            Gealan 8000 (4 камеры)
                                                                        </option>
                                                                        <option value="a">
                                                                            Salamander StreamLine (5 камер)
                                                                        </option>
                                                                        <option value="e">
                                                                            Salamander Bluevolution (6 камер)
                                                                        </option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-right">
                                                        <a href="#" target="_blank" class="link link_blue">Подробнее
                                                            о профильных системах</a>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Стеклопакет
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="glass" id="spVal" class="fancy-select">
                                                                        <option value="32">32мм (3 стекла)</option>
                                                                        <option value="320">32мм ТОП (3 стекла)</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Кашировка
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="kaschieren" class="fancy-select"
                                                                            onchange="Kashirovka(this.options[this.selectedIndex].value)">
                                                                        <option value="none" selected>Нет</option>
                                                                        <option value="ul">С улицы</option>
                                                                        <option value="kv">В квартире</option>
                                                                        <option value="2st">С 2 сторон</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Тонировка
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="tinted" class="fancy-select"
                                                                            onchange="Tonirovka(this.options[this.selectedIndex].value)">
                                                                        <option value="0" selected>Нет</option>
                                                                        <option value="1">Да</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="calc-group calc__group calc__group_gap_small">
                                        <div class="calc-group__side">
                                            <div class="calc-group__title">
                                                Монтаж и отделка
                                            </div>
                                        </div>
                                        <div class="calc-group__main">
                                            <div class="fields-group">
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Монтаж
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="install" id="mont" class="fancy-select">
                                                                        <option value="0">
                                                                            Не требуется
                                                                        </option>
                                                                        <option value="1" selected>
                                                                            Требуется
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Подоконник
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="still" id="podok" class="fancy-select">
                                                                        <option value="0">
                                                                            Не требуется
                                                                        </option>
                                                                        <option value="1" selected>
                                                                            Требуется
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Москитная сетка
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="grid" id="setka" class="fancy-select">
                                                                        <option value="0">
                                                                            Не требуется
                                                                        </option>
                                                                        <option value="1" selected>
                                                                            Требуется
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Тип дома
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="type" id="otkos" class="fancy-select">
                                                                        <option value="0" selected>
                                                                            Панельный
                                                                        </option>
                                                                        <option value="1">
                                                                            Кирпичный
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Отделка откосов
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="decking" id="otkos_t" class="fancy-select">
                                                                        <option value="0">
                                                                            Не требуется
                                                                        </option>
                                                                        <option value="2">
                                                                            Сэндвич панель
                                                                        </option>
                                                                        <option value="1" selected>
                                                                            Стеновая панель
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Наружний слив
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="drain" id="sliv" class="fancy-select">
                                                                        <option value="0">
                                                                            Не требуется
                                                                        </option>
                                                                        <option value="120" selected>
                                                                            Оцинкованный
                                                                        </option>
                                                                        <option value="150">
                                                                            Белый полимер
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fields-group__item">
                                                    <div class="fields-group__item-left">
                                                        <div class="fields-group__title">
                                                            Уголок наружный
                                                        </div>
                                                    </div>
                                                    <div class="fields-group__item-middle">
                                                        <div class="fields-item">
                                                            <div class="fields-item__wrapper">
                                                                <div class="fields-item__side">
                                                                    <select name="corner" id="ugolok" class="fancy-select">
                                                                        <option value="0">
                                                                            Не требуется
                                                                        </option>
                                                                        <option value="1" selected>
                                                                            Требуется
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="calc-group calc__group">
                                        <div class="calc-group__side calc-group__side_center">
                                            <div js-calc-button class="calc-group__title calc-group__title_center">
                                                <button id="calc-button" class="button">
                                                        <span class="button__content">
                                                            <span class="button__title">Рассчитать стоимость</span>
                                                        </span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="calc-group__main">
                                            <div js-calc-total class="calc-total is-hidden">
                                                <div class="calc-total__wrapper">
                                                    <div class="calc-total__item">
                                                        <div class="calc-total__item-major">
                                                            <div class="calc-total__item-main">
                                                                <div class="calc-total__name">
                                                                    Изделие
                                                                </div>
                                                            </div>
                                                            <div class="calc-total__item-delimiter"></div>
                                                            <div class="calc-total__item-side">
                                                                <div class="calc-total__sum">
                                                                    <span js-result-product id="result">0</span> ₽
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="calc-total__item-minor">
                                                            <div class="calc-total__item-desc">
                                                                С учетом скидки <span id="discount-percent">15</span> %.
                                                                Ваша экономия <span id="discount-value">1500</span> ₽.
                                                                Максимальная скидка 20 %, распространяется на
                                                                изделие с монтажом и отделкой.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="calc-total__item">
                                                        <div class="calc-total__item-major">
                                                            <div class="calc-total__item-main">
                                                                <div class="calc-total__name">
                                                                    Монтаж
                                                                </div>
                                                            </div>
                                                            <div class="calc-total__item-delimiter"></div>
                                                            <div class="calc-total__item-side">
                                                                <div class="calc-total__sum">
                                                                    <span js-result-montag id="montCena">0</span> ₽
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="calc-total__item">
                                                        <div class="calc-total__item-major">
                                                            <div class="calc-total__item-main">
                                                                <div class="calc-total__name">
                                                                    Отделка
                                                                </div>
                                                            </div>
                                                            <div class="calc-total__item-delimiter"></div>
                                                            <div class="calc-total__item-side">
                                                                <div class="calc-total__sum">
                                                                    <span js-result-otdelka id="OTD">0</span> ₽
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="calc-total__item calc-total__item_total">
                                                        <div class="calc-total__item-major">
                                                            <div class="calc-total__item-main">
                                                                <div class="calc-total__name">
                                                                    Итого
                                                                </div>
                                                            </div>
                                                            <div class="calc-total__item-delimiter"></div>
                                                            <div class="calc-total__item-side">
                                                                <div class="calc-total__sum">
                                                                    <span js-result-product-total id="Cena">0</span> ₽
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="calc-total__item calc-total__item_action">
                                                        <div class="calc-total__action">
                                                            <div class="calc-total__action-main">
                                                                <button js-calc-button-add-order type="submit" class="button button_full">
                                                                        <span class="button__content">
                                                                            <span class="button__title">Добавить в заказ</span>
                                                                        </span>
                                                                </button>
                                                            </div>
                                                            <div class="calc-total__action-side">
                                                                <div class="calc-total__item-desc">
                                                                    Не является публичной офертой, точная
                                                                    стоимость будет обозначена менеджером с
                                                                    учетом актуальных условий и акций.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div js-calc-side class="calc__side">
                                <div js-calc-panel class="calc__panel">
                                    <div class="calc__panel-wrapper">
                                        <div class="calc__info calc__info_filled">
                                            <form js-calc-form data-mod="card" action="/wp-admin/admin-ajax.php?action=wp_send_order" type="post" class="calc-info">
                                                <div class="calc-info__wrapper">
                                                    <div class="calc-info__header">Ваш заказ</div>
                                                    <div class="calc-info__body">
                                                        <div js-calc-orders class="calc-info__orders"></div>
                                                        <div class="calc-info__total">
                                                            <div class="calc-info__total-wrapper">
                                                                <div class="calc-info__total-side">Итого</div>
                                                                <div class="calc-info__total-delimiter"></div>
                                                                <div class="calc-info__total-main"><span js-calc-orders-total></span> ₽</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="calc-info__footer">
                                                        <div class="calc-info__action">
                                                            <div class="calc-info__action-main">
                                                                <div class="calc-info__input">
                                                                    <div class="calc-info__input-side">
                                                                        Ваш телефон
                                                                    </div>
                                                                    <div class="calc-info__input-main">
                                                                        <label js-inputShadow class="field">
                                                                            <span class="field__error"></span>
                                                                            <input js-phone js-phonemask
                                                                                   data-is-inputmask="true" required
                                                                                   type="phone" name="phone"
                                                                                   class="field__input">

                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="calc-info__action-side">
                                                                <button type="submit" class="button button_full">
                                                                    <span class="button__content">
                                                                        <span class="button__title">
                                                                            Отправить заявку
                                                                        </span>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="calc-info__wrapper calc-info__wrapper_success">
                                                    <div class="calc-info__body">
                                                        <div class="calc-info__desc calc-info__desc_center">
                                                            Ваш заказ отправлен
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="calc__info calc__info_empty">
                                            <form js-calc-form data-mod="callback" action="/wp-admin/admin-ajax.php?action=wp_send_callback" type="post" class="calc-info">
                                                <div class="calc-info__wrapper">
                                                    <div class="calc-info__header">Ваш заказ</div>
                                                    <div class="calc-info__body">
                                                        <div class="calc-info__desc">
                                                            Вы еще ничего не выбрали. Сделайте расчет изделия и
                                                            добавьте его в заказ. Если возникли вопросы, оставьте
                                                            нам свой телефон:
                                                        </div>
                                                    </div>
                                                    <div class="calc-info__footer">
                                                        <div class="calc-info__action">
                                                            <div class="calc-info__action-main">
                                                                <div class="calc-info__input">
                                                                    <div class="calc-info__input-side">
                                                                        Ваш телефон
                                                                    </div>
                                                                    <div class="calc-info__input-main">
                                                                        <label js-inputShadow class="field">
                                                                            <span class="field__error"></span>
                                                                            <input js-phone js-phonemask
                                                                                   data-is-inputmask="true" required
                                                                                   type="phone" name="phone"
                                                                                   class="field__input">

                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="calc-info__action-side">
                                                                <button type="submit" class="button button_full">
                                                                    <span class="button__content">
                                                                        <span class="button__title">
                                                                            Заказать звонок
                                                                        </span>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="calc-info__wrapper calc-info__wrapper_success">
                                                    <div class="calc-info__body">
                                                        <div class="calc-info__desc calc-info__desc_center">
                                                            Наши менеджеры свяжутся с вами в ближайшее время
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input id="procentDilera" size="10" maxlength="2" value="0" type="hidden">

<input type="hidden" id="s5801" value="<?php echo get_field('s5801'); ?>">
<input type="hidden" id="s5802" value="<?php echo get_field('s5802'); ?>">
<input type="hidden" id="s5803" value="<?php echo get_field('s5803'); ?>">
<input type="hidden" id="s5832" value="<?php echo get_field('s5832'); ?>">

<input type="hidden" id="s5701" value="<?php echo get_field('s5701'); ?>">
<input type="hidden" id="s5702" value="<?php echo get_field('s5702'); ?>">
<input type="hidden" id="s5703" value="<?php echo get_field('s5703'); ?>">
<input type="hidden" id="s6740" value="<?php echo get_field('s6740'); ?>">

<input type="hidden" id="s250220" value="<?php echo get_field('s250220'); ?>">
<input type="hidden" id="s251020" value="<?php echo get_field('s251020'); ?>">
<input type="hidden" id="s252120" value="<?php echo get_field('s252120'); ?>">

<input type="hidden" id="s413717" value="<?php echo get_field('s413717'); ?>">

<input type="hidden" id="s170420" value="<?php echo get_field('s170420'); ?>">
<input type="hidden" id="s171020" value="<?php echo get_field('s171020'); ?>">
<input type="hidden" id="s172420" value="<?php echo get_field('s172420'); ?>">
<input type="hidden" id="s173415" value="<?php echo get_field('s173415'); ?>">

<input type="hidden" id="S35801" value="<?php echo get_field('S35801'); ?>">
<input type="hidden" id="S35802" value="<?php echo get_field('S35802'); ?>">
<input type="hidden" id="S35803" value="<?php echo get_field('S35803'); ?>">

<input type="hidden" id="PW35801" value="<?php echo get_field('PW35801'); ?>">
<input type="hidden" id="PW35802" value="<?php echo get_field('PW35802'); ?>">
<input type="hidden" id="PW35803" value="<?php echo get_field('PW35803'); ?>">

<input type="hidden" id="S57001" value="<?php echo get_field('S57001'); ?>">
<input type="hidden" id="S57002" value="<?php echo get_field('S57002'); ?>">
<input type="hidden" id="S57003" value="<?php echo get_field('S57003'); ?>">

<input type="hidden" id="s67001" value="<?php echo get_field('s67001'); ?>">
<input type="hidden" id="s67002" value="<?php echo get_field('s67002'); ?>">
<input type="hidden" id="s67003" value="<?php echo get_field('s67003'); ?>">

<input type="hidden" id="s57111" value="<?php echo get_field('s57111'); ?>">
<input type="hidden" id="s57102" value="<?php echo get_field('s57102'); ?>">
<input type="hidden" id="s57103" value="<?php echo get_field('s57103'); ?>">

<input type="hidden" id="s35817" value="<?php echo get_field('s35817'); ?>">
<input type="hidden" id="s35820" value="<?php echo get_field('s35820'); ?>">
<input type="hidden" id="s35815" value="<?php echo get_field('s35815'); ?>">
<input type="hidden" id="s57006" value="<?php echo get_field('s57006'); ?>">

<input type="hidden" id="r8029" value="<?php echo get_field('r8029'); ?>">
<input type="hidden" id="r8075" value="<?php echo get_field('r8075'); ?>">
<input type="hidden" id="r8044" value="<?php echo get_field('r8044'); ?>">

<input type="hidden" id="r714400" value="<?php echo get_field('r714400'); ?>">

<input type="hidden" id="s5832k" value="<?php echo get_field('s5832k'); ?>">
<input type="hidden" id="s413833k" value="<?php echo get_field('s413833k'); ?>">
<input type="hidden" id="s413717k" value="<?php echo get_field('s413717k'); ?>">
<input type="hidden" id="s35817k" value="<?php echo get_field('s35817k'); ?>">
<input type="hidden" id="s35820k" value="<?php echo get_field('s35820k'); ?>">
<input type="hidden" id="s35815k" value="<?php echo get_field('s35815k'); ?>">
<input type="hidden" id="s57006k" value="<?php echo get_field('s57006k'); ?>">
<input type="hidden" id="r714400k" value="<?php echo get_field('r714400k'); ?>">
<input type="hidden" id="s6740k" value="<?php echo get_field('s6740k'); ?>">

<input type="hidden" id="s35810" value="<?php echo get_field('s35810'); ?>">
<input type="hidden" id="s35816" value="<?php echo get_field('s35816'); ?>">
<input type="hidden" id="spa1" value="<?php echo get_field('spa1'); ?>">
<input type="hidden" id="spa2" value="<?php echo get_field('spa2'); ?>">
<input type="hidden" id="spa_soed1" value="<?php echo get_field('spa_soed1'); ?>">
<input type="hidden" id="spa_soed2" value="<?php echo get_field('spa_soed2'); ?>">

<input type="hidden" id="Arm203" value="<?php echo get_field('Arm203'); ?>">
<input type="hidden" id="Arm207" value="<?php echo get_field('Arm207'); ?>">
<input type="hidden" id="T30" value="<?php echo get_field('T30'); ?>">
<input type="hidden" id="T24" value="<?php echo get_field('T24'); ?>">
<input type="hidden" id="T41" value="<?php echo get_field('T41'); ?>">
<input type="hidden" id="s215120P" value="<?php echo get_field('s215120P'); ?>">
<input type="hidden" id="s455210P" value="<?php echo get_field('s455210P'); ?>">
<input type="hidden" id="s415020" value="<?php echo get_field('s415020'); ?>">

<input type="hidden" id="T40" value="<?php echo get_field('T40'); ?>">

<input type="hidden" id="s228" value="<?php echo get_field('s228'); ?>">
<input type="hidden" id="s255" value="<?php echo get_field('s255'); ?>">
<input type="hidden" id="Rehau865002" value="<?php echo get_field('Rehau865002'); ?>">
<input type="hidden" id="Veka254" value="<?php echo get_field('Veka254'); ?>">

<input type="hidden" id="s417022" value="<?php echo get_field('s417022'); ?>">
<input type="hidden" id="s218025" value="<?php echo get_field('s218025'); ?>">

<input type="hidden" id="s178020" value="<?php echo get_field('s178020'); ?>">
<input type="hidden" id="s178820" value="<?php echo get_field('s178820'); ?>">

<input type="hidden" id="s218823" value="<?php echo get_field('s218823'); ?>">
<input type="hidden" id="V35803" value="<?php echo get_field('V35803'); ?>">
<input type="hidden" id="V57103" value="<?php echo get_field('V57103'); ?>">

<input type="hidden" id="ss354187" value="<?php echo get_field('ss354187'); ?>">
<input type="hidden" id="falc1" value="<?php echo get_field('falc1'); ?>">
<input type="hidden" id="PS1M" value="<?php echo get_field('PS1M'); ?>">
<input type="hidden" id="PS70" value="<?php echo get_field('PS70'); ?>">
<input type="hidden" id="s117040" value="<?php echo get_field('s117040'); ?>">
<input type="hidden" id="s217040" value="<?php echo get_field('s217040'); ?>">
<input type="hidden" id="Riht28" value="<?php echo get_field('Riht28'); ?>">
<input type="hidden" id="Riht36" value="<?php echo get_field('Riht36'); ?>">
<input type="hidden" id="Riht40" value="<?php echo get_field('Riht40'); ?>">
<input type="hidden" id="Riht52" value="<?php echo get_field('Riht52'); ?>">

<input type="hidden" id="Sam3916" value="<?php echo get_field('Sam3916'); ?>">
<input type="hidden" id="Sam4225" value="<?php echo get_field('Sam4225'); ?>">
<input type="hidden" id="Sam3935" value="<?php echo get_field('Sam3935'); ?>">
<input type="hidden" id="Sam580" value="<?php echo get_field('Sam580'); ?>">
<input type="hidden" id="Sam440" value="<?php echo get_field('Sam440'); ?>">
<input type="hidden" id="us10" value="<?php echo get_field('us10'); ?>">

<input type="hidden" id="germ" value="<?php echo get_field('germ'); ?>">
<input type="hidden" id="butil" value="<?php echo get_field('butil'); ?>">

<input type="hidden" id="s3272680195" value="<?php echo get_field('s3272680195'); ?>">
<input type="hidden" id="s3272680095" value="<?php echo get_field('s3272680095'); ?>">
<input type="hidden" id="s3272680135" value="<?php echo get_field('s3272680135'); ?>">
<input type="hidden" id="s8403615" value="<?php echo get_field('s8403615'); ?>">
<input type="hidden" id="H115" value="<?php echo get_field('H115'); ?>">
<input type="hidden" id="H175" value="<?php echo get_field('H175'); ?>">
<input type="hidden" id="sito" value="<?php echo get_field('sito'); ?>">
<input type="hidden" id="ug10" value="<?php echo get_field('ug10'); ?>">
<input type="hidden" id="ug14" value="<?php echo get_field('ug14'); ?>">
<input type="hidden" id="ug16" value="<?php echo get_field('ug16'); ?>">

<input type="hidden" id="coeff" value="0.98"/>

<div id="scripts">
    <script src="<?php bloginfo('template_url'); ?>/assets/js/cenpolicy.js" type="text/javascript"></script>
</div>
<?php get_footer() ?>
