<?php
/**
 * Главная страница
 */
get_header(); ?>
    <div class="list-sections">
        <div class="list-sections__list">
            <?php $topSlider = get_field('main_slider'); ?>
            <?php if ($topSlider): ?>
                <div class="list-sections__list-item list-sections__list-item_billboards">
                    <div class="block">
                        <div class="block-wrapper">
                            <div js-billboards class="billboards billboards_single">
                                <div js-billboards-slider class="billboards__slider">
                                    <div js-billboards-slider-list class="billboards__slider-list">
                                        <?php foreach ($topSlider as $slide): ?>
                                            <div class="billboards__slider-item"
                                                 style="background-image: url('<?php echo $slide['main_slider_img']['url'] ?>')">
                                                <div class="billboards__slider-content">
                                                    <div class="billboards__slider-side">
                                                        <?php $title = $slide['main_slider_title']; ?>
                                                        <?php if ($title): ?>
                                                            <div class="billboards__slider-title">
                                                                <?php echo $title ?>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php $desc = $slide['main_slider_desc']; ?>
                                                        <?php if ($desc): ?>
                                                            <div class="billboards__slider-desc">
                                                                <?php echo $desc ?>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <?php $url = $slide['main_slider_url']; ?>
                                                    <?php if ($url): ?>
                                                        <div class="billboards__slider-main">
                                                            <div class="billboards__slider-action">
                                                                <a href="<?php echo $url; ?>"
                                                                   class="button button_res-xs_small">
                                                                    <span class="button__content">
                                                                        <span class="button__title">Узнать больше</span>
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="list-sections__list-item list-sections__list-item_categories">
                <div class="block">
                    <div class="block-wrapper">
                        <div class="list-cards list-cards_3x">
                            <div class="list-cards__wrapper">
                                <div class="list-cards__list">
                                    <?php $menu = get_field('main_menu'); ?>
                                    <?php
                                    $field = $menu['main_balkon'];
                                    $url = ($field != null ? $field : '#');
                                    ?>
                                    <div class="list-cards__list-item">
                                        <a href="<?php echo $url ?>" class="category-card"
                                           style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/cat-1.png')">
                                            <div class="category-card__wrapper">
                                                <div class="category-card__side">
                                                    <div class="category-card__title">
                                                        <div class="category-card__title-caption">
                                                            Остекление балконов
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                    $field = $menu['main_crystalli'];
                                    $url = ($field != null ? $field : '#');
                                    ?>
                                    <div class="list-cards__list-item">
                                        <a href="<?php echo $url ?>" class="category-card"
                                           style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/cat-2.png')">
                                            <div class="category-card__wrapper">
                                                <div class="category-card__side">
                                                    <div class="category-card__title">
                                                        <div class="category-card__title-caption">
                                                            Подоконник Crystallit
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="category-card__main">
                                                    <div class="category-card__badge category-card__badge_red">
                                                        <div class="category-card__badge-icon">★</div>
                                                        <div class="category-card__badge-caption">Эксклюзивный
                                                            дилер
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                    $field = $menu['main_salamander'];
                                    $url = ($field != null ? $field : '#');
                                    ?>
                                    <div class="list-cards__list-item">
                                        <a href="<?php echo $url ?>" class="category-card"
                                           style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/cat-3.png')">
                                            <div class="category-card__wrapper">
                                                <div class="category-card__side">
                                                    <div class="category-card__title">
                                                        <div class="category-card__title-caption">
                                                            Окна Salamander
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="category-card__main">
                                                    <div class="category-card__badge category-card__badge_green">
                                                        <div class="category-card__badge-icon">★</div>
                                                        <div class="category-card__badge-caption">Эксклюзивный
                                                            дилер
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $calcUrl = get_field('main_calc'); ?>
            <?php if ($calcUrl): ?>
                <div class="list-sections__list-item list-sections__list-item_calc">
                    <div class="calc-section block">
                        <div class="calc-section__wrapper block-wrapper">
                            <div class="calc-section__img"></div>
                            <div class="calc-section__side">
                                <div class="calc-section__title">
                                    Онлайн-калькулятор
                                </div>
                            </div>
                            <div class="calc-section__main">
                                <div class="calc-section__action">
                                    <a href="<?php echo $calcUrl ?>" class="button">
                                            <span class="button__content">
                                                <span class="button__title">
                                                    Рассчитать стоимость
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            $info = get_field('main_info');
            $infoTitle = $info['main_info_title'];
            $infoDesc = $info['main_info_desc'];
            ?>
            <?php if ($infoTitle || $infoDesc): ?>
                <div class="list-sections__list-item list-sections__list-item_intro-info">
                    <div class="info-cols info-cols_intro block">
                        <div class="info-cols__wrapper block-wrapper">
                            <div class="info-cols__list">
                                <?php if ($infoTitle): ?>
                                    <div class="info-cols__list-item">
                                        <div class="info-cols__title info-cols__title_max-width_small">
                                            <?php echo $infoTitle; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="info-cols__list-item">
                                    <?php if ($infoDesc): ?>
                                        <div class="info-cols__main">
                                            <div class="info-cols__desc">
                                                <?php echo $infoDesc ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="info-cols__side">
                                        <div class="list-info">
                                            <div class="list-info__list list-info__list_3x">
                                                <div class="list-info__list-item">
                                                    <div class="list-info__item">
                                                        <div class="list-info__item-main">
                                                            <div class="list-info__item-title">
                                                                100%
                                                            </div>
                                                        </div>
                                                        <div class="list-info__item-side">
                                                            <div class="list-info__item-desc">
                                                                соответствие ГОСТу
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-info__list-item">
                                                    <div class="list-info__item">
                                                        <div class="list-info__item-main">
                                                            <div class="list-info__item-title">
                                                                1500 м²
                                                            </div>
                                                        </div>
                                                        <div class="list-info__item-side">
                                                            <div class="list-info__item-desc">
                                                                площадь производства
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-info__list-item">
                                                    <div class="list-info__item">
                                                        <div class="list-info__item-main">
                                                            <div class="list-info__item-title">
                                                                72 часа
                                                            </div>
                                                        </div>
                                                        <div class="list-info__item-side">
                                                            <div class="list-info__item-desc">
                                                                срок изготовления заказа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php $bottomSlider = get_field('main_slider2'); ?>
            <?php if ($bottomSlider): ?>
                <div class="list-sections__list-item">
                    <div js-billboards class="billboards">
                        <div js-billboards-slider class="billboards__slider">
                            <div class="billboards__slider-additional block">
                                <div class="billboards__slider-box block-wrapper">
                                    <div js-billboards-slider-nav class="billboards__slider-nav"></div>
                                    <div js-billboards-slider-dots class="billboards__slider-dots"></div>
                                </div>
                            </div>
                            <div js-billboards-slider-list class="billboards__slider-list">
                                <?php foreach ($bottomSlider as $slide): ?>
                                    <div class="billboards__slider-item">
                                      <a  class="billboards__slider-img"
                                           style="background-image: url('<?php echo $slide['img']['url'] ?>')"> 
                                            <img src="<?php echo $slide['img']['url'] ?>">
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php get_footer(); ?>