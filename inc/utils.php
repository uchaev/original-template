<?php
/**
 * Получение хлебных крошек
 * @param int $id ID начального постав
 * @return array Хлебные крошки
 */
function origin_get_breadcrumbs($id)
{
    if (!$id) {
        return array();
    }
    $breadcrumbs = array();
    $post_id = $id;
    while (($post_id = wp_get_post_parent_id($post_id)) != null) {
        $post = get_post($post_id);
        $breadcrumbs[] = origin_create_breadcrumb(
            $post->post_title,
            get_permalink($post_id)
        );
    }
    return $breadcrumbs;
}

/**
 * Создание хлебной крошки
 * @param string $title Заголовок
 * @param string $url Ссылка
 * @return array Хлебная крошка
 */
function origin_create_breadcrumb($title, $url)
{
    return array(
        'title' => $title,
        'url' => $url,
    );
}

/**
 * Получение хлебных крошек для произвольного типа
 * @param int $id ID текущего поста
 * @return array Хлебные крошки
 */
function origin_get_breadcrumbs_custom($id)
{
    $parent_id = get_post_meta($id, '_wpcf_belongs_page_id')[0];
    if (!$parent_id) {
        return array();
    }
    $parent = get_post($parent_id);
    $breadcrumbs = array();
    $post_id = $parent_id;
    while (($post_id = wp_get_post_parent_id($post_id)) != null) {
        $post = get_post($post_id);
        $breadcrumbs[] = origin_create_breadcrumb(
            $post->post_title,
            get_permalink($post_id)
        );
    }
    $breadcrumbs[] = origin_create_breadcrumb(
        $parent->post_title,
        get_permalink($parent_id)
    );
    return $breadcrumbs;
}

/**
 * Получение верхнего меню 1-ого уровня
 * @return array|false Меню
 */
function origin_get_top_menu_level1()
{
    return wp_get_nav_menu_items(2);
}

/**
 * Получение верхнего меню 2-ого уровня
 * @return array|false Меню
 */
function origin_get_top_menu_level2()
{
    $items = wp_get_nav_menu_items(3);
    if (!$items) {
        return $items;
    }
    $result = [];
    foreach ($items as $item) {
        if ($item->menu_item_parent) {
            //2 уровень
            $result[$item->menu_item_parent]['childs'][] = $item;
        } else {
            //1 уровень
            $result[$item->ID] = array(
                'item' => $item,
                'childs' => array(),
            );
        }
    }
    return $result;
}

/**
 * Получение 1-ое нижнее меню
 * @return array|false Меню
 */
function origin_get_bottom_menu_level1()
{
    return wp_get_nav_menu_items(4);
}

/**
 * Получение 2-ое нижнее меню
 * @return array|false Меню
 */
function origin_get_bottom_menu_level2()
{
    return wp_get_nav_menu_items(5);
}

/**
 * Получение 3-ое нижнее меню
 * @return array|false Меню
 */
function origin_get_bottom_menu_level3()
{
    return wp_get_nav_menu_items(6);
}

/**
 * Получение 3-ое нижнее меню
 * @return array|false Меню
 */
function origin_get_bottom_menu_level4()
{
    return wp_get_nav_menu_items(7);
}

function origin_get_body_class()
{
    global $template;
    if (endsWith($template, 'index.php') || endsWith($template, 'landing.php')) {
        return '';
    }
    if (endsWith($template, 'simple.php')
        || endsWith($template, 'single-adverts.php')
        || endsWith($template, 'single-articles.php')
        || endsWith($template, 'index.php')
    ) {
        return 'is-header-gap-none';
    }
    return 'is-header-normal';
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}