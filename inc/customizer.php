<?php
/**
 * Регистрация секций
 * @param $wp_customize
 */
function wp_register_custom_settings($wp_customize)
{
    wp_create_contacts_section($wp_customize);
}

add_action('customize_register', 'wp_register_custom_settings');

/**
 * Создание новой секции
 * @param $wp_customize
 */
function wp_create_contacts_section($wp_customize)
{
    $wp_customize->add_section(
        'data_site_section',
        array(
            'title' => 'Контактная информация',
            'capability' => 'edit_theme_options',
            'description' => ''
        )
    );
    create_setting($wp_customize, 'data', 'phone', 'Телефон');
    create_setting($wp_customize, 'data', 'email', 'Email');
    create_setting($wp_customize, 'data', 'instagram', 'Instagram');
    create_setting($wp_customize, 'data', 'vk', 'Vk');
    create_setting($wp_customize, 'data', 'feedback_email', 'Отпавлять заявки на E-Mail');

    //Офисы в барнауле
    $wp_customize->add_section(
        'offices_site_section',
        array(
            'title' => 'Офисы в барнауле',
            'capability' => 'edit_theme_options',
            'description' => ''
        )
    );
    create_setting($wp_customize, 'offices', 'office_1_address', 'Адрес 1-ого офиса');
    create_setting($wp_customize, 'offices', 'office_1_main_phone', 'Главный телефон 1-ого офиса');
    create_setting($wp_customize, 'offices', 'office_1_addit_phone', 'Дополнительный телефон 1-ого офиса');
    create_setting($wp_customize, 'offices', 'office_1_position', 'Координаты 1-ого офиса');

    create_setting($wp_customize, 'offices', 'office_2_address', 'Адрес 2-ого офиса');
    create_setting($wp_customize, 'offices', 'office_2_main_phone', 'Главный телефон 2-ого офиса');
    create_setting($wp_customize, 'offices', 'office_2_addit_phone', 'Дополнительный телефон 2-ого офиса');
    create_setting($wp_customize, 'offices', 'office_2_position', 'Координаты 2-ого офиса');

    create_setting($wp_customize, 'offices', 'office_3_address', 'Адрес 3-ого офиса');
    create_setting($wp_customize, 'offices', 'office_3_main_phone', 'Главный телефон 3-ого офиса');
    create_setting($wp_customize, 'offices', 'office_3_addit_phone', 'Дополнительный телефон 3-ого офиса');
    create_setting($wp_customize, 'offices', 'office_3_position', 'Координаты 3-ого офиса');
}

/**
 * Создание новой настройки в секции
 * @param $wp_customize
 * @param $section Секция
 * @param $name Название настройки
 * @param $label Лейбл настройки
 */
function create_setting($wp_customize, $section, $name, $label)
{
    $wp_customize->add_setting(
        $name . '_input_setting',
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        $name . '_input_control',
        array(
            'type' => 'text',
            'label' => $label,
            'section' => $section . '_site_section',
            'settings' => $name . '_input_setting'
        )
    );
}


function origin_customizer_settings($wp_customize)
{
    $wp_customize->selective_refresh->add_partial('phone_input_setting', array(
        'selector' => '.phone_input_setting',
        'render_callback' => 'phone_input_setting_partial',
    ));

    $wp_customize->selective_refresh->add_partial('email_input_setting', array(
        'selector' => '.email_input_setting',
        'render_callback' => 'email_input_setting_partial',
    ));

    $wp_customize->selective_refresh->add_partial('instagram_input_setting', array(
        'selector' => '.instagram_input_setting',
        'render_callback' => 'instagram_input_setting_partial',
    ));

    $wp_customize->selective_refresh->add_partial('vk_input_setting', array(
        'selector' => '.vk_input_setting',
        'render_callback' => 'vk_input_setting_partial',
    ));

    $wp_customize->selective_refresh->add_partial('feedback_email_input_setting', array(
        'selector' => '.feedback_email_input_setting',
        'render_callback' => 'feedback_email_input_setting_partial',
    ));
}

add_action('customize_register', 'origin_customizer_settings');

function origin_phone_input_setting()
{
    return get_option('phone_input_setting');
}

function origin_phone_raw_input_setting()
{
    return str_replace('-', '', get_option('phone_input_setting'));
}

function origin_email_input_setting()
{
    return get_option('email_input_setting');
}

function origin_instagram_input_setting()
{
    return get_option('instagram_input_setting');
}

function origin_vk_input_setting()
{
    return get_option('vk_input_setting');
}

function origin_office1()
{
    return array(
        'address' => get_option('office_1_address_input_setting'),
        'phone_main' => get_option('office_1_main_phone_input_setting'),
        'phone_main_raw' => str_replace('-', '', get_option('office_1_main_phone_input_setting')),
        'phone_addit' => get_option('office_1_addit_phone_input_setting'),
        'phone_addit_raw' => str_replace('-', '', get_option('office_1_addit_phone_input_setting')),
    );
}

function origin_office2()
{
    return array(
        'address' => get_option('office_2_address_input_setting'),
        'phone_main' => get_option('office_2_main_phone_input_setting'),
        'phone_main_raw' => str_replace('-', '', get_option('office_2_main_phone_input_setting')),
        'phone_addit' => get_option('office_2_addit_phone_input_setting'),
        'phone_addit_raw' => str_replace('-', '', get_option('office_2_addit_phone_input_setting')),
    );
}

function origin_office3()
{
    return array(
        'address' => get_option('office_3_address_input_setting'),
        'phone_main' => get_option('office_3_main_phone_input_setting'),
        'phone_main_raw' => str_replace('-', '', get_option('office_3_main_phone_input_setting')),
        'phone_addit' => get_option('office_3_addit_phone_input_setting'),
        'phone_addit_raw' => str_replace('-', '', get_option('office_3_addit_phone_input_setting')),
    );
}

function origin_offices()
{
    return array(
        origin_office1(),
        origin_office2(),
        origin_office3(),
    );
}

function origin_office_positions()
{
    $position1 = get_option('office_1_position_input_setting');
    $position2 = get_option('office_2_position_input_setting');
    $position3 = get_option('office_3_position_input_setting');

    $result = '';
    if ($position1) {
        $result .= $position1;
        $result .= ';';
    }
    if ($position2) {
        $result .= $position2;
        $result .= ';';
    }
    if ($position3) {
        $result .= $position3;
    }
    return $result;
}