<?php

const PAGE_SIZE = 10;

/**
 * Получение списка статей
 * @param int $current Текущая страница
 * @return WP_Query
 */
function origin_get_articles($current)
{
    $args = array(
        'post_type' => 'articles',
        'post_status' => 'publish',
        'posts_per_page' => PAGE_SIZE,
        'orderby' => 'meta_value',
        'order' => 'DESC',
        'paged' => $current
    );
    return new WP_Query($args);
}

/**
 * Получение списка акций
 * @param int $current Текущая страница
 * @return WP_Query
 */
function origin_get_adverts($current)
{
    $args = array(
        'post_type' => 'adverts',
        'post_status' => 'publish',
        'posts_per_page' => PAGE_SIZE,
        'orderby' => 'meta_value',
        'order' => 'DESC',
        'paged' => $current
    );
    return new WP_Query($args);
}

/**
 * Получение списка проектов
 * @param $current
 * @return WP_Query
 */
function origin_get_projects($current, $type)
{
    $args = array(
        'post_type' => 'project',
        'post_status' => 'publish',
        'posts_per_page' => PAGE_SIZE,
        'orderby' => 'meta_value',
        'order' => 'DESC',
        'paged' => $current
    );
    if ($type) {
        $args['meta_key'] = 'wpcf-project_type';
        $args['meta_value'] = $type;
    }
    return new WP_Query($args);
}

/**
 * Получение списка отзывов
 * @return WP_Query
 */
function origin_get_feedbacks()
{
    $args = array(
        'post_type' => 'feedbacks',
        'post_status' => 'publish',
        'numberposts' => -1,
        'orderby' => 'meta_value',
        'order' => 'DESC',
    );
    return new WP_Query($args);
}