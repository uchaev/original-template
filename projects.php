<?php
/**
 * Template Name: Шаблон страницы проектов
 * @package wordpress
 * @subpackage origin
 * @since 1.0
 */
get_header() ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php $type = (get_query_var('type')) ? get_query_var('type') : 0; ?>
<div class="list-sections">
    <div class="list-sections__list">
        <div class="list-sections__list-item list-sections__list-item_project-card">
            <div class="projects block">
                <div class="projects__wrapper block-wrapper">
                    <div class="projects__title">
                        Выполненные проекты
                    </div>
                    <div class="projects__nav">
                        <div class="projects__nav-list">
                            <?php $url = get_permalink(); ?>
                            <div class="projects__nav-item <?php echo($type == 0 ? 'projects__nav-item_active' : '') ?>">
                                <a href="<?php echo $url ?>" class="projects__nav-link">Все проекты</a>
                            </div>
                            <div class="projects__nav-item <?php echo($type == 1 ? 'projects__nav-item_active' : '') ?>">
                                <a href="<?php echo $url ?>?type=1" class="projects__nav-link">Квартиры</a>
                            </div>
                            <div class="projects__nav-item <?php echo($type == 2 ? 'projects__nav-item_active' : '') ?>">
                                <a href="<?php echo $url ?>?type=2" class="projects__nav-link">Коммерческие объекты</a>
                            </div>
                            <div class="projects__nav-item <?php echo($type == 3 ? 'projects__nav-item_active' : '') ?>">
                                <a href="<?php echo $url ?>?type=3" class="projects__nav-link">Частные дома</a>
                            </div>
                        </div>
                    </div>
                    <?php $projects = origin_get_projects($paged, get_query_var('type')); ?>
                    <div class="projects__list projects__list_3x">
                        <?php while ($projects->have_posts()) : $projects->the_post(); ?>
                            <?php $selected = get_field('project_selected', get_the_ID()) ?>
                            <div class="projects__list-item">
                                <div class="projects__item">
                                    <div class="projects__item-main">
                                        <div class="projects__item-img">
                                            <?php $image = get_field('project_image', get_the_ID()) ?>

                                            <?php if ($image): ?>
                                                <a href="<?php echo $image['url'] ?>">
                                                    <img src="<?php echo $image['sizes']['large'] ?>"
                                                         alt="<?php echo $image['alt'] ?>"
                                                         title="<?php echo $image['title'] ?>"
                                                         width="385" height="240"/>
                                                </a>
                                            <?php else: ?>
                                                <img src="http://placehold.it/385x240" alt="">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="projects__item-side">
                                        <a href="<?php echo get_permalink(get_the_ID()); ?>"
                                           class="projects__item-link">
                                            <?php the_title(); ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile;
                        wp_reset_query(); ?>
                    </div>
                    <div class="pagination">
                        <div class="pagination__list">
                            <a href="#" class="prev page-numbers"></a>
                            <a href="#" class="page-numbers current">1</a>
                            <a href="#" class="page-numbers">2</a>
                            <a href="#" class="next page-numbers"></a>
                        </div>
                    </div>
                    <?php echo paginate_links(array(
                        'total' => $projects->max_num_pages
                    )); ?>
                </div>
            </div>
        </div>
        <div class="list-sections__list-item list-sections__list-item_const">
            <div class="box-constructor block">
                <div class="box-constructor__wrapper block-wrapper">
                    <div class="box-constructor__side">
                        <h1 class="box-constructor__title">
                            Конструктор окон
                        </h1>
                    </div>
                    <div class="box-constructor__main">
                        <div id=constructor></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>
