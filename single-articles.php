<?php
/**
 * Template Name: Шаблон страницы статьи
 * @package wordpress
 * @subpackage origin
 * @since 1.0
 */
get_header() ?>
<?php get_template_part('template-parts/content', 'simple'); ?>
<?php get_footer() ?>
