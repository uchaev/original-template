<?php
/**
 * Template Name: Шаблон страницы проекта
 * @package wordpress
 * @subpackage origin
 * @since 1.0
 */
get_header() ?>
<div class="list-sections">
    <div class="list-sections__list">
        <div class="list-sections__list-item list-sections__list-item_project-card">
            <div class="project block">
                <div class="project__wrapper block-wrapper">
                    <div class="project__major">
                        <?php get_template_part('template-parts/content', 'breadcrumbs-project'); ?>
                        <div class="project__title">
                            <?php the_title(); ?>
                        </div>
                        <div class="project__major-wrapper">
                            <div class="project__main">
                                <div js-gallery class="project__gallery">
                                    <?php $images = get_field('project_images'); ?>
                                    <?php if ($images): ?>
                                        <div class="project__gallery-main">
                                            <div js-gallery-main class="gallery-slider gallery-slider_main">
                                                <div js-gallery-main-list class="gallery-slider__list">
                                                    <?php $index = -1;
                                                    foreach ($images as $image): $index++; ?>
                                                        <div js-gallery-main-item data-index="<?php echo $index; ?>"
                                                             class="gallery-slider__item">
                                                            <a href="<?php echo $image['url']; ?>"
                                                               data-fancybox=1 class="gallery-slider__link">
                                                                <img src="<?php echo $image['url'] ?>"
                                                                     class="gallery-slider__img"
                                                                     alt="<?php echo $image['alt'] ?>"
                                                                     title="<?php echo $image['title'] ?>">
                                                            </a>
                                                        </div>
                                                    <?php endforeach ?>
                                                </div>
                                                <div class="gallery-slider__nav">
                                                    <div js-gallery-main-nav class="gallery-slider__nav-list"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="project__gallery-side">
                                            <div js-gallery-preview class="gallery-slider gallery-slider_previews">
                                                <div js-gallery-preview-list class="gallery-slider__list">
                                                    <?php $index = -1;
                                                    foreach ($images as $image): $index++; ?>
                                                        <div js-gallery-preview-item
                                                             data-index="<?php echo $index; ?>"
                                                             class="gallery-slider__item <?php echo($index == 0 ? 'is-active' : '') ?>">
                                                            <a href="<?php echo $image['url'] ?>" data-fancybox
                                                               class="gallery-slider__link">
                                                                <img src="<?php echo $image['sizes']['thumbnail'] ?>"
                                                                     class="gallery-slider__img"
                                                                     alt="<?php echo $image['alt'] ?>"
                                                                     title="<?php echo $image['title'] ?>">
                                                            </a>
                                                        </div>
                                                    <?php endforeach ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="project__side">
                                <div class="project__table">
                                    <?php $wares = get_field('wares'); ?>
                                    <?php $manufactoryTerm = get_field('manufactory_term'); ?>
                                    <?php $buildTerm = get_field('build_term'); ?>
                                    <?php if ($wares || $manufactoryTerm || $buildTerm): ?>
                                        <table>
                                            <tbody>
                                            <?php if ($wares): ?>
                                                <tr>
                                                    <td>Изготовленные изделия</td>
                                                    <td><?php echo $wares; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php if ($manufactoryTerm): ?>
                                                <tr>
                                                    <td>Срок изготовления заказа</td>
                                                    <td><?php echo $manufactoryTerm; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php if ($manufactoryTerm): ?>
                                                <tr>
                                                    <td>Срок монтажа</td>
                                                    <td><?php echo $buildTerm; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    <?php endif; ?>
                                </div>

                                <?php $productsBlock = get_field('products_block'); ?>
                                <?php if ($productsBlock['products_preset'] || $productsBlock['products_custom']): ?>
                                    <div class="project__table">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><b>Профильные системы</b></td>
                                                <td></td>
                                            </tr>
                                            <?php if ($productsBlock['products_preset']): ?>
                                                <?php foreach ($productsBlock['products_preset'] as $item): ?>
                                                    <tr>
                                                        <td>
                                                            <?php if ($item['url']): ?>
                                                                <a href="<?php echo $item['url'] ?>">
                                                                    <?php echo $item['product'] ?>
                                                                </a>
                                                            <?php else: ?>
                                                                <?php echo $item['product'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>

                                            <?php if ($productsBlock['products_custom']): ?>
                                                <?php foreach ($productsBlock['products_custom'] as $item): ?>
                                                    <tr>
                                                        <td>
                                                            <?php if ($item['url']): ?>
                                                                <a href="<?php echo $item['url'] ?>">
                                                                    <?php echo $item['product'] ?>
                                                                </a>
                                                            <?php else: ?>
                                                                <?php echo $item['product'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php endif; ?>
                                <?php $serviceBlock = get_field('services_block'); ?>
                                <?php if ($serviceBlock['services_preset'] || $serviceBlock['services_custom']): ?>
                                    <div class="project__table">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td><b>Услуги</b></td>
                                                <td></td>
                                            </tr>
                                            <?php if ($serviceBlock['services_preset']): ?>
                                                <?php foreach ($serviceBlock['services_preset'] as $item): ?>
                                                    <tr>
                                                        <td>
                                                            <?php if ($item['url']): ?>
                                                                <a href="<?php echo $item['url'] ?>">
                                                                    <?php echo $item['service'] ?>
                                                                </a>
                                                            <?php else: ?>
                                                                <?php echo $item['service'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <?php if ($serviceBlock['services_custom']): ?>
                                                <?php foreach ($serviceBlock['services_custom'] as $item): ?>
                                                    <tr>
                                                        <td>
                                                            <?php if ($item['url']): ?>
                                                                <a href="<?php echo $item['url'] ?>">
                                                                    <?php echo $item['service'] ?>
                                                                </a>
                                                            <?php else: ?>
                                                                <?php echo $item['service'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php endif; ?>

                                <div class="project__desc">
                                    <?php get_template_part('template-parts/content', 'content'); ?>
                                </div>
                                <div class="project__actions">
                                    <div class="project__action">
                                        <button class="button">
                                        <span class="button__content">
                                            <span class="button__title">
                                                Обсудить ваш проект
                                            </span>
                                        </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $projects = get_field('other_projects'); ?>
                    <?php if ($projects): ?>

                        <div class="project__minor">
                            <div class="projects">
                                <div class="projects__wrapper">
                                    <div class="projects__title projects__title_center">
                                        Другие проекты
                                    </div>
                                    <div class="projects__list projects__list_3x">
                                        <?php foreach ($projects as $project): ?>
                                            <div class="projects__list-item">
                                                <div class="projects__item">
                                                    <div class="projects__item-main">
                                                        <div class="projects__item-img">
                                                            <?php $image = get_field('project_image', $project['project']->ID) ?>
                                                            <?php if ($image): ?>
                                                                <img src="<?php echo $image['sizes']['large'] ?>"
                                                                     alt="<?php echo $image['alt'] ?>"
                                                                     title="<?php echo $image['title'] ?>"
                                                                     width="385" height="240"/>
                                                            <?php else: ?>
                                                                <img src="http://placehold.it/385x240" alt="">
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="projects__item-side">
                                                        <a href="<?php echo get_permalink($project['project']->ID); ?>"
                                                           class="projects__item-link">
                                                            <?php echo $project['project']->post_title; ?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>
