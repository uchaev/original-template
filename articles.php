<?php
/**
 * Template Name: Шаблон страницы статей
 * @package wordpress
 * @subpackage origin
 * @since 1.0
 */
get_header() ?>
<?php get_template_part('template-parts/content', 'breadcrumbs'); ?>
<br/>

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php $articles = origin_get_articles($paged); ?>
<?php while ($articles->have_posts()) : $articles->the_post(); ?>
    <?php the_title(); ?>
    <?php the_content('Читать полностью'); ?>
    <a href="<?php echo get_permalink(get_the_ID()); ?>">
        Читать полностью
    </a>
<?php endwhile;
wp_reset_query(); ?>
<br/>
<?php echo paginate_links(array(
    'total' => $articles->max_num_pages
)); ?>
<?php get_footer() ?>
