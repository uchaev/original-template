<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url'); ?>/assets/images/favicon.png">
    <title><?php echo get_bloginfo('name'); ?> | <?php the_title();?></title>
    <meta name="description" content="<?php echo get_bloginfo('description'); ?>"/>
    <meta name="title" content="<?php echo get_bloginfo('name'); ?>"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <script src="https://api-maps.yandex.ru/2.1/?apikey=7279ff52-8cf6-46ef-9f49-043703363ac3&lang=ru_RU"
            type="text/javascript">
    </script>
    <?php wp_head(); ?>
</head>
<body class="body <?php echo origin_get_body_class(); ?>">
<div class="body__wrapper">
    <div class="body__header">
        <div class="header block">
            <div class="header-desktop">
                <div class="header__wrapper block-wrapper">
                    <div class="header-top header__top">
                        <div class="header-top__wrapper">
                            <?php $topMenuLevel1 = origin_get_top_menu_level1(); ?>
                            <?php if ($topMenuLevel1): ?>
                                <div class="header-top__main">
                                    <div class="header-top__nav">
                                        <div class="header-top__nav-list">
                                            <?php
                                            $last = count($topMenuLevel1) - 1;
                                            $index = -1;
                                            ?>
                                            <?php foreach ($topMenuLevel1 as $item): $index++; ?>
                                                <div class="header-top__nav-item">
                                                    <a href="<?php echo $item->url; ?>" class="header-top__nav-link">
                                                        <?php if ($index == $last): ?>
                                                            <span class="icon"> ❖ </span>
                                                        <?php endif; ?>
                                                        <?php echo $item->title; ?>
                                                    </a>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php
                            $vk = origin_vk_input_setting();
                            $inst = origin_instagram_input_setting();
                            ?>
                            <?php if ($vk || $inst): ?>
                                <div class="header-top__side">
                                    <div class="social">
                                        <div class="social__wrapper">
                                            <div class="social__side">
                                                <div class="social__title">
                                                    НАШИ СОЦСЕТИ:
                                                </div>
                                            </div>
                                            <div class="social__main">
                                                <div class="social__list">
                                                    <?php if ($vk): ?>
                                                        <div class="social__list-item">
                                                            <a href="<?php echo $vk; ?>" target="_blank"
                                                               class="social-badge social-badge_vk"></a>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php if ($inst): ?>
                                                        <div class="social__list-item">
                                                            <a href="<?php echo $inst; ?>" target="_blank"
                                                               class="social-badge social-badge_instagram"></a>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="header-middle header__middle">
                        <div class="header-middle__wrapper">
                            <div class="header-middle__left">
                                <a href="/" class="header-middle__logo">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/logo.svg"
                                         class="header-middle__logo-img" alt="Логотип оригинал">
                                </a>
                            </div>
                            <div class="header-middle__middle">
                                <div js-toggle-box class="office-box">
                                    <div class="office-box__side">
                                        <a href="#" class="link link_large link_dashed">ОФИСЫ В БАРНАУЛЕ</a>
                                    </div>
                                    <div class="office-box__main">
                                        <div js-toggle-box-close class="office-box__close"></div>
                                        <div class="office-box__sections">
                                            <?php
                                            $offices = origin_offices();
                                            ?>
                                            <?php if ($offices): foreach ($offices as $office): ?>
                                                <div class="office-box__section">
                                                    <?php echo $office['address'] ?>
                                                    <?php if ($office['phone_main']): ?>
                                                        <a href="tel:83852<?php echo $office['phone_main_raw']; ?>">
                                                            (3852)&nbsp;<?php echo $office['phone_main']; ?>
                                                        </a>
                                                    <?php endif; ?>
                                                    <?php if ($office['phone_addit']): ?>
                                                        <a href="tel:83852<?php echo $office['phone_addit_raw']; ?>">
                                                            (3852)&nbsp;<?php echo $office['phone_addit']; ?>
                                                        </a>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach; endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header-middle__right">
                                <div class="header-middle__actions">
                                    <div class="header-middle__actions-item">
                                        <a href="tel:83852<?php echo origin_phone_raw_input_setting(); ?>"
                                           class="header-middle__phone">
                                            <sup class="code">(3852)</sup>
                                            <?php echo origin_phone_input_setting(); ?>
                                        </a>
                                    </div>
                                    <div class="header-middle__action-item">
                                        <div class="header-middle__callback">
                                            <button js-question-button class="button button_small">
                                                <span class="button__content">
                                                    <span class="button__title">
                                                        Задать вопрос
                                                    </span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-bottom header__bottom">
                        <div class="header-bottom__wrapper">
                            <div class="header-bottom__nav">
                                <div class="header-bottom__nav-list">
                                    <?php $topMenuLevel2 = origin_get_top_menu_level2(); ?>
                                    <?php if ($topMenuLevel2): foreach ($topMenuLevel2 as $item): ?>
                                        <?php if ($item['childs']): ?>
                                            <div class="header-bottom__toggle">
                                                <div class="header-bottom__toggle-side">
                                                    <?php echo $item['item']->title; ?>
                                                </div>
                                                <div class="header-bottom__toggle-main">
                                                    <div class="header-bottom__toggle-list">
                                                        <?php foreach ($item['childs'] as $child): ?>
                                                            <div class="header-bottom__toggle-item">
                                                                <a href="<?php echo $child->url; ?>"
                                                                   class="header-bottom__toggle-link">
                                                                    <?php echo $child->title; ?>
                                                                </a>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php else: ?>
                                            <div class="header-bottom__nav-item">
                                                <a href="<?php echo $item['item']->url; ?>"
                                                   class="header-bottom__nav-link">
                                                    <?php echo $item['item']->title; ?>
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-mobile">
                <div class="header-mobile__wrapper">
                    <div class="header-mobile__main">
                        <a href="/" class="header-mobile__logo">
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/logo.svg"
                                 class="header-mobile__logo-img" alt="Логотип оригинал">
                        </a>
                    </div>
                    <div class="header-mobile__side">
                        <div class="header-mobile__actions">
                            <div class="header-mobile__actions-item">
                                <div js-office-addresses class="office-box">
                                    <div class="office-box__side">
                                        <a href="#" class="link link_small link_dashed">ОФИСЫ В БАРНАУЛЕ</a>
                                    </div>
                                    <div class="office-box__main">
                                        <div js-office-addresses-close class="office-box__close"></div>
                                        <div class="office-box__sections">
                                            <?php
                                            $offices = origin_offices();
                                            ?>
                                            <?php if ($offices): foreach ($offices as $office): ?>
                                                <div class="office-box__section">
                                                    <?php echo $office['address'] ?>
                                                    <?php if ($office['phone_main']): ?>
                                                        <a href="tel:83852<?php echo $office['phone_main_raw']; ?>">
                                                            (38-52)&nbsp;<?php echo $office['phone_main']; ?>
                                                        </a>
                                                    <?php endif; ?>
                                                    <?php if ($office['phone_addit']): ?>
                                                        <a href="tel:83852<?php echo $office['phone_addit_raw']; ?>">
                                                            (38-52)&nbsp;<?php echo $office['phone_addit']; ?>
                                                        </a>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach; endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header-mobile__actions-item">
                                <a href="tel:83852<?php echo origin_phone_raw_input_setting(); ?>"
                                   class="header-mobile__phone">
                                </a>
                            </div>
                            <div class="header-mobile__actions-item">
                                <div js-header-menu class="header-mobile__menu"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="body__content">
