(function (t) {
  function s (s) {
    for (var c, r, e = s[0], a = s[1], u = s[2], _ = 0, d = []; _ < e.length; _++) r = e[_], i[r] && d.push(i[r][0]), i[r] = 0
    for (c in a) Object.prototype.hasOwnProperty.call(a, c) && (t[c] = a[c])
    l && l(s)
    while (d.length) d.shift()()
    return n.push.apply(n, u || []), o()
  }

  function o () {
    for (var t, s = 0; s < n.length; s++) {
      for (var o = n[s], c = !0, e = 1; e < o.length; e++) {
        var a = o[e]
        0 !== i[a] && (c = !1)
      }
      c && (n.splice(s--, 1), t = r(r.s = o[0]))
    }
    return t
  }

  var c = {}, i = { app: 0 }, n = []

  function r (s) {
    if (c[s]) return c[s].exports
    var o = c[s] = { i: s, l: !1, exports: {} }
    return t[s].call(o.exports, o, o.exports, r), o.l = !0, o.exports
  }

  r.m = t, r.c = c, r.d = function (t, s, o) {
    r.o(t, s) || Object.defineProperty(t, s, {
      enumerable: !0,
      get: o
    })
  }, r.r = function (t) {'undefined' !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: 'Module' }), Object.defineProperty(t, '__esModule', { value: !0 })}, r.t = function (t, s) {
    if (1 & s && (t = r(t)), 8 & s) return t
    if (4 & s && 'object' === typeof t && t && t.__esModule) return t
    var o = Object.create(null)
    if (r.r(o), Object.defineProperty(o, 'default', { enumerable: !0, value: t }), 2 & s && 'string' != typeof t) for (var c in t) r.d(o, c, function (s) {return t[s]}.bind(null, c))
    return o
  }, r.n = function (t) {
    var s = t && t.__esModule ? function () {return t['default']} : function () {return t}
    return r.d(s, 'a', s), s
  }, r.o = function (t, s) {return Object.prototype.hasOwnProperty.call(t, s)}, r.p = '/'
  var e = window['webpackJsonp'] = window['webpackJsonp'] || [], a = e.push.bind(e)
  e.push = s, e = e.slice()
  for (var u = 0; u < e.length; u++) s(e[u])
  var l = a
  n.push([0, 'chunk-vendors']), o()
})({
  0: function (t, s, o) {t.exports = o('56d7')}, '034f': function (t, s, o) {
    'use strict'
    var c = o('64a9'), i = o.n(c)
    i.a
  }, '1c0b': function (t, s, o) {}, '56d7': function (t, s, o) {
    'use strict'
    o.r(s)
    o('cadf'), o('551c'), o('097d')
    var c = o('2b0e'), i = function () {
      var t = this, s = t.$createElement, o = t._self._c || s
      return o('Constructor')
    }, n = [], r = function () {
      var t = this, s = t.$createElement, o = t._self._c || s
      return o('div', { staticClass: 'constructor', class: { 'is-load': !t.load }, attrs: { 'js-constructor': '' } }, [o('div', { staticClass: 'constructor__wrapper' }, [o('div', { staticClass: 'constructor__main' }, [o('div', {
        staticClass: 'constructor__image',
        staticStyle: { 'background-image': 'url(\'/wp-content/themes/origin/assets/images/constructor/assets/vid-is-okna.png\')' }
      }, [o('img', { staticClass: 'constructor__image-main', attrs: { src: '/wp-content/themes/origin/assets/images/constructor/assets/vid-is-okna.png', alt: '' } }), t._l(t.options, function (s) {
        return o('div', { staticClass: 'constructor__images' }, t._l(s.colors, function (t) {
          return o('div', {
            staticClass: 'constructor__images-item load-img',
            class: { 'constructor__images-item_active': t.active },
            style: { 'background-image': 'url(' + t.image + ')' }
          })
        }), 0)
      })], 2)]), o('div', { staticClass: 'constructor__side' }, [o('div', { staticClass: 'constructor__cols' }, [o('div', { staticClass: 'constructor__menu constructor__cols-item constructor__cols-item_side' }, [o('div', { staticClass: 'constructor__menu-side' }, [o('div', { staticClass: 'constructor-options' }, [t._m(0), o('div', { staticClass: 'constructor-options__main' }, [o('div', { staticClass: 'constructor-options__list' }, t._l(t.options, function (s) {
        return o('div', { staticClass: 'constructor-options__list-item' }, [o('div', {
          staticClass: 'constructor-options__item',
          class: { 'constructor-options__item_active': s.active },
          on: { click: function (o) {t.setOption(s)} }
        }, [o('div', { staticClass: 'constructor-options__item-main' }, [o('div', { staticClass: 'constructor-options__item-title' }, [t._v('\n                                                    ' + t._s(s.title) + '\n                                                ')])])])])
      }), 0)])])]), o('div', { staticClass: 'constructor__menu-main' }, [o('div', { staticClass: 'constructor-selected' }, [t._m(1), o('div', { staticClass: 'constructor-selected__main' }, [o('div', { staticClass: 'constructor-selected__list' }, t._l(t.selectedColors, function (s) {return o('div', { staticClass: 'constructor-selected__list-item' }, [o('div', { staticClass: 'constructor-selected__item' }, [o('div', { staticClass: 'constructor-selected__item-main' }, [o('div', { staticClass: 'constructor-selected__item-title' }, [t._v('\n                                                    ' + t._s(s.parentTitle) + ':\n                                                    '), o('br'), o('span', { staticClass: 'gray' }, [t._v(' ' + t._s(s.title))])])])])])}), 0)])])])]), o('transition', { attrs: { name: 'fade' } }, [o('div', {
        directives: [{
          name: 'show',
          rawName: 'v-show',
          value: t.showColorMenu,
          expression: 'showColorMenu'
        }], staticClass: 'constructor__colors constructor__cols-item constructor__cols-item_main'
      }, [o('div', { staticClass: 'constructor-colors' }, [o('div', { staticClass: 'constructor-colors__toggle' }, [o('button', {
        staticClass: 'constructor-colors__toggle-btn',
        on: { click: t.toggleColorMenu }
      }, [o('img', { attrs: { src: 'data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ3NS4yIDQ3NS4yIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0NzUuMiA0NzUuMjsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIzMnB4IiBoZWlnaHQ9IjMycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik00MDUuNiw2OS42QzM2MC43LDI0LjcsMzAxLjEsMCwyMzcuNiwwcy0xMjMuMSwyNC43LTE2OCw2OS42UzAsMTc0LjEsMCwyMzcuNnMyNC43LDEyMy4xLDY5LjYsMTY4czEwNC41LDY5LjYsMTY4LDY5LjYgICAgczEyMy4xLTI0LjcsMTY4LTY5LjZzNjkuNi0xMDQuNSw2OS42LTE2OFM0NTAuNSwxMTQuNSw0MDUuNiw2OS42eiBNMzg2LjUsMzg2LjVjLTM5LjgsMzkuOC05Mi43LDYxLjctMTQ4LjksNjEuNyAgICBzLTEwOS4xLTIxLjktMTQ4LjktNjEuN2MtODIuMS04Mi4xLTgyLjEtMjE1LjcsMC0yOTcuOEMxMjguNSw0OC45LDE4MS40LDI3LDIzNy42LDI3czEwOS4xLDIxLjksMTQ4LjksNjEuNyAgICBDNDY4LjYsMTcwLjgsNDY4LjYsMzA0LjQsMzg2LjUsMzg2LjV6IiBmaWxsPSIjMDAwMDAwIi8+CgkJPHBhdGggZD0iTTM0Mi4zLDEzMi45Yy01LjMtNS4zLTEzLjgtNS4zLTE5LjEsMGwtODUuNiw4NS42TDE1MiwxMzIuOWMtNS4zLTUuMy0xMy44LTUuMy0xOS4xLDBjLTUuMyw1LjMtNS4zLDEzLjgsMCwxOS4xICAgIGw4NS42LDg1LjZsLTg1LjYsODUuNmMtNS4zLDUuMy01LjMsMTMuOCwwLDE5LjFjMi42LDIuNiw2LjEsNCw5LjUsNHM2LjktMS4zLDkuNS00bDg1LjYtODUuNmw4NS42LDg1LjZjMi42LDIuNiw2LjEsNCw5LjUsNCAgICBjMy41LDAsNi45LTEuMyw5LjUtNGM1LjMtNS4zLDUuMy0xMy44LDAtMTkuMWwtODUuNC04NS42bDg1LjYtODUuNkMzNDcuNiwxNDYuNywzNDcuNiwxMzguMiwzNDIuMywxMzIuOXoiIGZpbGw9IiMwMDAwMDAiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K' } })])]), o('div', { staticClass: 'constructor-colors__list' }, t._l(t.colors, function (s, c) {
        return o('div', {
          key: c,
          staticClass: 'constructor-colors__list-item'
        }, [o('div', {
          staticClass: 'constructor-colors__item',
          class: { 'constructor-colors__item_active': s.active },
          on: { click: function (o) {t.selectColor(s)} }
        }, [o('div', { staticClass: 'constructor-colors__item-side' }, [o('div', { staticClass: 'constructor-colors__item-icon' }, [o('img', { attrs: { src: s.icon } })])]), o('div', { staticClass: 'constructor-colors__item-main' }, [o('div', { staticClass: 'constructor-colors__item-title' }, [t._v('\n                                                ' + t._s(s.title) + '\n                                            ')])])])])
      }), 0)])])]), o('div', { staticClass: 'constructor__menu-main constructor__menu-main_mobile' }, [o('div', { staticClass: 'constructor-selected' }, [t._m(2), o('div', { staticClass: 'constructor-selected__main' }, [o('div', { staticClass: 'constructor-selected__list' }, t._l(t.selectedColors, function (s) {return o('div', { staticClass: 'constructor-selected__list-item' }, [o('div', { staticClass: 'constructor-selected__item' }, [o('div', { staticClass: 'constructor-selected__item-main' }, [o('div', { staticClass: 'constructor-selected__item-title' }, [t._v('\n                                                ' + t._s(s.parentTitle) + ':\n                                                '), o('br'), o('span', { staticClass: 'gray' }, [t._v(' ' + t._s(s.title))])])])])])}), 0)])])])], 1)])])])
    }, e = [function () {
      var t = this, s = t.$createElement, o = t._self._c || s
      return o('div', { staticClass: 'constructor-options__side' }, [o('div', { staticClass: 'constructor-options__title' }, [t._v('\n                                    Выберите цвет\n                                ')])])
    }, function () {
      var t = this, s = t.$createElement, o = t._self._c || s
      return o('div', { staticClass: 'constructor-selected__side' }, [o('div', { staticClass: 'constructor-selected__title' }, [t._v('\n                                    Выбранные цвета\n                                ')])])
    }, function () {
      var t = this, s = t.$createElement, o = t._self._c || s
      return o('div', { staticClass: 'constructor-selected__side' }, [o('div', { staticClass: 'constructor-selected__title' }, [t._v('\n                                Выбранные цвета\n                            ')])])
    }], a = (o('7514'), o('ac6a'), o('bd7e')), u = o.n(a), l = OPTIONS_CONSTRUCTOR, _ = 0
    l.forEach(function (t) {t.colors.forEach(function (s) {s.id = ++_, s.parentSlug = t.slug, s.parentTitle = t.title})})
    var d = {
      name: 'Constructor', data: function () {return { options: l, showColorMenu: !1, load: !1 }}, computed: {
        colors: function () {
          var t = this.options.find(function (t) {return !0 === t.active})
          return t ? t.colors : []
        }, allColors: function () {
          var t = []
          return this.options.forEach(function (s) {s.colors.forEach(function (s) {return t.push(s)})}), t
        }, selectedColors: function () {return this.allColors.filter(function (t) {return !0 === t.active}).map(function (t) {return { title: t.title, parentTitle: t.parentTitle }})}
      }, methods: {
        selectColor: function (t) {
          var s
          this.options.filter(function (s) {return s.slug === t.parentSlug}).forEach(function (o) {o.colors.forEach(function (t) {return t.active = !1}), s = o.colors.find(function (s) {return s.id === t.id}), s && (s.active = !0)})
        },
        setOption: function (t) {this.showColorMenu = !0, this.options.forEach(function (t) {return t.active = !1}), this.options.find(function (s) {return s.id === t.id}).active = !0},
        toggleColorMenu: function () {this.options.forEach(function (t) {return t.active = !1}), this.showColorMenu = !this.showColorMenu}
      }, mounted: function () {
        var t = this
        u()('[js-constructor]', { background: '.load-img' }, function () {t.load = !0})
      }
    }, v = d, C = (o('f6a1'), o('2877')), M = Object(C['a'])(v, r, e, !1, null, null, null)
    M.options.__file = 'Constructor.vue'
    var g = M.exports, f = { name: 'app', components: { Constructor: g } }, m = f, L = (o('034f'), Object(C['a'])(m, i, n, !1, null, null, null))
    L.options.__file = 'App.vue'
    var p = L.exports
    c['a'].config.productionTip = !1, new c['a']({ render: function (t) {return t(p)} }).$mount('#constructor')
  }, '64a9': function (t, s, o) {}, f6a1: function (t, s, o) {
    'use strict'
    var c = o('1c0b'), i = o.n(c)
    i.a
  }
})
//# sourceMappingURL=app.e81bd06b.js.map