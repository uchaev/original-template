// JavaScript Document
//
// ЦЕНЫ НА КОМПЛЕКТУЮЩИЕ

// профиль Ortex

s5801 = document.getElementById('s5801').value; //218.92 * 0.51//	рама Ortex
s5802 = document.getElementById('s5802').value; //254.73	 //	створка Ortex
s5803 = document.getElementById('s5803').value; //277.64	 //	импост Ortex
s5832 = document.getElementById('s5832').value; //54.98 	 //	Штапик с.п. 32 мм Ortex

s5701 = document.getElementById('s5701').value; //288 * 0.5 //	рама Ortex 70 мм
s5702 = document.getElementById('s5702').value; //335	 //	створка Ortex 70 мм
s5703 = document.getElementById('s5703').value; //362	 //	импост Ortex 70 мм
s6740 = document.getElementById('s6740').value; //65 	 //	Штапик с.п. 70 мм Ortex

//Профиля и штапики
// SALAMANDER

s250220 = document.getElementById('s250220').value; //606.75 * 1.06 //	рама 68 мм	Salamander-SL
s251020 = document.getElementById('s251020').value; //655.01	 //	створка 80 мм	Salamander-SL
s252120 = document.getElementById('s252120').value; //760.16	 //	импост 82 мм	Salamander-SL

//s413833 =	210	 //	штапик 33 мм	Salamander-штапик
s413717 = document.getElementById('s413717').value; //146.86	 //	3D штапик 17 мм	Salamander-штапик

//Профиля и штапики
// SALAMANDER BLUE

s170420 = document.getElementById('s170420').value; //1522 * 0.71//	рама 68 мм	Salamander-BLUE
s171020 = document.getElementById('s171020').value; //1574	 //	створка 80 мм	Salamander-BLUE
s172420 = document.getElementById('s172420').value; //1972	 //	импост 82 мм	Salamander-BLUE
s173415 = document.getElementById('s173415').value; //628	 //	мост глухого остекления

// EXPROF
S35801 = document.getElementById('S35801').value; //263 * 0.75	 //	рама 63 мм	Exprof-окно 58
S35802 = document.getElementById('S35802').value; //298	 //	створка 77 мм	Exprof-окно 58
S35803 = document.getElementById('S35803').value; //329	 //	импост 82 мм	Exprof-окно 58

PW35801 = document.getElementById('PW35801').value; // 229.60 * 0.58 //	рама 63 мм	Exprof-окно 58 XS
PW35802 = document.getElementById('PW35802').value; //258.06      //	створка 77 мм	Exprof-окно 58 XS
PW35803 = document.getElementById('PW35803').value; // 286.52	 //	импост 82 мм	Exprof-окно 58 XS

S57001 = document.getElementById('S57001').value; //209.73	 //	рама 72 мм	Exprof-окно 70
S57002 = document.getElementById('S57002').value; //233 //	створка 85 мм	Exprof-окно 70
S57003 = document.getElementById('S57003').value; //260	 //	импост 98 мм	Exprof-окно 70

s67001 = document.getElementById('s67001').value; // 319 * 0.67 // Experta rama
s67002 = document.getElementById('s67002').value; //360 // experta stvorka
s67003 = document.getElementById('s67003').value; //377 // experta impost

s57111 = document.getElementById('s57111').value; //299 * 0.69 // profecta rama
s57102 = document.getElementById('s57102').value; //322 // profecta stvorka
s57103 = document.getElementById('s57103').value; //351 // profecta impost

s35817 = document.getElementById('s35817').value; //68.8	 //	штапик с/п 32 мм	Exprof-штапик
s35820 = document.getElementById('s35820').value; //93	 //	штапик с/п 24 мм	Exprof-штапик
s35815 = document.getElementById('s35815').value; //124	 //	штапик с/п 04 мм	Exprof-штапик
s57006 = document.getElementById('s57006').value; //80.28	 //	штапик с/п 40 мм	Exprof-штапик

// REHAU
r8029 = document.getElementById('r8029').value; //416.24 * 0.87 //	рама Gealan 8000 (550711)
r8075 = document.getElementById('r8075').value; //551.76 //створка Gealan 8000 (550721)
r8044 = document.getElementById('r8044').value; //488.84 //импост Gealan 8000 (550028)

r714400 = document.getElementById('r714400').value; // 99.22 // штапик Gealan 8000 (560580)

// КАШИРОВКА штапик
s5832k = document.getElementById('s5832k').value; //250	 //	штапик 9 мм	Salamander-штапик
s413833k = document.getElementById('s413833k').value; //290	 //	штапик 17 мм	Salamander-штапик
s413717k = document.getElementById('s413717k').value; //260	 //	3D штапик 17 мм	Salamander-штапик
s35817k = document.getElementById('s35817k').value; //241.99	 //	штапик с/п 32 мм	Exprof-штапик
s35820k = document.getElementById('s35820k').value; //244.52	 //	штапик с/п 24 мм	Exprof-штапик
s35815k = document.getElementById('s35815k').value; //288.45	 //	штапик с/п 04 мм	Exprof-штапик
s57006k = document.getElementById('s57006k').value; //235.67	 //	штапик с/п 40 мм	Exprof-штапик
r714400k = document.getElementById('r714400k').value; //250	 //	штапик с/п 32 мм	LG-штапик
s6740k = document.getElementById('s6740k').value; //265 	 //	Штапик с.п. 70 мм Ortex

// Дверной профиль
s35810 = document.getElementById('s35810').value; //576.32 //створка 107
s35816 = document.getElementById('s35816').value; //481.02 //рама 72мм
spa1 = document.getElementById('spa1').value; //579.78 //СПА порог Алюминиевый
spa2 = document.getElementById('spa2').value; //0
spa_soed1 = document.getElementById('spa_soed1').value; //70 // соед порога
spa_soed2 = document.getElementById('spa_soed2').value; // 0

//Армировка
Arm203 = document.getElementById('Arm203').value; //139	 //	арм импост/ 2d	Exprof-металл
Arm207 = document.getElementById('Arm207').value; //101	 //	арм рама/створка	Exprof-металл
T30 = document.getElementById('T30').value; //124.15	 //	арм рама/створка	rehau
T24 = document.getElementById('T24').value; //76.67	      //	арм импост	rehau
T41 = document.getElementById('T41').value; //205.70	//	арм. рама	           Salamander 2d
s215120P = document.getElementById('s215120P').value; //173.69	 //	арм. 252120	Salamander sl
s455210P = document.getElementById('s455210P').value; //131.27  //	арм. 250220. 251020	Salamander sl
s415020 = document.getElementById('s415020').value; //1005	// арм. 212020. 112220	Salamander blue

// Дверная армировка
T40 = document.getElementById('T40').value; //244.97 //створка

//Резина стекло
s228 = document.getElementById('s228').value; //14.36	 //	резина упорная	Exprof
s255 = document.getElementById('s255').value; //15.91	 //	резина стекольная	Exprof
Rehau865002 = document.getElementById('Rehau865002').value; //28.96	 //	резина стекольная	Salamander Rehau
Veka254 = document.getElementById('Veka254').value; // 22.52	 //	резина упорная	Salamander
//s19237	 =	12.07	 //	резина упорная LG	LG

//Прочее сборочные
s417022 = document.getElementById('s417022').value; //4.29	 //	водоотв. колп.	на все
s218025 = document.getElementById('s218025').value; //53.63	 //	соединитель импоста	Salamander

s178020 = document.getElementById('s178020').value; //236	 //	соединитель импоста	Salamander BLUE
s178820 = document.getElementById('s178820').value; //48	 //	уплотнительный блок	Salamander BLUE

s218823 = document.getElementById('s218823').value; //58.99 //	упл. соед. импоста	Salamander
V35803 = document.getElementById('V35803').value; //29.38	 //	соединитель импоста	Exprof
V57103 = document.getElementById('V57103').value; //41	 //	соединитель импоста	Exprof supr

ss354187 = document.getElementById('ss354187').value; //70.18	 //	соед. импоста FS-70R	LG
falc1 = document.getElementById('falc1').value; //9.68	 //	фальц. вкладыш BF-70R	LG
PS1M = document.getElementById('PS1M').value; //4.08	 //	мост на фальц	Exprof
PS70 = document.getElementById('PS70').value; //4.08	 //	мост на фальц	Exprof
s117040 = document.getElementById('s117040').value; //8.04 //	фальц. вкладыш 76 мм	Salamander
s217040 = document.getElementById('s217040').value; //8.42	 //	фальц. вкладыш 60 мм	Salamander
Riht28 = document.getElementById('Riht28').value; //1.8	 //	рихтовочная пластина 28	Без группы
Riht36 = document.getElementById('Riht36').value; //2.4	 //	рихтовочная пластина 36	Без группы
Riht40 = document.getElementById('Riht40').value; //3	 //	рихтовочная пластина 40	Без группы
Riht52 = document.getElementById('Riht52').value; //4	 //	рихтовочная пластина 52	Без группы

Sam3916 = document.getElementById('Sam3916').value; //0.6	 //	шуруп 3.9*16 с буром	Без группы
Sam4225 = document.getElementById('Sam4225').value; //0.8	 //	шуруп 4.2*25 оцинк	Без группы
Sam3935 = document.getElementById('Sam3935').value; //0.6	 //	шуруп 3.9*35 оцинк	Без группы
Sam580 = document.getElementById('Sam580').value; //1.3	 //	шуруп 5*80	Без группы
Sam440 = document.getElementById('Sam440').value; //0.8	 //	шуруп 4*40 оцинк	Без группы
us10 = document.getElementById('us10').value; //42.43//соединитель углов двери

//заполнение
germ = document.getElementById('germ').value; //334.61	 //	Polikad	Polikad
butil = document.getElementById('butil').value; //1514.70	 //	Бутил Fenzi	Бутил

s3272680195 = document.getElementById('s3272680195').value; //34.88	 //	рамка 9.5 мм	Дист. рамка
s3272680095 = document.getElementById('s3272680095').value; //20.33	 //	рамка 9.5 мм	Дист. рамка
s3272680135 = document.getElementById('s3272680135').value; //23.79	 //	рамка 13.5 мм	Дист. рамка
s8403615 = document.getElementById('s8403615').value; //25.18	 //	рамка 15.5 мм	Дист. рамка
H115 = document.getElementById('H115').value; //17.69	 //	рамка 11.5 мм	Дист. рамка
H175 = document.getElementById('H175').value; //20.06	//	рамка 17.5 мм	Дист. рамка
sito = document.getElementById('sito').value; //132	 //	молекулярное сито	Без группы
ug10 = document.getElementById('ug10').value; //2.7	 //	уголок 9.5 мм	Без группы
ug14 = document.getElementById('ug14').value; //3.40	 //	уголок 13.5 мм	Без группы
ug16 = document.getElementById('ug16').value; //3.42	 //	уголок 15.5 мм	Без группы

// ----------------- PRISVOENIE ZNACHENIY-------------------//
ug52 = ug14 //blue
ug520 = ug14 //blue

ug32 = ug10
ug40 = ug14
ug24 = ug16
ug320 = ug10
ug400 = ug14
ug240 = ug16
ug0 = ug1 = 0
ram32 = s3272680095
ram320 = s3272680095
ram52 = s3272680195
ram520 = s3272680195
ram40 = s3272680135
ram400 = s3272680135
ram24 = s8403615
ram240 = s8403615
ram0 = ram1 = 0
a40 = a400 = s413717;
b32 = b320 = s5832;
b24 = b240 = s413717;
c40 = c400 = r714400;
d32 = d320 = f32 = f320 = s35817;
d24 = d240 = f24 = f240 = d0 = s35820;
d1 = f1 = s35815;

g40k = g400k = h40k = h400k = s57006k;
g40 = g400 = h40 = h400 = s57006;

e52 = e520 = s413717; //blue
e52k = e520k = s413717k; //blue


i40 = i400 = s6740; //orteks
i40k = i400k = s6740k; //orteks


a40k = a400k = s413717k;
b32k = b320k = s5832k;
b24k = b240k = s413717k;
c40k = c400k = r714400k;
d32k = d320k = f32k = f320k = s35817k;
d24k = d240k = f24k = f240k = d0k = s35820k;
d1k = f1k = s35815k;
tonir = 0;
Ptonir = 1260;
kashir_ul = 0;
kashir_kv = 0;
Pkashir = 3000;
kashir_ruchka = 0;
kashir_petli = 0;
kashir_vodootl = 0;
kashir_marker = 0;

glass4mm = 591.88 //	Стекло 4 мм	Стекло
glass4mmTOP = 994.35 //Стекло 4мм ТОП
sendvich24 = 1240 //сендвич 24мм

rabPVH = 18.00	 //	работа ПВХ	Без группы
rabSP = 18.00	 //	работа С/П	Без группы
// прочее по фурнитуре
s20201Planet = 58	 //	Ручка Планета	фурнитура GU
ss406250 = 372.94 * 0.4	 //соед. балконный * коэф.вырывнивания 	Salamander-дверной
ss000606 = 73.6	 //	соед. Универс	Exprof-соединители
ss732469 = 83.39	 //	соед. балконный 70	LG-60
RuchkaZamok = 886	 //	ручка оконная ключ	фурнитура GU
a01601 = 1400 //ручка-скоба 300мм
cth0415 = 84 //накладка на ПЗ овал
gezets2000 = 3780 // доводчик

//furnitura
ss101548 = 229	 //	ножницы.откидные	фурнитура МАСО
ss10538 = 118	 //	шпинг.осн.зап.верх.90	фурнитура МАСО
ss10586 = 118
ss125535 = 338.8//ответная планка 50пз!
ss223011 = 4200 //замок GTS!
ss25535l = 600 // ответ 49рз экспроф!
ss3166920011 = 784 //цилиндр 35*45!
ss33322 = 35.5	 //	отв.планк.по.КВЕ.правая	фурнитура МАСО
ss33323 = 35.5	 //	отв.планк.по.КВЕ.левая	фурнитура МАСО
ss33460 = 35.5	 //	отв.планк.по.Rehau.правая	фурнитура МАСО
ss33461 = 35.5	 //	отв.планк.по.Rehau.левая	фурнитура МАСО
ss33483 = 35.5	 //	отв.планк.по.VEKA.правая	фурнитура МАСО
ss33484 = 35.5	 //	отв.планк.по.VEKA.левая	фурнитура МАСО
ss34283 = 23	 //	отв.планк.VEKA	фурнитура МАСО
ss34850 = 23	 //	отв.планк.КВЕ	фурнитура МАСО
ss354970 = 23	 //	отв.планк.Rehau	фурнитура МАСО
ss40005 = 84	 //	комплект декор.накладок	фурнитура МАСО
ss41339 = 19	 //	накл.сред.прижим.ств	фурнитура МАСО
ss41342 = 19	 //	накл.сред.прижим.рама	фурнитура МАСО
ss41742 = 15	 //	накл.ниж.пет.рамы.корот	фурнитура МАСО
ss41743 = 15		 //	накл.ниж.пет.рамы.длин	фурнитура МАСО
ss42083 = 15	 //	накл.верх.пет.рамы	фурнитура МАСО
ss42084 = 15		 //	накл.верх.угловой.пет	фурнитура МАСО
ss42087 = 15		 //	накл.нижн.пет.ств	фурнитура МАСО
ss52321 = 272	 //	петля.универс.фрамужняя	фурнитура МАСО
ss52389 = 110	 //	прижим.наклад.средний	фурнитура МАСО
ss52400 = 385	 //	осн.зап.ПО.фикс.370-620	фурнитура МАСО!
ss52422 = 342	 //	осн.зап.ПО.сред.500-650	фурнитура МАСО!
ss52423 = 424	 //	осн.зап.ПО.сред.600-900	фурнитура МАСО!
ss52425 = 367	 //	осн.зап.ПО.сред.901-1300	фурнитура МАСО!
ss52426 = 510	 //	осн.зап.ПО.сред.1301-1800	фурнитура МАСО!
ss52427 = 792	 //	осн.зап.ПО.сред.1801-2350	фурнитура МАСО!
ss52432 = 128	 //	шпинг.осн.зап.ниж500-1800	фурнитура МАСО
ss52433 = 128	 //	шпинг.осн.зап.ниж1800-235	фурнитура МАСО
ss52443 = 476	 //	ножницы.Gr00.281-310	фурнитура МАСО
ss52444 = 476	 //	ножницы.Gr0.431-630	фурнитура МАСО
ss52445 = 582	 //	ножницы.Gr1.601-800	фурнитура МАСО
ss52446 = 685	 //	ножницы.Gr2.801-1050	фурнитура МАСО
ss52447 = 685	 //	ножницы.Gr3.1051-1300	фурнитура МАСО
ss52453 = 287	 //	сред.зап.MV300.600-750	фурнитура МАСО
ss52457 = 396	 //	сред.зап.Gr2.1501-1750	фурнитура МАСО
ss52458 = 434	 //	сред.зап.Gr3.1750-1950	фурнитура МАСО
ss52462 = 136	 //	петля.поворотная.створки	фурнитура МАСО
ss52463 = 232	 //	осн.зап.П.сред.300-500	фурнитура МАСО
ss52464 = 279	 //	осн.зап.П.сред.500-700	фурнитура МАСО
ss52465 = 304	 //	осн.зап.П.сред.700-1000	фурнитура МАСО
ss52466 = 420	 //	осн.зап.П.сред.1000-1400	фурнитура МАСО
ss52478 = 151	 //	петля.нижн.створки	фурнитура МАСО
ss52480 = 81	 //	петля.верхн.рамы	фурнитура МАСО
ss52483 = 154	 //	петля.нижн.рамы	фурнитура МАСО
ss52486 = 98	 //	петля.угловая.Rehau	фурнитура МАСО
ss52487 = 89	 //	петля.угловая.КВЕ	фурнитура МАСО
ss52502 = 150	 //	защелка.балконная	фурнитура МАСО
ss52513 = 449	 //	угловая.передача.400-1650	фурнитура МАСО
ss52514 = 217	 //	угловая.передача.280-650	фурнитура МАСО
ss52660 = 297	 //	ножн.пров.310-600.прав	фурнитура МАСО
ss52661 = 297	 //	ножн.пров.310-600.левые	фурнитура МАСО
ss52662 = 418	 //	ножн.пров.601-800.прав	фурнитура МАСО
ss52663 = 418	 //	ножн.пров.601-800.левые	фурнитура МАСО
ss52664 = 785	 //	ножн.пров.801-1050.прав	фурнитура МАСО
ss52665 = 785	 //	ножн.пров.801-1050.левые	фурнитура МАСО
ss52666 = 785	 //	ножн.пров.1050-1300.прав	фурнитура МАСО
ss52667 = 785	 //	ножн.пров.1050-1300.левые	фурнитура МАСО
ss54674 = 217	 //	сред.зап.Gr00.750-1050	фурнитура МАСО
ss54675 = 217	 //	сред.зап.Gr0.1051-1300	фурнитура МАСО
ss54676 = 256	 //	сред.зап.Gr1.1251-1500	фурнитура МАСО
ss55009 = 102	 //	петля.угловая.VEKA	фурнитура МАСО
ss55037 = 541	 //	осн.зап.П.сред.1400-1800	фурнитура МАСО
ss55169 = 733	 //	Б-ТВ Е92 удлин-ль гр.2	фурнитура МАСО
ss55170 = 547	 //	Б-ТВ Е92 удлин-ль гр.1	фурнитура МАСО
ss58188 = 3134	 //	Б-ТВ Е92 многозап.замок	фурнитура МАСО
ss59920 = 733	 //	сред.зап.Gr4.1951-2350	фурнитура МАСО
ss6254852503 = 3006//
ss6259930007 = 938 // ручка диридент внешняя
ss6280720007 = 242 // ручка диридент без штифта
ss6299870001 = 333.19 //
ss6317482501 = 3381 // перед зап Д 25 1951-2450
ss6310552501 = 3131
ss6310562503 = 3757 // перед б зап д 25 1951-2450
ss6320750501 = 219.21
ss6320750701 = 225.46
ss6320751201 = 320.66
ss6321040001 = 162.84 // удлинитель верхний
ss6321550301 = 196.66
ss8178674100 = 700 //замок 49pzw 35-16-92
ss8558394010 = 700 // замок 49 оконный
ss8558454010 = 770 // замок 50 пз 25 16
ss9186124010 = 770 // замок 50пз 35 16
ss9268746901 = 56 // четырехгранник
ss9311150001 = 36.32
ss93546900l3 = 50.3
ss9404690001 = 87.68
ssk154160207 = 560 //моноблок узкий
ss9355960001 = 204.13 //защелка дверная
ss94491 = 16	 //	штифт.верх.петли	фурнитура МАСО
ss95127 = 35	 //	отв.планк.приподнимат.КВЕ	фурнитура МАСО
ss95141 = 35	 //	отв.планк.приподним.Sal	фурнитура МАСО
ss95283 = 35	 //	отв.планк.приподн.VEKA	фурнитура МАСО
ss96587 = 65
nakrutka_na_povorotnuyu = 18
petliD1 = ss9375640007 = 560//петля КТН аналог
petliD2 = ssk142390107 = 1820//петля КТН оригинал
petliO1 = h003100907 = 560 // KTK-GU
petliO3 = naborMACO = ss52462 + ss52487 + ss52480 + ss52483 + ss52478 + ss40005 + ss94491 + Sam4225 * 5 + Sam440 * 11;
//кашированные петли
petliD1k = ss9375640007 = 1134.00//петля КТН аналог
petliD2k = ssk142390107 = 2530//петля КТН оригинал
petliO1k = h003100907 = 1500 // KTK-GU
petliO3k = naborMACO = ss52462 + ss52487 + ss52480 + ss52483 + ss52478 + 87.4 + ss94491 + Sam4225 * 5 + Sam440 * 11;
//Moskitka (cen net v obshey baze)
ramaMs = 0.114
ramaMv = 0.136
impM = 0.104
polotnoMs = 0.054
polotnoMv = 0.076
krepI = 2
krepM = 1
ruchkaM = 2
ugolokM = 4

PramaM = 81.5
PimpM = 90
PpolotnoM = 128
PkrepI = 2.7
PkrepM = 21.8
PruchkaM = 4
PugolokM = 4.4
PshnurM = 5.5

ruchkaBB = 20.8
pod1 = 750
pod0 = 420

sbrosProf = 0;
//
//
// ФУНКЦИЯ ПО ПРИСВОЕНИЮ ЗНАЧЕНИЙ СТЕКЛОПАКЕТОВ В ЗАВИСИМОСТИ ОТ ПРОФИЛЯ
//
aSP = new Array(
  "32мм (3 стекла), 32, 32мм ТОП (3 стекла), 320, 24мм (2 стекла), 24, 24мм ТОП(2 стекла), 240",
  "40мм (3 стекла), 40, 40мм ТОП (3 стекла), 400",
  "32мм (3 стекла), 32, 32мм ТОП (3 стекла), 320, 24мм (2 стекла), 24, 24мм ТОП(2 стекла), 240, 1 стекло, 1",
  "32мм (3 стекла), 32, 32мм ТОП (3 стекла), 320, 24мм (2 стекла), 24, 24мм ТОП(2 стекла), 240, 1 стекло, 1",
  "40мм (3 стекла), 40, 40мм ТОП (3 стекла), 400",
  "40мм (3 стекла), 40, 40мм ТОП (3 стекла), 400",
  "40мм (3 стекла), 40, 40мм ТОП (3 стекла), 400",
  "40мм (3 стекла), 40, 40мм ТОП (3 стекла), 400",
  "52мм (3 стекла), 52, 52мм ТОП (3 стекла), 520, 40мм (3 стекла), 40, 40мм ТОП(3 стекла), 400"
);

function getSPByStreet(znak) {
  sSP = aSP[znak];
  return sSP.split(',');
}

function MkSP(znak) {
  aCurrSP = getSPByStreet(znak);
  nCurrSPCnt = aCurrSP.length;
  oSPList = document.getElementById("spVal");
  oSPListOptionsCnt = oSPList.options.length;
  oSPList.length = 0; // ОБНУЛЕНИЕ ПОЛЯ
  for (i = 0; i < nCurrSPCnt; i = i + 2) {
    if (document.createElement) {
      newSPListOption = document.createElement("OPTION");
      newSPListOption.text = aCurrSP[i];
      newSPListOption.value = aCurrSP[i + 1];
      // метод IE, либо DOM
      (oSPList.options.add) ? oSPList.options.add(newSPListOption) : oSPList.add(newSPListOption, null);
    } else {
      // для NN3.x-4.x
      oSPList.options[i] = new Option(aCurrSP[i], aCurrSP[i], false, false);
    }
  }
}

//
//
// ФУНКЦИЯ ПО ПРИСВОЕНИЮ ЗНАЧЕНИЙ ПЕТЕЛЬ В ЗАВИСИМОСТИ ОТ ТИПА ПРОФИЛЯ ДВЕРИ
//
aPetli = new Array(
  "KTK-GU (рекоменд), 1, KTK-SN, 2, MACO оконные, 3",
  "KTN аналог, 1, KTN оригинал, 2"
);

function getSPByStreetPetli(znakPetli) {
  sPetli = aPetli[znakPetli];
  return sPetli.split(',');
}

function MkPetli(znakPetli) {
  aCurrPetli = getSPByStreetPetli(znakPetli);
  nCurrPetliCnt = aCurrPetli.length;
  oPetliList = document.getElementById("tipPetliDveri");
  oPetliListOptionsCnt = oPetliList.options.length;
  oPetliList.length = 0; // ОБНУЛЕНИЕ ПОЛЯ
  for (i = 0; i < nCurrPetliCnt; i = i + 2) {
    if (document.createElement) {
      newPetliListOption = document.createElement("OPTION");
      newPetliListOption.text = aCurrPetli[i];
      newPetliListOption.value = aCurrPetli[i + 1];
      // метод IE, либо DOM
      (oPetliList.options.add) ? oPetliList.options.add(newPetliListOption) : oPetliList.add(newPetliListOption, null);
    } else {
      // для NN3.x-4.x
      oPetliList.options[i] = new Option(aCurrPetli[i], aCurrPetli[i], false, false);
    }
  }

}


// Убираем настройки для двери
function param_door_close() {
  TipInfo = document.getElementById('tipInfo').value;
//Вставляем то, что убирали
//ширина створки
  if (TipInfo == '11' || TipInfo == '22' || TipInfo == '21' || TipInfo == '31' || TipInfo == '32' || TipInfo == '45') {
    //Есть створка
    document.getElementById('stvorka2-wrapepr').classList.remove('hidden');
    document.getElementById('stvorka2').value = "";
  } else {
    //Нет створки
    document.getElementById('stvorka2-wrapepr').classList.add('hidden');
    document.getElementById('stvorka2').value = "";
  }

  //Балконный блок
  if (TipInfo == "45") {
    document.getElementById('bbglu1-wrapepr').classList.remove('hidden');
    document.getElementById('bbglu1').value = "";
  } else {
    document.getElementById('bbglu1-wrapepr').classList.add('hidden');
    document.getElementById('bbglu1').value = "";
  }
}

// ФУНКЦИЯ ПО ИЗМЕНЕНИЮ ЦВЕТА ПОЛЯ ЭСКИЗ
//
function chC() {
  document.getElementById('id5').style.backgroundColor = '#fe0410';
}

function chColo(a) {
  document.getElementById('idd' + a).style.backgroundColor = '#00CE00';
}

function chColo1(b) {
  document.getElementById('idd' + b).style.backgroundColor = '#009B9B';
}

function chColor(a, b, c, d, e, f, g, h, i, j) {
  num1 = a;
  num2 = b;
  num3 = c;
  num4 = d;
  num5 = e;
  num6 = f;
  num7 = g;
  num8 = h;
  num9 = i;
  document.getElementById('id' + num1).style.backgroundColor = '#fe0410';
  document.getElementById('id' + num2).style.backgroundColor = '#f0f0f0';
  document.getElementById('id' + num3).style.backgroundColor = '#f0f0f0';
  document.getElementById('id' + num4).style.backgroundColor = '#f0f0f0';
  document.getElementById('id' + num5).style.backgroundColor = '#f0f0f0';
  document.getElementById('id' + num6).style.backgroundColor = '#f0f0f0';
  document.getElementById('id' + num7).style.backgroundColor = '#f0f0f0';
  document.getElementById('id' + num8).style.backgroundColor = '#f0f0f0';
  document.getElementById('id' + num9).style.backgroundColor = '#f0f0f0';
}

//
// TONE
// ТОНИРОВКА
//
function Tonirovka(type) {
  window.tonir = type;
}

//
// KASH
// КАШИРОВКА
//
function Kashirovka(type) {
  if (type == 'none') {
    window.kashir_ul = 0;
    window.kashir_kv = 0;
    window.kashir_ruchka = 0;
    window.kashir_petli = 0;
    window.kashir_vodootl = 0;
    window.kashir_marker = 0;
  } else if (type == 'ul') {
    window.kashir_ul = 1;
    window.kashir_kv = 0;
    window.kashir_ruchka = 0;
    window.kashir_petli = 0;
    window.kashir_vodootl = 10;
    window.kashir_marker = 448;
  } else if (type == 'kv') {
    window.kashir_ul = 0;
    window.kashir_kv = 1;
    window.kashir_ruchka = 174;
    window.kashir_petli = 65;
    window.kashir_vodootl = 0;
    window.kashir_marker = 448;
  } else if (type == '2st') {
    window.kashir_ul = 1;
    window.kashir_kv = 1;
    window.kashir_ruchka = 174;
    window.kashir_petli = 65;
    window.kashir_vodootl = 10;
    window.kashir_marker = 448;
  }
}


//
// PARAMETRI
// НАЧАЛЬНЫЕ ДАННЫЕ ДЛЯ ОБРАБОТКИ
//
function Parametri() {
  prof = document.getElementById('prof').value;
  tip = document.getElementById('tipInfo').value;


  if (tip != "d") {

    sp = eval(document.getElementById('spVal').value);
    ugolok = eval('ug' + sp);
    ramka = eval('ram' + sp);

    if (kashir_kv == '0') {
      shtapik = eval(prof + sp);
    } else {
      shtapik = eval(prof + sp + 'k')
    }

    if (prof == "a") {
      rama = s250220;
      stvorka = s251020;
      impost = s252120;
      armIMP = s215120P;
      arm = s455210P;
      soedIMP = s218025 + s218823;
      most = s117040;
      riht = Riht40;
      rezS = Rehau865002;
      rezU = Veka254;
      razStvV = 0.080
      razStvS = 0.053
      razFalc = 0.040
      sStv = 0.080
      petlUglov = ss52486
      otvPl = ss354970
      otvPl2 = ss33460
      otvPl3 = ss95141
      msR = 28
      gR = 9
      est = 0
      sRama = ss406250 * 2
      ramaT_Ulica = 0.095
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.080
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.135
      impostT_kvartira = 0.055
    }
    if (prof == "b") {
      rama = s5801;
      stvorka = s5802;
      impost = s5803;
      armIMP = Arm203;
      arm = Arm207;
      soedIMP = s218025;
      most = s217040;
      riht = Riht36;
      rezS = s255;
      rezU = s228;
      razStvV = 0.084
      razStvS = 0.055
      razFalc = 0.040
      sStv = 0.080
      petlUglov = ss52486
      otvPl = ss354970
      otvPl2 = ss33460
      otvPl3 = ss95141
      msR = 40
      gR = 12.5
      est = 0
      sRama = ss406250 * 2
      ramaT_Ulica = 0.095
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.080
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.115
      impostT_kvartira = 0.050
    }
    if (prof == "c") {
      rama = r8029;
      stvorka = r8075;
      impost = r8044;
      armIMP = T30;
      arm = T30;
      soedIMP = ss354187;
      most = falc1;
      riht = Riht40;
      rezS = Rehau865002;
      rezU = Veka254;
      razStvV = 0.072
      razStvS = 0.049
      razFalc = 0.040
      sStv = 0.078
      petlUglov = ss55009
      otvPl = ss34283
      otvPl2 = ss33483
      otvPl3 = ss95283
      msR = 28
      gR = 8.8
      est = 1
      sRama = ss732469
      ramaT_Ulica = 0.085
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.075
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.115
      impostT_kvartira = 0.050
    }
    if (prof == "d") {
      rama = S35801;
      stvorka = S35802;
      impost = S35803;
      armIMP = Arm203;
      arm = Arm207;
      soedIMP = V35803;
      most = PS1M;
      riht = Riht36;
      rezS = s255;
      rezU = s228;
      razStvV = 0.07
      razStvS = 0.05
      razFalc = 0.04
      sStv = 0.077
      petlUglov = ss52487
      otvPl = ss34850
      otvPl2 = ss33322
      otvPl3 = ss95127
      msR = 38
      gR = 12
      est = 1
      sRama = ss000606 * 2
      ramaT_Ulica = 0.080
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.075
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.115
      impostT_kvartira = 0.055
    }
//blue
    if (prof == "e") {
      rama = s170420;
      stvorka = s171020;
      impost = s172420;
      armIMP = s415020;
      arm = T41;
      soedIMP = s178020;
      most = PS70;
      riht = Riht52;
      rezS = Rehau865002;
      rezU = Veka254;
      razStvV = 0.072
      razStvS = 0.05
      razFalc = 0.040
      sStv = 0.085
      petlUglov = ss52487
      otvPl = ss34850
      otvPl2 = ss33322
      otvPl3 = ss95127
      msR = 28
      gR = 8.8
      est = 1
      sRama = ss000606 * 2
      ramaT_Ulica = 0.085
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.080
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.135
      impostT_kvartira = 0.050
    }
    if (prof == "f") {
      rama = PW35801;
      stvorka = PW35802;
      impost = PW35803;
      armIMP = Arm203;
      arm = Arm207;
      soedIMP = V35803;
      most = PS1M;
      riht = Riht36;
      rezS = s255;
      rezU = s228;
      razStvV = 0.07
      razStvS = 0.048
      razFalc = 0.04
      sStv = 0.077
      petlUglov = ss52487
      otvPl = ss34850
      otvPl2 = ss33322
      otvPl3 = ss95127
      msR = 38
      gR = 12
      est = 1
      sRama = ss000606 * 2
      ramaT_Ulica = 0.080
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.075
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.115
      impostT_kvartira = 0.055
    }
    if (prof == "g") {
      rama = s67001;
      stvorka = s67002;
      impost = s67003;
      armIMP = Arm203;
      arm = Arm207;
      soedIMP = V57103;
      most = PS70;
      riht = Riht40;
      rezS = s255;
      rezU = s228;
      razStvV = 0.072
      razStvS = 0.05
      razFalc = 0.040
      sStv = 0.085
      petlUglov = ss52487
      otvPl = ss34850
      otvPl2 = ss33322
      otvPl3 = ss95127
      msR = 28
      gR = 8.8
      est = 1
      sRama = ss000606 * 2
      ramaT_Ulica = 0.085
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.080
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.135
      impostT_kvartira = 0.050
    }
    if (prof == "h") {
      rama = s57111;
      stvorka = s57102;
      impost = s57103;
      armIMP = Arm203;
      arm = Arm207;
      soedIMP = V57103;
      most = PS70;
      riht = Riht40;
      rezS = s255;
      rezU = s228;
      razStvV = 0.072
      razStvS = 0.05
      razFalc = 0.040
      sStv = 0.085
      petlUglov = ss52487
      otvPl = ss34850
      otvPl2 = ss33322
      otvPl3 = ss95127
      msR = 28
      gR = 8.8
      est = 1
      sRama = ss000606 * 2
      ramaT_Ulica = 0.085
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.080
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.135
      impostT_kvartira = 0.050
    }
    if (prof == "i") {
      rama = s5701;
      stvorka = s5702;
      impost = s5703;
      armIMP = Arm203;
      arm = Arm207;
      soedIMP = V57103;
      most = PS70;
      riht = Riht40;
      rezS = s255;
      rezU = s228;
      razStvV = 0.072
      razStvS = 0.05
      razFalc = 0.040
      sStv = 0.085
      petlUglov = ss52487
      otvPl = ss34850
      otvPl2 = ss33322
      otvPl3 = ss95127
      msR = 28
      gR = 8.8
      est = 1
      sRama = ss000606 * 2
      ramaT_Ulica = 0.085
      ramaT_Kvartira = 0.055
      stvorkaT_Ulica = 0.080
      stvorkaT_Kvartira = 0.080
      impostT_ulica = 0.135
      impostT_kvartira = 0.050
    }


    if (tip == "1") {
      chastNum = 1;
      impNum = 0;
      gluhoeNum = 1;
      stvorkaNum = 0;
      koef = 0
      otd0 = 1400
      otd1 = 2400
    }
    if (tip == "11") {
      chastNum = 1
      impNum = 0
      gluhoeNum = 0
      stvorkaNum = 1
      koef = 0
      otd0 = 1400
      otd1 = 2400
    }
    if (tip == "2") {
      chastNum = 2;
      impNum = 1;
      gluhoeNum = 2;
      stvorkaNum = 0;
      koef = 0
      otd0 = 1400
      otd1 = 2400
    }
    if (tip == "21") {
      chastNum = 2
      impNum = 1
      gluhoeNum = 1
      stvorkaNum = 1
      koef = 0
      otd0 = 1400
      otd1 = 2400
    }
    if (tip == "22") {
      chastNum = 2
      impNum = 1
      gluhoeNum = 0
      stvorkaNum = 2
      koef = 0
      otd0 = 1400
      otd1 = 2400
    }
    if (tip == "3") {
      chastNum = 3
      impNum = 2
      gluhoeNum = 3
      stvorkaNum = 0
      koef = 0
      otd0 = 1900
      otd1 = 2900
    }
    if (tip == "31") {
      chastNum = 3
      impNum = 2
      gluhoeNum = 2
      stvorkaNum = 1
      koef = 0.027
      otd0 = 1900
      otd1 = 2900
    }
    if (tip == "32") {
      chastNum = 3
      impNum = 2
      gluhoeNum = 1
      stvorkaNum = 2
      koef = 0
      otd0 = 1900
      otd1 = 2900
    }
// ---------STEKLO PAKETI --------------------//
    if (sp == "52") {
      st1 = glass4mm
      st2 = glass4mm
      st3 = glass4mm
      kdist = 2
      kgerm = 0.2324
      ksito = 0.7
      krab = 6
    }
    if (sp == "520") {
      st1 = glass4mmTOP
      st2 = glass4mm
      st3 = glass4mm
      kdist = 2
      kgerm = 0.2324
      ksito = 0.07
      krab = 6
    }
    if (sp == "40") {
      st1 = glass4mm
      st2 = glass4mm
      st3 = glass4mm
      kdist = 2
      kgerm = 0.2324
      ksito = 0.07
      krab = 6
    }
    if (sp == "400") {
      st1 = glass4mmTOP
      st2 = glass4mm
      st3 = glass4mm
      kdist = 2
      kgerm = 0.2324
      ksito = 0.07
      krab = 6
    }
    if (sp == "32") {
      st1 = glass4mm
      st2 = glass4mm
      st3 = glass4mm
      kdist = 2
      kgerm = 0.166
      ksito = 0.05
      krab = 6
    }
    if (sp == "320") {
      st1 = glass4mmTOP
      st2 = glass4mm
      st3 = glass4mm
      kdist = 2
      kgerm = 0.166
      ksito = 0.05
      krab = 6
    }
    if (sp == "24") {
      st1 = glass4mm
      st2 = glass4mm
      st3 = 0
      kdist = 1
      kgerm = 0.125
      ksito = 0.04125
      krab = 4
    }
    if (sp == "240") {
      st1 = glass4mmTOP
      st2 = glass4mm
      st3 = 0
      kdist = 1
      kgerm = 0.125
      ksito = 0.04125
      krab = 4
    }
    if (sp == "0") {
      st1 = glass4mm
      st2 = 0
      st3 = 0
      kdist = 0
      kgerm = 0
      ksito = 0
      krab = 2
    }
    if (sp == "1") {
      st1 = glass4mm
      st2 = 0
      st3 = 0
      kdist = 0
      kgerm = 0
      ksito = 0
      krab = 2
    }
    Raschet();
  }
}

//
// FINDSTV
// ОПРЕДЕЛЕНИЕ КОЛИЧЕСТВА СТВОРОК
//
function findStv() {
  tip = document.getElementById('tipInfo').value;
  if (tip == "1") {
    chastNum = 1;
    gluhoeNum = 1;
    stvorkaNum = 0;
  }
  if (tip == "11") {
    chastNum = 1
    gluhoeNum = 0
    stvorkaNum = 1
  }
  if (tip == "2") {
    chastNum = 2;
    gluhoeNum = 2;
    stvorkaNum = 0;
  }
  if (tip == "21") {
    chastNum = 2
    gluhoeNum = 1
    stvorkaNum = 1
  }
  if (tip == "22") {
    chastNum = 2
    gluhoeNum = 0
    stvorkaNum = 2
  }
  if (tip == "3") {
    chastNum = 3
    gluhoeNum = 3
    stvorkaNum = 0
  }
  if (tip == "31") {
    chastNum = 3
    gluhoeNum = 2
    stvorkaNum = 1
  }
  if (tip == "32") {
    chastNum = 3
    gluhoeNum = 1
    stvorkaNum = 2
  }
  if (tip == "45") {
    chastNum = 3
    gluhoeNum = 1
    stvorkaNum = 1
    otd0 = 2200
    otd1 = 3200
  }
  if (tip == "d") {
    chastNum = 1
    gluhoeNum = 0
    stvorkaNum = 1
  }
  stvFind = document.getElementById('shirina').value;
  stvFind1 = Number(eval(stvFind) / chastNum).toFixed(0);
  document.getElementById('stvorka2').value = stvFind1;
}

//
//
// ОПРЕДЕЛЯЕТСЯ КАКУЮ ФУНКЦИЮ ЗАПУСКАТЬ
//
function Raschet() {
  if (tip == "45") {
    bbRas();
  }
  if (tip != "45") {
    osnRas();
  }
}

//
//
// НАЧАЛЬНЫЕ ДАННЫЕ ДЛЯ БАЛКОННОГО БЛОКА
//
function bbRas() {
  shirina = document.getElementById('shirina').value;
  visota = document.getElementById('visota').value;
  stvorka1 = document.getElementById('stvorka2').value;
  gluV1 = document.getElementById('bbglu1').value;
  setka1 = document.getElementById('setka').value;
  montaj = eval(document.getElementById('mont').value);
  setka2 = eval(setka1);
  sIzd = eval(shirina) / 1000;
  vIzd = eval(visota) / 1000;
  stvIzdShir = eval(stvorka1) / 1000;
  gluV = eval(gluV1) / 1000;

//Rashod materialov
  ramaIzd1 = (stvIzdShir * 2 + vIzd * 2) + 0.018; //dver'
  ramaIzd2 = ((sIzd - stvIzdShir) * 2 + gluV * 2) + 0.018;//gluhar'
  ramaIzd = ramaIzd1 + ramaIzd2;
  stvorkaIzd = ((stvIzdShir - razStvV) * 2 + (vIzd - razStvV) * 2) + 0.018;
  impostIzd = (stvIzdShir - razStvV - 0.12) + 0.006;
  shtapikStvorka = ((impostIzd - 0.006) * 4 + (vIzd - razStvV - 0.12 - 0.04) * 2) * 0.98;
  shtapikGluhoe = ((sIzd - stvIzdShir - 0.009) * 2 + (gluV - 0.009) * 2) * 0.98;
  soedRama = gluV * sRama;
  rezinaStvorka = stvorkaIzd * 0.99;
  rezinaGluhoe = shtapikGluhoe * 0.99;
  rezinaPritvor = (stvorkaIzd * 2) * 0.98;
  armRama1 = ramaIzd1 - 0.464;
  armRama2 = ramaIzd2 - 0.464;
  armRama = (armRama1 + armRama2) * 0.99;
  armStv = stvorkaIzd - 0.464;
  armImp = impostIzd - 0.01;
  vodootliv = eval(Number(sIzd / 0.5).toFixed(0)) + 2;
  mostChast = 3 * (chastNum + stvorkaNum);
  soedImp = 2;
  samImp580 = 2;
  samImp4225 = 12;
  rihtChast = 18;
  stekloPaketS1 = (((stvIzdShir - razStvV - 0.12) * (vIzd - razStvV - 0.12 - 0.04)) * 0.99) / 2
  stekloPaketS2 = (((stvIzdShir - razStvV - 0.12) * (vIzd - razStvV - 0.12 - 0.04)) * 0.99) / 2
  stekloPaketS = stekloPaketS1 + stekloPaketS2;
  stekloPaketG = ((sIzd - stvIzdShir - 0.09) * (gluV - 0.09)) * 0.99;
  ramkaPaketS = ((stvIzdShir - razStvV - 0.12) * 4 + (vIzd - razStvV - 0.12 - 0.04) * 2) * 0.983;
  ramkaPaketG = ((sIzd - stvIzdShir - 0.09) * 2 + (gluV - 0.09) * 2) * 0.983;
  ugolokPaket = 4 * chastNum * kdist;
  molSitoS = ((stvIzdShir - razStvV - 0.12) * 4 + (vIzd - razStvV - 0.12 - 0.04) * 2) * ksito;
  molSitoG = ((sIzd - stvIzdShir - 0.09) * 2 + (gluV - 0.09) * 2) * ksito;
  germPaketS = ((stvIzdShir - razStvV - 0.12) * 4 + (vIzd - razStvV - 0.12 - 0.04) * 2) * kgerm;
  germPaketG = ((sIzd - stvIzdShir - 0.09) * 2 + (gluV - 0.09) * 2) * kgerm;
  butilPaketS = ((stvIzdShir - razStvV - 0.12) * 4 + (vIzd - razStvV - 0.12 - 0.04) * 2) / 150 * kdist;
  butilPaketG = ((sIzd - stvIzdShir - 0.09) * 2 + (gluV - 0.09) * 2) / 150 * kdist;
  naborBB = ss52502 + ruchkaBB + 10;
  rabotaPaket = chastNum * krab;
  rabotaImp = 3;
  rabotaRama = 5.2 * 2;
  rabotaStvorka = 5.2;
  rabotaFurnitura = 5;
  rfs = (stvIzdShir - razStvV - razFalc) * 1000;
  rfv = (vIzd - razStvV - razFalc) * 1000;
  msNal = 0;
  nadbavkaZaRazmer = -600;
  Raschet2();
}

//
//
// РАСЧЕТ СТАНДАРТНЫХ ОКОН
//
function osnRas() {
  shirina = document.getElementById('shirina').value;
  visota = document.getElementById('visota').value;

  stvorka1 = document.getElementById('stvorka2').value;
  setka1 = document.getElementById('setka').value;
  montaj = eval(document.getElementById('mont').value);
  setka2 = eval(setka1);
  sIzd = eval(shirina) / 1000;
  vIzd = eval(visota) / 1000;
  ploshadIzd = sIzd * vIzd;
  stvIzdShir = eval(stvorka1) / 1000;
  if (gluhoeNum > 0) {
    gluFind = Number((sIzd - stvIzdShir * stvorkaNum) / gluhoeNum).toFixed(4);
  } else {
    gluFind = 0;
  }

// Rashod materialov
  window.ramaIzd = (sIzd * 2 + vIzd * 2) + 0.036;
  window.stvorkaIzd = (((stvIzdShir - razStvS) * 2 + (vIzd - razStvV) * 2) + 0.0) * stvorkaNum
  window.impostIzd = (vIzd - 0.095) * impNum;
  window.shtapikGluhoe = (((gluFind - 0.063 - 0.041) * 2 + (vIzd - 0.126) * 2) * 1.01) * gluhoeNum;
  window.shtapikStvorka = (((stvIzdShir - razStvS - sStv) * 2 + (vIzd - razStvV - sStv) * 2) * 0.97) * stvorkaNum
  window.rezinaGluhoe = (((gluFind - 0.063 - 0.041) * 2 + (vIzd - 0.126) * 2) * 1.01) * gluhoeNum;
  window.rezinaStvorka = (((stvIzdShir - razStvS - sStv) * 2 + (vIzd - razStvV - sStv) * 2) * 0.96) * stvorkaNum
  window.rezinaPritvor = ((((stvIzdShir - razStvS) * 2 + (vIzd - razStvV) * 2) * 0.95) * 2) * stvorkaNum
  window.armRama = ((sIzd - 0.14) * 2 + (vIzd - 0.14) * 2) * 0.99
  window.armStv = (((stvIzdShir - razStvS - sStv) * 2 + (vIzd - razStvV - sStv) * 2) * 0.99) * stvorkaNum
  window.armImp = impostIzd * 0.99;
  window.vodootliv = Number(sIzd / 0.5).toFixed(0);

  window.mostChast = 3 * (chastNum + stvorkaNum)
  window.soedImp = 2 * impNum;
  window.samImp580 = 2 * impNum;
  window.samImp4225 = 12 * impNum;
  window.rihtChast = 6 * chastNum;
  window.stekloPaketG = (((gluFind - 0.045 - 0.021) * (vIzd - 0.088)) * 0.97) * gluhoeNum;
  window.stekloPaketS = (((stvIzdShir - razStvS - razFalc - sStv) * (vIzd - razStvV - razFalc - sStv)) * 0.98) * stvorkaNum
  window.ramkaPaketG = (((gluFind - 0.054 - 0.021) * 2 + (vIzd - 0.088) * 2) * 0.99) * gluhoeNum;
  window.ramkaPaketS = (((stvIzdShir - razStvS - razFalc - sStv) * 2 + (vIzd - razStvV - razFalc - sStv) * 2) * 0.97) * stvorkaNum
  window.ugolokPaket = 4 * chastNum * kdist;
  window.molSitoG = (((gluFind - 0.054 - 0.021) * 2 + (vIzd - 0.088) * 2) * ksito) * gluhoeNum;
  window.molSitoS = (((stvIzdShir - razStvS - razFalc - sStv) * 2 + (vIzd - razStvV - razFalc - sStv) * 2) * ksito) * stvorkaNum
  window.germPaketG = (((gluFind - 0.054 - 0.021) * 2 + (vIzd - 0.088) * 2) * kgerm) * gluhoeNum;
  window.germPaketS = (((stvIzdShir - razStvS - razFalc - sStv) * 2 + (vIzd - razStvV - razFalc - sStv) * 2) * kgerm) * stvorkaNum
  window.butilPaketG = (((gluFind - 0.054 - 0.021) * 2 + (vIzd - 0.088) * 2) / 150 * kdist) * gluhoeNum;
  window.butilPaketS = (((stvIzdShir - razStvS - razFalc - sStv) * 2 + (vIzd - razStvV - razFalc - sStv) * 2) / 150 * kdist) * stvorkaNum
  window.naborBB = 0;
  window.soedRama = 0;
  window.rabotaPaket = krab * chastNum;
  window.rabotaImp = 3 * impNum;
  window.rabotaRama = 5.2;
  window.rabotaStvorka = 5.2 * stvorkaNum;
  window.rabotaFurnitura = 5 * stvorkaNum;
//alert(germPaketG)
  rfs = (stvIzdShir - razStvS - razFalc + koef) * 1000 //razmeri po falcu shirina
  rfv = (vIzd - razStvV - razFalc) * 1000 //razmeri po falcu visota
  msNal = 1;
  //------------Nadbavka za razmer izdelia -------------------//
  if (ploshadIzd >= 0 && ploshadIzd <= 0.3) {
    nadbavkaZaRazmer = 240;
  }
  if (ploshadIzd > 0.3 && ploshadIzd <= 0.7) {
    nadbavkaZaRazmer = 200;
  }
  if (ploshadIzd > 0.7 && ploshadIzd <= 0.9) {
    nadbavkaZaRazmer = 152;
  }
  if (ploshadIzd > 0.9 && ploshadIzd <= 1) {
    nadbavkaZaRazmer = 100;
  }
  if (ploshadIzd > 1 && ploshadIzd <= 2) {
    nadbavkaZaRazmer = 0;
  }
  if (ploshadIzd > 2) {
    nadbavkaZaRazmer = ploshadIzd * 210;
  }
  Raschet2();
}

//
//
// ОБЩИЙ РАСЧЕТ ИЗДЕЛИЙ + ОТДЕЛКА
//
function Raschet2() {
//Rashod furnituri
//nabori

//rS = stvIzdShir-razStvS //razmeri stvorki po shirine
//rV = vIzd-razStvV //razmeri stvorki po visote

  if (rfs >= 310 && rfs <= 600) {
    n2 = ss52660 + petlUglov + Sam4225 * 3
//n3 = ss52444 + petlUglov + Sam4225*3

  }
  if (rfs >= 601 && rfs <= 800) {
    n2 = ss52662 + petlUglov + Sam4225 * 3
//n3 = ss52445 + petlUglov + Sam4225*3

  }
  if (rfs >= 801 && rfs <= 1050) {
    n2 = ss52664 + petlUglov + Sam4225 * 6 + otvPl
//n3 = ss52446 + petlUglov + Sam4225*6 +otvPl

  }
  if (rfs >= 1051 && rfs <= 1300) {
    n2 = ss52666 + petlUglov + Sam4225 * 7 + otvPl
//n3 = ss52447 + petlUglov + Sam4225*6 +otvPl
  }
  if (rfs < 310 || rfs > 1300) {
    n2 = 0
//n3=0
  }

//-------------------------------------------------------
  if (rfv >= 370 && rfv <= 499) {
    n4 = ss52400 + otvPl2 + Sam4225 * 5
    n5 = 0
  }
  if (rfv >= 500 && rfv <= 650) {
    n4 = ss52422
    n5 = ss52432 + otvPl2 + Sam4225 * 5
  }
  if (rfv >= 651 && rfv <= 900) {
    n4 = ss52423 + otvPl + otvPl3 + Sam4225 * 4
    n5 = ss52432 + otvPl2 + Sam4225 * 5
  }
  if (rfv >= 901 && rfv <= 1300) {
    n4 = ss52425 + otvPl + otvPl3 + Sam4225 * 7
    n5 = ss52432 + otvPl2 + Sam4225 * 5
  }
  if (rfv >= 1301 && rfv <= 1800) {
    n4 = ss52426 + otvPl + otvPl3 + Sam4225 * 9
    n5 = ss52432 + otvPl2 + Sam4225 * 5
  }
  if (rfv >= 1801 && rfv <= 2350) {
    n4 = ss52427 + otvPl * 2 + otvPl3 + Sam4225 * 12
    n5 = ss52433 + otvPl2 + Sam4225 * 5
  }
  if (rfv < 370 || rfv > 2350) {
    n4 = 0
    n5 = 0
  }
//------------------------------------------------------------
  if (rfv >= 651 && rfv <= 749) {
    n6 = ss52453 + otvPl + Sam4225 * 3
  }
  if (rfv >= 750 && rfv <= 1050) {
    n6 = ss54674 + otvPl + Sam4225 * 3
  }
  if (rfv >= 1051 && rfv <= 1300) {
    n6 = ss54675 + otvPl + Sam4225 * 5
  }
  if (rfv >= 1301 && rfv <= 1500) {
    n6 = ss54676 + otvPl + Sam4225 * 5
  }
  if (rfv >= 1501 && rfv <= 1750) {
    n6 = ss52457 + otvPl + Sam4225 * 6
  }
  if (rfv >= 1751 && rfv <= 1950) {
    n6 = ss52458 + otvPl * 2 + Sam4225 * 8
  }
  if (rfv >= 1951 && rfv <= 2350) {
    n6 = ss59920 + otvPl * 2 + Sam4225 * 8
  }
  if (rfv < 651 || rfv > 2350) {
    n6 = 0
  }
//-------------------------------------------------------------------
  if (rfs >= 651 && rfs <= 749) {
    n7 = ss52453 + otvPl + Sam4225 * 3
  }
  if (rfs >= 750 && rfs <= 1050) {
    n7 = ss54674 + otvPl + Sam4225 * 3
  }
  if (rfs >= 1051 && rfs <= 1300) {
    n7 = ss54675 + otvPl + Sam4225 * 5
  }
  if (rfs >= 1301 && rfs <= 1500) {
    n7 = ss54676 + otvPl + Sam4225 * 5
  }
  if (rfs >= 1501 && rfs <= 1750) {
    n7 = ss52457 + otvPl + Sam4225 * 6
  }
  if (rfs >= 1751 && rfs <= 1950) {
    n7 = ss52458 + otvPl * 2 + Sam4225 * 8
  }
  if (rfs >= 1951 && rfs <= 2350) {
    n7 = ss52458 + otvPl * 2 + Sam4225 * 8
  }
  if (rfs < 651 || rfs > 2350) {
    n7 = 0
  }
//-----------------------------------------------------------------------------
  if (rfs >= 280 && rfs <= 434) {
    n8 = ss52514 + otvPl + Sam4225 * 5
  }
  if (rfs >= 435 && rfs <= 1650) {
    n8 = ss52513 + otvPl + Sam4225 * 5
  }
  if (rfs < 280 || rfs > 1650) {
    n8 = 0
  }
//---------------------------MONTAJ--------------------------------

  if (tip == "1" || tip == "11" || tip == "2" || tip == "21" || tip == "22") {
    mo = 1500;
  }
  if (tip == "3" || tip == "31" || tip == "32") {
    mo = 2300;
  }
  if (tip == "45") {
    mo = 2560;
  }

  nabor1 = s20201Planet + ss52480 + ss94491 + Sam440 * 11 + ss52483 + ss52478 + ss40005 + naborBB //petli
  nabor2 = n2 //maco-nojnici provetrivaniya
//nabor3 = n3 //maco-nojnici
  nabor4 = n4 //maco-osnZapor
  nabor5 = n5 //maco-otkidZapor
  nabor6 = n6 //maco-sredZapVertik
  nabor7 = n7 //maco-sredZapGoriz
  nabor8 = n8 //maco-uglovPeredacha

// Stoimost
  window.Prama = ramaIzd * rama;
  window.Pimpost = impostIzd * impost;
  window.Pstvorka = stvorkaIzd * stvorka
  window.Pshtapik = shtapikGluhoe * shtapik + shtapikStvorka * shtapik
  window.PVH = Prama + Pimpost + Pshtapik + Pstvorka + soedRama
  window.ParmRama = armRama * arm;
  window.ParmStv = armStv * arm
  window.ParmImp = armImp * armIMP;
  window.METALL = ParmRama + ParmImp + ParmStv

  window.Pkashirovka_rama = (ramaIzd * ramaT_Ulica * kashir_ul + ramaIzd * ramaT_Kvartira * kashir_kv) * Pkashir;
  window.Pkashirovka_stvorka = (stvorkaIzd * stvorkaT_Ulica * kashir_ul + stvorkaIzd * stvorkaT_Kvartira * kashir_kv) * Pkashir;
  window.Pkashirovka_impost = (impostIzd * impostT_ulica * kashir_ul + impostIzd * impostT_kvartira * kashir_kv) * Pkashir;
  window.Pkashirovka_komplekt = kashir_ruchka + kashir_petli + kashir_vodootl * vodootliv + kashir_marker;
  window.Pkashirovka_Final = Pkashirovka_rama + Pkashirovka_stvorka + Pkashirovka_impost + Pkashirovka_komplekt;

  window.PrezinaS = rezinaGluhoe * rezS + rezinaStvorka * rezS
  window.PrezinaP = rezinaPritvor * rezU
  window.REZINA = PrezinaS + PrezinaP

  window.PmostChast = mostChast * most;
  window.PsoedImp = soedImp * soedIMP;
  window.Pvodoot = vodootliv * s417022 //+vodootlivS*s417022 -vrode ne schitaem?
  window.PrihtChast = rihtChast * riht;
  window.PsamImp4225 = samImp4225 * Sam4225;
  window.PsamImp580 = samImp580 * Sam580 * est;
  window.PSam3916 = ((sIzd * 2 + vIzd * 2) / 0.3 + impostIzd / 0.3 * 2) * Sam3916;
  window.SBOROCHNIE = Pvodoot + PsoedImp + PmostChast + PrihtChast + PsamImp4225 + PsamImp580 + PSam3916;

  window.PramkaPaket = ramkaPaketG * ramka * kdist + ramkaPaketS * ramka * kdist
  window.PbutilPaket = butilPaketG * butil + butilPaketS * butil
  window.PgermPaket = germPaketG * germ + germPaketS * germ
  window.PmolSito = molSitoG * sito + molSitoS * sito
  window.PstekloPaket = stekloPaketG * st1 + stekloPaketG * st2 + stekloPaketG * st3 + stekloPaketS * st1 + stekloPaketS * st2 + stekloPaketS * st3;
  window.PstekloTonirovka = (stekloPaketG + stekloPaketS) * tonir * Ptonir;
  window.PugolokPaket = ugolokPaket * ugolok;
// Наценка 10%
  window.ZAPOLNENIE = (PramkaPaket + PbutilPaket + PgermPaket + PmolSito + PstekloPaket + PugolokPaket + PstekloTonirovka) * 1, 1;

  window.FURNITURA = (nabor1 + nabor2 + nabor4 + nabor5 + nabor6 + nabor7 + nabor8) * stvorkaNum

  window.PrabotaPaket = rabotaPaket * rabSP;
  window.PrabotaRama = rabotaRama * rabPVH;
  window.PrabotaStvorka = rabotaStvorka * rabPVH
  window.PrabotaImp = rabotaImp * rabPVH;
  window.PrabotaFurnitura = rabotaFurnitura * rabPVH;
  window.RABOTA = PrabotaPaket + PrabotaRama + PrabotaImp + PrabotaStvorka + PrabotaFurnitura;

  PramaMS = ((stvIzdShir - ramaMs) * 2 + (vIzd - ramaMv) * 2) * PramaM;
  PimpMS = (stvIzdShir - impM) * PimpM;
  PpolotnoMS = ((stvIzdShir - polotnoMs) * (vIzd - polotnoMv)) * PpolotnoM;
  PshnurMS = ((stvIzdShir - ramaMs) * 2 + (vIzd - ramaMv) * 2 * 1.01) * PshnurM;
  PkomplectMS = krepM * PkrepM + krepI * PkrepI + ruchkaM * PruchkaM + ugolokM * PugolokM;
  MOSKITKA = (PramaMS + PimpMS + PpolotnoMS + PshnurMS + PkomplectMS + 80) * stvorkaNum * setka2 * msNal;

  MONTAJ = eval(mo * montaj);

//-------------OTDELKA--------------------//
//---podokonnik
  o1 = document.getElementById('otkos').value;
  w = document.getElementById('podok').value;
  if (w == "1") {
    Pw = (sIzd + 0.2) * eval('pod' + o1) + 400;
  } else {
    Pw = 0;
  }
//---otkos
  o3 = document.getElementById('otkos_t').value;
  o2 = eval('otd' + o1);
  if (o3 == "0") {
    o = o2 * 0;
  }
  if (o3 == "1") {
    o = o2;
  }
  if (o3 == "2") {
    o = o2 + 500;
  }
//---sliv
  s1 = eval(document.getElementById('sliv').value);
//---ugolok
  u1 = document.getElementById('ugolok').value;
  if (o1 == "1") {
    s = (sIzd + 0.2) * (s1 * 1.16);
    u = (vIzd * 2 + sIzd) * 115 * u1;
  }
  if (o1 == "0") {
    s = (sIzd + 0.2) * s1;
    u = (vIzd * 2 + sIzd) * 70 * u1;
  }
  OTDELKA = eval(Number(Pw + o + s + u).toFixed(0));
//-------------end OTDELKA-----------------//


  ITOGizd = (
    PVH + METALL + REZINA + SBOROCHNIE + ZAPOLNENIE
    + FURNITURA + RABOTA + MOSKITKA + Pkashirovka_Final + nadbavkaZaRazmer
  ) * document.getElementById('coeff').value; // 0.98 - коэфициент корректировки цены! менять после изменения цен для подгонки
  ITOGO = eval(Number(ITOGizd).toFixed(0));

  //Посчитаем выгоду с учетом скидки
  let discount = getDiscont();
  let windowPriceDiscount = (ITOGO * discount).toFixed(0);

  //Расчитаем цену изделия с учетом скидки
  ITOGO = eval((ITOGO - (ITOGO * discount)).toFixed(0));
  //Итоговая цена = Изделие + Монтаж + Отделка
  ItogoObchaya = ITOGO + MONTAJ + OTDELKA;

//
//
// ВЫВОДИМЫЕ ДАННЫЕ
  //Показываем процент скидки
  document.getElementById('discount-percent').innerHTML = (discount * 100).toFixed(0);
  //Показываем значение скидки
  document.getElementById('discount-value').innerHTML = windowPriceDiscount;
  //Цена за изделие
  document.getElementById('result').innerHTML = ITOGO;
  //Цена за монтаж
  document.getElementById('montCena').innerHTML = MONTAJ;
  //Цена за отделку
  document.getElementById('OTD').innerHTML = OTDELKA;
  //Итоговая цена Изделие + Монтаж + Отделка
  document.getElementById('Cena').innerHTML = ItogoObchaya;

//-----------dlya CHEKERA--------------//
//рама
  document.getElementById('my_rama_k').innerHTML = Number(ramaIzd).toFixed(2);
  document.getElementById('my_rama_s').innerHTML = Number(Prama).toFixed(2);
//створка
  document.getElementById('my_stvorka_k').innerHTML = Number(stvorkaIzd).toFixed(2);
  document.getElementById('my_stvorka_s').innerHTML = Number(Pstvorka).toFixed(2);
//импост
  document.getElementById('my_impost_k').innerHTML = Number(impostIzd).toFixed(2);
  document.getElementById('my_impost_s').innerHTML = Number(Pimpost).toFixed(2);
//штапик
  document.getElementById('my_shtapik_k').innerHTML = Number(shtapikGluhoe + shtapikStvorka).toFixed(2);
  document.getElementById('my_shtapik_s').innerHTML = Number(Pshtapik).toFixed(2);
// ПВХ
  document.getElementById('my_PVH_s').innerHTML = Number(PVH).toFixed(2);
//металл рама
  document.getElementById('my_impost_k').innerHTML = Number(impostIzd).toFixed(2);
  document.getElementById('my_impost_s').innerHTML = Number(Pimpost).toFixed(2);
//металл створка
  document.getElementById('my_shtapik_k').innerHTML = Number(shtapikGluhoe + shtapikStvorka).toFixed(2);
  document.getElementById('my_shtapik_s').innerHTML = Number(Pshtapik).toFixed(2);
// МЕТАЛЛ
  document.getElementById('my_PVH_s').innerHTML = Number(PVH).toFixed(2);
//--------end CHEKER-------------------//
}

/**
 * Получение размера скидки на основе выбранного монтажа
 */
function getDiscont() {
  //Монтаж
  let install = document.getElementById('mont').value;
  //Подоконник
  let still = document.getElementById('podok').value;
  //Москитная сетка
  let grid = document.getElementById('setka').value;
  //Отделка откосов
  let decking = document.getElementById('otkos_t').value;
  //Наружний слив
  let drain = document.getElementById('sliv').value;
  //Уголок наружний
  let corner = document.getElementById('ugolok').value;

  //20% - если выбран монтаж, подоконник, отделка откосов, наружний слив, уголок наружний
  if (install > 0 && still > 0 && decking > 0 && drain > 0 && corner > 0) {
    return 0.2;
  }
  //15% - если выбран монтаж, подоконник и отделка откосов.
  if (install > 0 && still > 0 && decking > 0) {
    return 0.15;
  }
  //10% - на все остальное
  return 0.1
}

//
//
// ИНФОРМАЦИЯ ПО ПРОФИЛЯМ
//

// --------------------- END PROFIL INFO ----------------------------- //
function browserDetectJS() {

  var an = navigator.appName;
  if (an == "Microsoft Internet Explorer") {
    alert("Вы используете обозреватель " + an + ". Для корректного расчета стоимости необходимо использовать обозреватель: Opera, Chrome или Mozilla FireFox, которые можно скачать в конце страницы. Спасибо!")
    document.getElementById('broz').focus();
  }
}


// ------------------------- ДЛЯ ЭКСПЕРИМЕНТОВ/ТЕСТОВ ---------------------------------------//
