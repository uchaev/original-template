'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (window, $, undefined) {
    'use strict';

    var App = function () {
        function App() {
            _classCallCheck(this, App);

            this.stack = [];
            this.started = false;
            this.sorted = false;
        }

        _createClass(App, [{
            key: 'use',
            value: function use(scope, context) {
                var _this = this;

                scope = App.normalizeScope(scope);

                if (!this.sorted) {
                    this.stack.sort(function (a, b) {
                        if (a.order === b.order) {
                            return 0;
                        }
                        return a.order > b.order ? 1 : -1;
                    });

                    this.sorted = true;
                }

                this.stack.filter(function (stack) {
                    return scope.some(function (scope) {
                        return stack.scope.indexOf(scope) > -1;
                    });
                }).map(function (stack) {
                    return stack.callback.call(_this, context, scope);
                });
            }
        }, {
            key: 'bind',
            value: function bind(scope, callback, order) {
                if (order === undefined) {
                    if (callback === undefined) {
                        callback = scope;
                        scope = ['default'];
                    } else if (typeof callback === 'number') {
                        order = callback || 0;
                        callback = scope;
                        scope = ['default'];
                    }
                }

                if (order === undefined) {
                    order = 0;
                }

                scope = App.normalizeScope(scope);

                this.stack.push({ scope: scope, callback: callback, order: order });
                this.sorted = false;
            }
        }, {
            key: 'start',
            value: function start() {
                var _this2 = this;

                if (this.started !== false) {
                    return;
                }

                this.started = true;

                $(function () {
                    _this2.use('default');
                });
            }
        }], [{
            key: 'normalizeScope',
            value: function normalizeScope(scope) {
                if (typeof scope === 'string') {
                    scope = scope.split(' ').map(function (scope) {
                        return scope.trim();
                    });
                }

                if (scope.length === 0) {
                    scope = ['default'];
                }

                return scope;
            }
        }]);

        return App;
    }();

    window.app = new App();
}(window, jQuery);
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

(function ($, window, undefined) {
  var errorMessages = {
    valueMissing: {
      default: 'Значение не должно быть пустым',
      email: 'Введите почту',
      phone: 'Введите номер телефона',
      terms: 'Вы должны согласиться с условями',
      size: 'Выберите размер',
      promocode: 'Введите промокод',
      name: 'Введите имя',
      firstname: 'Введите имя',
      lastname: 'Введите фамилию',
      password: 'Введите пароль',
      comment: 'Оставьте комментарий',
      rating: 'Поставьте оценку',
      number: 'Укажите число'
    },
    typeMismatch: {
      default: 'Неверный формат',
      phone: 'Неверный формат телефона',
      email: 'Неверный формат почты'
    },
    tooShort: {
      default: 'Недостаточно символов',
      password: 'Не менее 6 символов',
      password_new: 'Не менее 6 символов',
      password_old: 'Не менее 6 символов'
    },
    tooLong: {
      default: 'Слишком длинное значение'
    },
    rangeOverflow: {
      default: 'Значение больше максимального'
    },
    rangeUnderflow: {
      default: 'Значение меньше минимального'
    }
  };

  var processRequestError = function processRequestError($form, errors) {
    Object.keys(errors).map(function (field) {
      var formTag = $form[0].tagName;
      var message = errors[field];
      var $fields = formTag == 'INPUT' || formTag == 'TEXTAREA' ? $form : $form.find('[name="' + field + '"], [name^="' + field + '["]');

      if ($form.attr('id')) {
        $fields = $fields.add('[name="' + field + '"][form="' + $form.attr('id') + '"], [name^="' + field + '["][form="' + $form.attr('id') + '"]');
      }

      $fields.each(function (i, input) {
        var isToggle = input.type == 'checkbox' || input.type == 'radio';
        var $input = $(input);
        var $field = isToggle ? $input.closest('.toggle') : $input.closest('.field');
        var group = $input.data('fieldset');

        if (isToggle) {
          $input.data({ valid: false }).trigger('invalid');
        }

        if (group) {
          var $group = $input.parents('.fieldset[data-fieldset="' + group + '"]');
          var $error = $group.find('.fieldset__error');
          message = errorMessages.valueMissing[group] || message;

          if (!$error.length) {
            $error = $('<div class="fieldset__error"></div>').appendTo($group);
            $error[0].offsetWidth;
          }
          $error.html(message);
          $group.addClass('is-error');
        } else if (!isToggle) {
          var _$error = $field.find('.field__error');
          if (!_$error.length) {
            _$error = $('<span class="field__error" />');
            $field.append(_$error);
            _$error[0].offsetWidth;
          }
          _$error.html(message);
        }

        $field.removeClass('is-ok').addClass('is-error');
      });

      if (formTag == 'FORM') {
        var $submit = $form.find('[type="submit"]');
        $form.off('.catch-change').on('valid.catch-change invalid.catch-change', function (e) {
          $submit.removeClass('is-error');
        });
        $submit.addClass('is-error');
      }
    });
  };

  var testInput = function testInput(input) {
    var error = {};

    // console.log(input.name, input.disabled, input.value, input.dataset && input.dataset.isInputmask == "true" ? 'inputmask' : '', input.validity.valid);
    if (input.disabled) {
      // Что-то делать или нет?
      console.log(input.name, 'disabled');
    } else if (input.validity.valueMissing) {
      error[input.name] = errorMessages.valueMissing[input.name || input.type] || errorMessages.valueMissing['default'];
      // Поле не заполнено
    } else if (input.value.length && (input.type == 'email' || input.name == 'email') && !Inputmask.isValid(input.value.toLowerCase(), { alias: 'email' })) {
      // Слишком не email
      error[input.name] = errorMessages.typeMismatch['email'] || errorMessages.typeMismatch['default'];
    } else if (input.dataset && input.dataset.isInputmask == 'true' && !$(input).inputmask('isComplete')) {
      error[input.name] = errorMessages.typeMismatch[input.name] || errorMessages.typeMismatch['default'];
    } else if (input.validity.tooShort || input.minlength && input.value.length < input.minlength) {
      // Слишком короткий
      error[input.name] = errorMessages.tooShort[input.name] || errorMessages.tooShort['default'];
    } else if (input.validity.tooLong) {
      // Слишком длинный
      error[input.name] = errorMessages.tooLong[input.name] || errorMessages.tooLong['default'];
    } else if (input.validity.typeMismatch) {
      // Работает только на email и url, лучше использовать для этого inputmask
      error[input.name] = errorMessages.typeMismatch[input.name] || errorMessages.typeMismatch[input.type] || errorMessages.typeMismatch['default'];
    } else if (input.validity.rangeOverflow) {
      // Число больше максимального
      error[input.name] = errorMessages.rangeOverflow[input.name] || errorMessages.rangeOverflow['default'];
    } else if (input.validity.rangeUnderflow) {
      // Число меньше минимального
      error[input.name] = errorMessages.rangeUnderflow[input.name] || errorMessages.rangeUnderflow['default'];
    }

    return error;
  };

  $.fn.formClearError = function () {
    this.find('.field.is-error, .toggle.is-error, .fieldset.is-error').removeClass('is-error');
    // this.find(':checkbox, :radio').each((inputIndex, index) => {
    //     input.setCustomValidity('');
    //     input.reportValidity();
    // });
  };

  $.fn.formError = function () {
    var errors = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    processRequestError(this, errors);
  };

  $.fn.inputValidate = function () {
    var errors = testInput(this[0]);
    processRequestError(this, errors);
    return errors;
  };

  $.fn.formValidate = function () {
    this.formClearError();
    var $inputs = this.find('input, textarea');
    var errors = {};

    $inputs.each(function (inputIndex, input) {
      errors = _extends({}, errors, testInput($inputs.eq(inputIndex)[0]));
    });

    // processRequestError(this, errors);
    processRequestError(this, errors);
    return errors;
  };

  $.fn.form = function () {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        successCallback = _ref.success,
        errorCallback = _ref.error,
        beforeSendCallback = _ref.beforeSend,
        completeCallback = _ref.complete;

    this.each(function (i, form) {
      var $form = $(form);
      var lock = void 0;

      $form.off('.form').on('submit.form', function (e) {
        e.preventDefault();

        var validateErrors = $form.formValidate();

        if (lock || !$.isEmptyObject(validateErrors)) {
          return;
        }

        lock = true;
        var data = void 0;
        var $files = $form.find('[type="file"]');

        if ($files.length) {
          data = new FormData(form);
          $files.each(function (i, input) {
            data.append(input.name, input);
          });
        } else {
          data = $form.serialize();
        }

        var $submit = $form.find('button[type=submit]').prop('disabled', true);

        $form.addClass('is-loading').formClearError();

        $.ajax({
          url: $form.attr('action'),
          type: $form.attr('method') || 'post',
          dataType: 'json',
          data: data,
          contentType: data instanceof FormData ? false : 'application/x-www-form-urlencoded',
          processData: !(data instanceof FormData),
          beforeSend: function beforeSend(xhr, settings) {
            if (beforeSendCallback) {
              beforeSendCallback.call(null, xhr, form, settings);
            }
          },
          success: function success(response) {
            if (successCallback) {
              successCallback.call(null, response, form);
            }
          },
          error: function error(xhr) {
            app.processError(xhr, {
              request: function request(errors) {
                return processRequestError($form, errors);
              }
            }).then(errorCallback || function () {});
          },
          complete: function complete(response) {
            lock = false;
            $form.removeClass('is-loading');
            $submit.prop('disabled', false);

            if (completeCallback) {
              completeCallback.call(null, response, form);
            }
          }
        });
      });
    });
  };
})(jQuery, window);
'use strict';

(function ($) {
    $.fn.inputShadow = function () {
        return $(this).each(function (parent_i, parent) {
            var $parent = $(parent);
            var $input = $parent.find('input, textarea, [contenteditable], select');
            var isRequired = $input.prop('required') || false;
            var isContenteditable = $input.is('[contenteditable]');
            var errorRemoveEvent = $parent.data('error-remove');

            if (isContenteditable) {
                var $hiddenInput = $('<input type="text" name="' + $input.attr('name') + '" value="' + $input.html().trim().replace(/<div>/gi, '<br>').replace(/<\/div>/gi, '').replace(/<br>/gi, '\n') + '"/>').appendTo($parent);
                $hiddenInput.hide().prop({
                    disabled: $input.attr('disabled'),
                    required: $input.attr('required')
                });
            }

            $parent.removeClass('is-focus is-filled').toggleClass('is-required', isRequired);
            $input.off('.input-shadow');

            if ($input.is('select')) {
                var $options = $input.find('option');
                var selectedVal;
                $input.on({
                    'update.input-shadow': function updateInputShadow(e) {
                        $options = $input.find('option');
                        $input.trigger('changeSilent');
                    },
                    'change.input-shadow changeSilent.input-shadow': function changeInputShadowChangeSilentInputShadow(e, notUser) {
                        if ($input.is(':disabled')) {
                            $parent.addClass('is-disabled');
                        } else {
                            $parent.removeClass('is-disabled');
                        }

                        selectedVal = $options.filter(':selected').val();

                        if (selectedVal !== '' && selectedVal !== '00') {
                            $parent.addClass('is-filled');

                            if (!notUser && $parent.hasClass('is-error')) {
                                $parent.removeClass('is-error');
                            }
                        } else {
                            $parent.removeClass('is-filled');

                            if (!notUser && !isRequired && $parent.hasClass('is-error')) {
                                $parent.removeClass('is-error');
                            }
                        }
                    }
                });
            } else if ($input.is(':checkbox, :radio')) {
                var group = $input.data('fieldset');
                var $group = group ? $parent.parents('[js-inputShadow-fieldset][data-fieldset="' + group + '"]').off('.input-shadow') : $();
                var $groupInputs = $group.length ? $group.find('input') : $();
                $input.on({
                    'change.input-shadow': function changeInputShadow(e) {
                        if ($.isEmptyObject($input.inputValidate())) {
                            $parent.removeClass('is-error');
                            $input.data({ valid: true }).trigger('valid');
                        } else {
                            $parent.addClass('is-error');
                            $input.data({ valid: false }).trigger('invalid');
                        }
                    }
                });
                if ($input.is(':radio')) {
                    var name = $input.attr('name');
                    $group.on({
                        'valid.input-shadow': function validInputShadow(e) {
                            $groupInputs.filter('[name="' + name + '"]').data({ valid: true });
                        },
                        'invalid.input-shadow': function invalidInputShadow(e) {
                            $groupInputs.filter('[name="' + name + '"]').data({ valid: false });
                        }
                    });
                }
                $group.on({
                    'valid.input-shadow invalid.input-shadow': function validInputShadowInvalidInputShadow(e) {
                        if ($groupInputs.filter(function (inputIndex, input) {
                            return $groupInputs.eq(inputIndex).data('valid') === false;
                        }).length) {
                            $group.addClass('is-error');
                        } else {
                            $group.removeClass('is-error');
                        }
                    }
                });
            } else {
                $input.on({
                    'clear.input-shadow': function clearInputShadow(e) {
                        // e.stopPropagation();
                        $parent.removeClass('is-error is-ok is-valid');
                        $input.val(null).trigger('changeSilent');
                    },
                    'focus.input-shadow': function focusInputShadow(e) {
                        $parent.addClass('is-focus');
                        if (errorRemoveEvent == 'focus') {
                            $parent.removeClass('is-error');
                        }
                        if ($.fn.inputmask && $input.data('inputmask-showmaskonfocus') && $input.inputmask("hasMaskedValue")) {
                            $parent.addClass('is-filled');
                        }
                    },
                    'blur.input-shadow': function blurInputShadow(e) {
                        var $targetInput = isContenteditable ? $hiddenInput : $input;
                        $parent.removeClass('is-focus');
                        if (!$.isEmptyObject($targetInput.inputValidate())) {
                            $parent.removeClass('is-ok is-valid');
                            $targetInput.data({ valid: false }).trigger('invalid');
                        } else {
                            $parent.addClass('is-valid');
                            $targetInput.data({ valid: true }).trigger('valid');
                        }
                    },
                    'keyup.input-shadow': function keyupInputShadow(e) {
                        if (isContenteditable) {
                            $input.trigger('changeSilent');
                        }
                    },
                    'input.input-shadow change.input-shadow changeSilent.input-shadow': function inputInputShadowChangeInputShadowChangeSilentInputShadow(e, notUser) {
                        // $parent.removeClass('is-autofilled');
                        if ($input.is(':disabled')) {
                            $parent.addClass('is-disabled');
                        } else {
                            $parent.removeClass('is-disabled');
                        }

                        if ($input.val().length > 0 || isContenteditable && $input.text().length > 0 || $.fn.inputmask && $input.inputmask("unmaskedvalue").length > 0) {
                            $parent.addClass('is-filled');
                            if ($parent.hasClass('is-error') && e.type !== 'change' && !notUser) {
                                $parent.removeClass('is-error');
                            }
                        } else if ($.fn.inputmask && !$input.inputmask("hasMaskedValue")) {
                            $parent.removeClass('is-filled');
                        }

                        if (e.type !== 'change' && !notUser) {
                            $parent.removeClass('is-ok is-valid');
                        }

                        if (isContenteditable) {
                            $hiddenInput.val($input.html().trim().replace(/<div>/gi, '<br>').replace(/<\/div>/gi, '').replace(/<br>/gi, '\n'));
                        }

                        if (notUser && $.fn.inputmask && $input.inputmask("unmaskedvalue").length == 0) {
                            $parent.removeClass('is-filled');
                        }
                    },
                    'maskIncomplete': function maskIncomplete(e) {
                        $input.inputValidate();
                        if ($input.inputmask("unmaskedvalue").length == 0) {
                            $parent.removeClass('is-filled');
                        }
                    }
                });
            }

            $input.trigger('changeSilent', true);
        });
    };
})(jQuery);
'use strict';

(function () {

    'use strict';

    var Layer;
    var $document, $window, $body, $bodyWrapper, $overlay, overlayAllowed;

    var initialized,
        opened = [],
        bodyScroll,
        storage = {};

    function layerAlert(_ref) {
        var title = _ref.title,
            content = _ref.content,
            _ref$options = _ref.options,
            options = _ref$options === undefined ? {} : _ref$options;

        return new Promise(function (resolve, reject) {
            var $layer = $('\n                 <div js-overlayer class="layer layer_overlay layer_alert" data-id="flash/error">\n                    <div class="layer__inside">\n                        <a js-close-layer class="layer-close" role="button" href="#"></a>\n\n                        ' + (title ? '\n                            <div class="layer__header">\n                                <div class="layer__title">' + title + '</div>\n                            </div>\n                        ' : '') + '\n                        <div class="layer__content">\n                            <div class="layer__description">' + content + '</div>\n                            <button js-close-layer class="button button_blue button_wide"><span class="button__content"><span class="button__title">\u0417\u0430\u043A\u0440\u044B\u0442\u044C</span></span></button>\n                        </div>\n                    </div>\n                </div>\n            ');
            var layer = new Layer($layer[0], {
                isOverlayer: true,
                afterOpen: function afterOpen($layer, layerData) {
                    $layer.find('[js-close-layer]').focus();
                    options.afterOpen && options.afterOpen.call($layer, layerData);
                    if (options.closeOthers) {
                        opened.forEach(function (openItem, openIndex) {
                            if (openItem.id !== 'flash/error') {
                                openItem.close();
                            }
                        });
                    }
                },
                afterClose: function afterClose($layer, layerData) {
                    $layer.remove();
                    resolve(layer);
                    options.afterClose && options.afterClose.call($layer, layerData);
                }
            });
            layer.open();
        });
    }

    function initialize() {
        if (initialized) return;
        initialized = true;

        $document = $(document);
        $window = $(window);
        $body = $('body');
        $bodyWrapper = $('.body__wrapper');

        overlayAllowed = true;

        if (overlayAllowed) {
            $overlay = $('<div class="body__overlay"></div>').appendTo($body);

            $overlay.on('click', function (e) {
                e.preventDefault();
                if (opened && !opened[opened.length - 1].options.disableOverlayClose) {
                    opened[opened.length - 1].close();
                }
            });
        }

        $document.on('click', '[js-open-layer]', function (e) {
            e.preventDefault();
            e.stopPropagation();
            open($(this).attr('js-open-layer'));
        });

        $document.on('click', '[js-toggle-layer]', function (e) {
            e.preventDefault();
            e.stopPropagation();
            toggle($(this).attr('js-toggle-layer'));
        });
    }

    function findLayer(id) {
        var layer = storage[id];
        if (!layer) {
            layer = new Layer($('.layer[data-id="' + id + '"]'));
            storage[layer.id] = layer;
        }
        return layer;
    }

    function open(id, data) {
        return findLayer(id).open(data);
    }

    function toggle(id) {
        var layer = findLayer(id);
        if (layer.opened) {
            layer.close();
        } else {
            layer.open();
        }
    }

    function close(id, data, delay) {
        if (!id && opened.length) {
            return findLayer(opened[opened.length - 1].id).close(delay, data);
        } else if (id) {
            return findLayer(id).close(delay, data);
        }
    }

    function showOverlay() {
        if (overlayAllowed) {
            $overlay.addClass('is-opened');
        }
    }

    function hideOverlay() {
        if (overlayAllowed) {
            $overlay.removeClass('is-opened');
        }
    }

    function lockBody() {
        $bodyWrapper.css('top', -(bodyScroll = $document.scrollTop())).addClass('is-locked');
    }

    function unlockBody() {
        $bodyWrapper.css('top', 'auto').removeClass('is-locked');
        $document.scrollTop(bodyScroll);
    }

    function hideBody(isOverlayer) {
        $bodyWrapper.addClass(isOverlayer ? 'is-overlay' : 'is-hide').removeClass(isOverlayer ? 'is-hide' : '');
    }

    function showBody(isOverlayer) {
        $bodyWrapper.removeClass(isOverlayer ? 'is-overlay is-hide' : 'is-hide');
    }

    Layer = function Layer($layer, options) {
        options = $.extend({}, options || {});

        $.extend(this, {
            options: options,
            id: null,
            initialized: false,
            opened: false,
            $origin: null,
            $layer: null
        });

        this.initialize($layer);
    };

    $.extend(Layer.prototype, {
        initialize: function initialize($layer) {
            if (this.initialized) return;
            this.initialized = true;
            $layer = $($layer);
            if (!$layer.length) {
                throw new Error('Layer not found');
            }

            this.$origin = this.$layer = $layer.detach();
            this.id = $layer.data('id');

            $layer.addClass('is-initialized');

            $layer.on('layer-close', function () {
                this.close();
            }.bind(this));

            $layer.on('click', '.layer-close, .layer-back, [js-close-layer]', function (e) {
                if (!$(e.currentTarget).attr('href') || $(e.currentTarget).attr('href') == '#') {
                    e.preventDefault();
                    this.close($(e.currentTarget).data('close-delay') || 0);
                }
            }.bind(this));
        },
        open: function open(data) {
            var _this = this;

            return new Promise(function (resolve, reject) {
                if (_this.options.clone) {
                    _this.$layer = _this.$origin.clone(true, true);
                }

                var $layer = _this.$layer,
                    $header = $layer.find('.layer-header'),
                    top = opened[opened.length - 1];
                $layer.css('top', 0);

                $window.trigger('layerOpen', [_this.$layer, _this.id]);
                if (_this.options.beforeOpen) {
                    _this.options.beforeOpen.call(_this, _this.$layer, data);
                }

                if (opened.length) {
                    top.scrollTop = $window.scrollTop();
                    // top.scrollTop = Math.abs(top.$layer.offset().top);
                    top.$layer.css('top', -top.scrollTop);
                    opened.forEach(function (layer) {
                        layer.$layer.addClass('is-faded');
                    });
                } else {
                    lockBody();
                }

                opened.push(_this);
                _this.opened = true;

                if (_this.options.isOverlayer) {
                    showOverlay();
                    $overlay.removeClass('is-complete');
                } else {
                    hideOverlay();
                    hideBody();
                }

                setTimeout(function () {
                    var _this2 = this;

                    $body.append($layer.addClass('is-animating'));

                    if (this.options.aside) {
                        $layer.addClass('is-aside');
                    }

                    $layer[0].offsetHeight;
                    $layer.addClass('is-opened');

                    $layer.one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', function (e) {
                        e.stopPropagation();
                        e.stopImmediatePropagation();
                        $layer.removeClass('is-animating');

                        if (this.options.isOverlayer) {
                            $overlay.addClass('is-complete');
                        }
                    }.bind(this));

                    if ($header.length) {
                        $layer.css('padding-top', $header.outerHeight());
                    }

                    $document.scrollTop(0);

                    // if ( $.fn.inputmask ) {
                    //     $layer.find('[data-inputmask]').inputmask();
                    // }

                    if (this.options.afterOpen) {
                        this.options.afterOpen.call(this, this.$layer, data);
                    }

                    resolve(this);

                    if (!this.options.disableOverlayClose) {
                        $document.on('keyup.layer-close-' + this.id, function (e) {
                            if (e.keyCode == 27) {
                                // ESC
                                _this2.close();
                            }
                        });
                    }
                    $window.trigger('layerOpened', [this.$layer, this.id]);
                }.bind(_this), 10);
            });
        },

        close: function close(delay, data) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {

                opened = opened.filter(function (layer) {
                    return layer !== this;
                }, _this3);
                var $layer = _this3.$layer;
                $layer.css('top', -$window.scrollTop());
                $layer.addClass('is-closing');

                setTimeout(function () {
                    var top = opened[opened.length - 1];

                    $window.trigger('layerClose', [this.$layer, this.id]);
                    if (this.options.beforeClose) {
                        this.options.beforeClose.call(this, $layer, data, top);
                    }
                    $document.off('.layer-close-' + this.id);

                    if (top && top.$layer.hasClass('is-faded')) {
                        top.$layer.css('top', 0).removeClass('is-faded');
                        $window.scrollTop(top.scrollTop);
                    }

                    $layer.addClass('is-animating').removeClass('is-opened');

                    if (!opened.length || this.options.isOverlayer) {
                        showBody();
                    }

                    if (top && this.options.isOverlayer) {
                        showOverlay();
                    } else if (this.options.isOverlayer) {
                        hideOverlay();
                    }

                    setTimeout(function () {
                        $layer.removeClass('is-animating').removeClass('is-closing').detach();
                        this.opened = false;

                        if (!opened.length) {
                            unlockBody();
                        }

                        if (this.options.afterClose) {
                            this.options.afterClose.call(this, $layer, data, top);
                        }

                        resolve(this);
                        $window.trigger('layerClosed', [this.$layer, this.id]);
                    }.bind(this), 300);
                }.bind(_this3), delay || 0);
            });
        }
    });

    $.extend(Layer, {
        alert: layerAlert,
        open: open,
        close: close,
        toggle: toggle,
        closeAll: function closeAll() {
            opened.forEach(function (open) {
                open.close();
            });
        }
    });

    $(function () {
        initialize();
    });

    window.Layer = Layer;

    $.fn.layer = function (options) {
        setTimeout(function () {
            this.each(function () {
                var layer = new Layer(this, options);
                storage[layer.id] = layer;
            });
        }.bind(this), 10);
    };
})();
'use strict';

!function () {
    function showError(message) {
        Layer.alert({ content: message });
    }

    function internalError(message) {
        showError(message || 'Произошел технический сбой');
    }

    app.errorHandler = function (xhr) {
        app.processError(xhr);
    };

    app.processError = function (xhr) {
        var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
            query = _ref.query,
            request = _ref.request;

        return new Promise(function (resolve, reject) {
            if (xhr.statusText == 'abort') {
                reject({ error: 'abort' });
            }

            var error = xhr.responseJSON.error;

            if (400 === xhr.status) {
                if (error.query) {
                    if (query) {
                        query(error.query);
                    } else {
                        internalError(error.message);
                    }
                } else if (error.request) {
                    if (request) {
                        request(error.request);
                    } else {
                        internalError(error.message);
                    }
                } else {
                    internalError(error.message);
                }
            } else if (401 === xhr.status) {} else if ([403, 404].includes(xhr.status)) {
                var _error = xhr.responseJSON.error;
                internalError(_error.message);
            } else {
                internalError();
            }

            resolve(error);
        }).catch(function (response) {
            console.log(response);
        });
    };
}();
'use strict';

(function ($) {
    var $window = $(window);
    var $HB;
    var windowResizeTimer;
    var $targets = $();
    var $togglers = $();
    var defaults = {
        scrollInView: 'close'
    };

    $window.on('resize', function (e) {
        clearTimeout(windowResizeTimer);
        windowResizeTimer = setTimeout(function () {
            $window.trigger('togglerThresholdResize');
        }, 300);
    });

    $.fn.toggler = function (options) {
        var settings = $.extend({}, defaults, options);
        $targets = $targets.add($('[js-toggler-target]'));
        $togglers = $togglers.add($(this));

        return this.each(function (i, toggler) {
            var $toggler = $(toggler).off('.toggler');
            var dataset = toggler.dataset || {};
            var $togglersOther = $togglers.filter('[data-toggler="' + dataset.toggler + '"]').not($toggler);
            var single = dataset.togglerSingle || false;
            var close = dataset.togglerClose || false;
            var animate = dataset.togglerAnimate || true;
            var threshold = dataset.togglerThreshold || false;
            var $target = $targets.filter('[data-toggler="' + dataset.toggler + '"]').addClass('toggler-target').off('.toggler');
            var maxHeight = 'none';

            if (single) {
                close = false;
            }
            if (close) {
                close = JSON.parse(close);
                close.$target = close.target ? $toggler.find(close.target) : $toggler;
                close.textOrigin = close.$target.text();
            }
            if (animate) {
                var heightFrom;
                var heightTo;
            }
            if (threshold) {
                var togglerThresholdResize = function togglerThresholdResize() {
                    thresholdHeight = parseInt($target.css('max-height'));
                    targetHeight = $target.removeClass('mod-threshold').outerHeight();
                    if (!$target.hasClass('is-open')) {
                        $target.addClass('mod-threshold');
                    }
                    if (!$target.hasClass('is-threshold') && (!thresholdFrom || $window.width() <= thresholdFrom) && !isNaN(thresholdHeight) && targetHeight > thresholdHeight) {
                        $target.addClass('is-threshold');
                        $toggler.addClass('is-threshold');
                    } else if ($target.hasClass('is-threshold') && (isNaN(thresholdHeight) || targetHeight <= thresholdHeight) && (!thresholdFrom || $window.width() <= thresholdFrom)) {
                        $target.removeClass('is-threshold');
                        $toggler.removeClass('is-threshold');
                        if ($toggler.hasClass('is-open')) {
                            $toggler.trigger('toggle', [true]);
                        }
                    }
                };

                threshold = JSON.parse(threshold) || {};
                var targetHeight = 0;
                var thresholdHeight = threshold.height;
                var thresholdFrom = threshold.from;

                $target.addClass('mod-threshold is-threshold');
                $toggler.addClass('mod-threshold');
                if (!threshold.height) {
                    thresholdHeight = parseInt($target.css('max-height'));
                }
                $target.removeClass('is-threshold');
                $window.on('togglerThresholdResize', function () {
                    togglerThresholdResize();
                });
                togglerThresholdResize();
                $toggler.on('updateToggler.toggler', function (e) {
                    e.stopPropagation();
                    togglerThresholdResize();
                });
            }

            $toggler.on({
                'close.toggler': function closeToggler(e, simple) {
                    e.preventDefault();
                    e.stopPropagation();
                    if ($target.hasClass('is-animate')) return;

                    if (!single && $toggler.hasClass('is-open')) {
                        $toggler.removeClass('is-open');
                        $togglersOther.removeClass('is-open');
                        if (close) {
                            close.$target.text(close.textOrigin);
                        }
                        if (animate && !simple) {
                            heightFrom = $target.removeClass('is-animate').outerHeight(true);
                            $target.removeClass('is-open');
                            if (threshold) {
                                $target.addClass('mod-threshold');
                            }
                            heightTo = $target.outerHeight(true);
                            $target.removeClass('mod-threshold').css('max-height', heightFrom).css('height', heightFrom).outerHeight(true);
                            $target.addClass('is-animate is-open').css('max-height', heightTo).off('.toggler-animate').on('transitionend.toggler-animate', function (e) {
                                if (e.originalEvent.propertyName == 'max-height') {
                                    $target.removeClass('is-animate is-open').css('max-height', '').css('height', '').off('.toggler-animate').trigger('togglerClosed');
                                    if (threshold) {
                                        $target.addClass('mod-threshold');
                                    }
                                }
                            });

                            if (settings.scrollInView || settings.scrollInView == 'close') {
                                var scrollTargetOffset = dataset.togglerScrollTarget && dataset.togglerScrollTarget == 'parent' ? $toggler.closest('[js-toggler-scroll-target]').offset() : dataset.togglerScrollTarget && dataset.togglerScrollTarget == 'target' ? $target.offset() : $toggler.offset();
                                var scrollTop = $window.scrollTop();
                                if (scrollTargetOffset.top < scrollTop || scrollTargetOffset.top > scrollTop + $window.height()) {
                                    $HB.not(':animated').stop().animate({
                                        scrollTop: scrollTargetOffset.top
                                    }, 500);
                                }
                            }
                        } else {
                            $target.removeClass('is-open');
                        }
                    }
                },
                'open.toggler': function openToggler(e, simple) {
                    e.preventDefault();
                    e.stopPropagation();
                    if ($target.hasClass('is-animate')) return;

                    $toggler.addClass('is-open');
                    $togglersOther.addClass('is-open');
                    if (close) {
                        close.$target.text(close.text);
                    } else if (single == 'remove') {
                        $toggler.remove();
                        $togglersOther.remove();
                    }
                    if (animate && !simple) {
                        heightFrom = $target.removeClass('is-animate').outerHeight(true);
                        $target.addClass('is-open').removeClass('mod-threshold');
                        heightTo = $target.outerHeight(true);
                        $target.css('max-height', heightFrom).outerHeight(true);
                        $target.addClass('is-animate').css('max-height', heightTo).off('.toggler-animate').on('transitionend.toggler-animate', function (e) {
                            if (e.originalEvent.propertyName == 'max-height') {
                                $target.removeClass('is-animate').css('max-height', '').off('.toggler-animate').trigger('togglerOpened');
                            }
                        });
                    } else {
                        $target.addClass('is-open');
                    }
                },
                'click.toggler toggle.toggler': function clickTogglerToggleToggler(e, simple) {
                    !dataset.togglerPrevent && e.preventDefault();
                    if ($target.hasClass('is-animate')) return;

                    if (!single && $toggler.hasClass('is-open')) {
                        $toggler.trigger('close');
                    } else {
                        $toggler.trigger('open');
                    }
                }
            });
        });
    };

    $(function () {
        $HB = $('html, body');
    });
})(jQuery);
'use strict';

app.bind('default', function (context) {
  var $billboards = $('[js-billboards]', context);
  $billboards.each(function () {
    var $billboard = $(this);
    var $billboardSlider = $billboard.find('[js-billboards-slider]');
    var $billboardSliderList = $billboardSlider.find('[js-billboards-slider-list]');
    var $billboardSliderNav = $billboardSlider.find('[js-billboards-slider-nav]');
    var $billboardSliderDots = $billboardSlider.find('[js-billboards-slider-dots]');

    $billboardSliderList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: true,
      dots: true,
      margin: 0,
      navText: [],
      dotsContainer: $billboardSliderDots,
      navContainer: $billboardSliderNav,
      items: 1,
      onInitialized: function onInitialized() {
        $billboard.addClass('is-init');
      },
      onTranslate: function onTranslate(item) {
        var $this = $(item.currentTarget);
        $this.addClass('is-lock');
      },
      onTranslated: function onTranslated(item) {
        var $this = $(item.currentTarget);
        $this.removeClass('is-lock');
      }
    });
  });
});

app.bind('default', function (context) {
  var $stills = $('[js-still]', context);
  $stills.each(function () {
    var $still = $(this);
    var $stillSlider = $still.find('[js-still-slider]');
    var $stillSliderList = $stillSlider.find('[js-still-slider-list]');
    var $stillSliderNav = $stillSlider.find('[js-still-slider-nav]');
    var $stillSliderDots = $stillSlider.find('[js-still-slider-dots]');

    $stillSliderList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: true,
      dots: true,
      margin: 10,
      navText: [],
      dotsContainer: $stillSliderDots,
      navContainer: $stillSliderNav,
      items: 5,
      responsive: {
        0: {
          items: 1
        },
        500: {
          items: 2
        },
        600: {
          items: 3
        },
        1200: {
          items: 4
        },
        1300: {
          items: 5
        }
      },
      onInitialized: function onInitialized() {
        $still.addClass('is-init');
      },
      onTranslate: function onTranslate(item) {
        var $this = $(item.currentTarget);
        $this.addClass('is-lock');
      },
      onTranslated: function onTranslated(item) {
        var $this = $(item.currentTarget);
        $this.removeClass('is-lock');
      }
    });
  });
});
'use strict';

app.bind('default', function (context) {
  $('[js-calc-panel]', context).stick_in_parent({ offset_top: 60 });
  $('[js-calc-mobile]', context).on('click', function () {
    $.scrollTo($('[js-calc-side]'), 200, { offset: { top: -60 } });
  });
  $('[js-calc]', context).each(function () {
    var $calcForm = $(this);

    var $inputHeight = $calcForm.find('[js-height-window]');
    var $inputWidth = $calcForm.find('[js-width-window]');
    var $fieldsErrorSize = $calcForm.find('[js-fields-size]');

    var $calcPriceProduct = $calcForm.find('[js-result-product]');
    var $calcPriceProductTotal = $calcForm.find('[js-result-product-total]');
    var $calcPriceMontag = $calcForm.find('[js-result-montag]');
    var $calcPriceOtdelka = $calcForm.find('[js-result-otdelka]');
    var $calcTotal = $calcForm.find('[js-calc-total]');
    var $calculateButton = $calcForm.find('[js-calc-button]');
    var $ButtonAddOrder = $calcForm.find('[js-calc-button-add-order]');

    $inputHeight.add($inputWidth).on('input', function () {
      $fieldsErrorSize.removeClass('is-visible');
    });

    $calculateButton.on('click', function () {
      $fieldsErrorSize.removeClass('is-visible');
      var errors = [];
      if ($inputHeight.val() === '' || $inputHeight.val() > 2000 || $inputHeight.val() < 700) {
        errors.push('height');
      }
      if ($inputWidth.val() === '' || $inputWidth.val() > 2000 || $inputWidth.val() < 700) {
        errors.push('width');
      }
      if (errors.length === 0) {
        $calcTotal.removeClass('is-hidden');
        $.scrollTo($calcTotal, 150, { offset: { top: -60 } });
      } else {
        $.scrollTo($inputHeight, 100, { offset: { top: -120 } });
        $fieldsErrorSize.addClass('is-visible');
      }
    });
    $ButtonAddOrder.on('click', function () {
      $calcTotal.addClass('is-hidden');
      $.notify('Заказ добавлен в корзину', { globalPosition: 'bottom left', className: 'info', gap: 4 });
      $.scrollTo($('[js-calc-side]'), 200, { offset: { top: -60 } });

      var total = {};

      $calcForm.find('input').each(function () {
        var $input = $(this);
        if ($input.attr('name') && $input.attr('name').length > 0) {
          total[$input.attr('name')] = $input.val();
        }
      });
      $calcForm.find('select').each(function () {
        var $select = $(this);
        var $options = $select.find('option');
        var $seelectOption = $options.filter('[value= ' + $select.val() + ']').text().trim();
        if ($select.attr('name') && $select.attr('name').length > 0) {
          total[$select.attr('name')] = $seelectOption;
        }
      });

      total['product_price'] = $calcPriceProduct.text().trim();
      total['product_price_total'] = $calcPriceProductTotal.text().trim();
      total['install_price'] = $calcPriceMontag.text().trim();
      total['install_trim'] = $calcPriceOtdelka.text().trim();

      function generateID(min, max) {

        var id = getRandomArbitrary(min, max);
        var array = JSON.parse(localStorage.getItem('orders'));
        if (array) {
          if (array.length > 0) {
            if (array.find(function (item) {
              return item.id === id;
            })) {
              generateID(min, max);
            }
            return id;
          }
          return id;
        }
        return id;
      }

      total['id'] = generateID(0, 9999);

      if (localStorage.getItem('orders')) {
        if (JSON.parse(localStorage.getItem('orders')).length > 0) {
          var array = JSON.parse(localStorage.getItem('orders'));
          array.push(total);
          localStorage.setItem('orders', JSON.stringify(array));
          updateCart();
        } else {
          var _array = [];
          _array.push(total);
          localStorage.setItem('orders', JSON.stringify(_array));
          updateCart();
        }
      } else {
        var _array2 = [];
        _array2.push(total);
        localStorage.setItem('orders', JSON.stringify(_array2));
        updateCart();
      }
    });
  });
  updateCart();

  function updateCart() {
    var $card = $('[js-calc-panel]');
    var $cardOrders = $('[js-calc-orders]');
    var $cardOrdersTotal = $('[js-calc-orders-total]');
    if (localStorage.getItem('orders')) {
      var array = JSON.parse(localStorage.getItem('orders'));
      if (array.length > 0) {
        $card.addClass('is-filled');
        $cardOrders.html('');
        var totalPrice = 0;
        array.forEach(function (item, index) {
          totalPrice += parseInt(item.product_price_total);
          var $order = $('<div class="calc-info__orders-item"> <div class="calc-info__order"> <div class="calc-info__order-wrapper"> <div class="calc-info__order-main"> <div class="calc-info__order-title"> ' + (index + 1) + '. ' + item.profile + ' </div> <div class="calc-info__order-desc"> ' + item.width + ' \xD7 ' + item.height + ' \u043C\u043C ' + (item.install === 'Требуется' ? 'монтаж' : '') + ' ' + (item.decking !== 'Не требуется' ? ',отделка' : '') + ' </div> <div class="calc-info__order-price"> ' + item.product_price_total + ' \u20BD </div> </div> <div class="calc-info__order-side"> <div js-order-remove class="calc-info__order-close" data-id=' + item.id + '></div> </div> </div> </div> </div>');
          $order.find('[js-order-remove]').on('click', function () {
            var _this = this;

            array = array.filter(function (el) {
              return el.id !== $(_this).data('id');
            });
            localStorage.setItem('orders', JSON.stringify(array));
            updateCart();
          });
          $cardOrders.append($order);
        });
        $cardOrdersTotal.html(totalPrice);
      } else {
        $card.removeClass('is-filled');
      }
    } else {
      $card.removeClass('is-filled');
    }
  }

  function getRandomArbitrary(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }

  $('[js-calc-form]', context).on('submit', function (e) {
    e.preventDefault();
    var $form = $(this);
    var mod = $form.data('mod');
    var URL = $form.attr('action');
    var type = $form.attr('type');
    var $phone = $form.find('[js-phone]');

    var data = {};
    data.phone = $phone.val();
    if (mod === 'card') {
      data.items = localStorage.getItem('orders');
    }

    $form.addClass('is-load');
    $.ajax({
      url: URL,
      type: type,
      data: data,
      complete: function complete() {
        $form.removeClass('is-load');
      },
      success: function success() {
        $form.addClass('is-success');
        if (mod === 'card') {
          var array = [];
          localStorage.setItem('orders', JSON.stringify(array));
        }
      }
    });
  });
});
'use strict';

app.pluralize = function (n, titles) {
  return n + ' ' + titles[n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];
};

app.isInViewport = function ($element, edge) {
  var rect = ($element instanceof jQuery ? $element[0] : $element).getBoundingClientRect();
  return rect.bottom >= 0 && rect.right >= 0 && rect.top <= window.innerHeight && rect.left <= window.innerWidth;
};

app.bind('default layer', function (context) {
  $('[js-overlayer]', context).layer({
    isOverlayer: true,
    beforeOpen: function beforeOpen($layer, layerData) {},
    afterOpen: function afterOpen($layer, layerData) {
      app.use('basic', $layer);
    },
    afterClose: function afterClose($layer, layerData) {}
  });
});
app.bind('default layer', function (context) {
  $('[js-overlayer-menu]', context).layer({
    isOverlayer: true,
    aside: true,
    beforeOpen: function beforeOpen($layer, layerData) {},
    afterOpen: function afterOpen($layer, layerData) {
      app.use('basic', $layer);
    },
    afterClose: function afterClose($layer, layerData) {}
  });
});

app.bind('default layer-actions', function (context) {
  $('[js-layer-actions]', context).layer({
    isOverlayer: true,
    beforeOpen: function beforeOpen($layer, layerData) {
      $layer.find('[js-formus]').removeClass('is-complete');
    },
    afterOpen: function afterOpen($layer, layerData) {
      app.use('basic formus', $layer);
    },
    afterClose: function afterClose($layer, layerData) {}
  });
});

app.bind('default inputs basic', function (context) {
  $('[js-selectus]', context).selectus();

  $('[js-phonemask]', context).attr('data-is-inputmask', true).inputmask({
    mask: '+7 (999) 999-99-99',
    placeholder: '',
    jitMasking: 4,
    showMaskOnHover: true,
    showMaskOnFocus: false,
    isComplete: function isComplete(buffer, opts) {
      return Inputmask.isValid(buffer.join(''), { mask: opts.mask });
    },
    oncomplete: function oncomplete() {
      $(this).trigger('maskComplete');
    },
    onincomplete: function onincomplete() {
      $(this).trigger('maskIncomplete');
    }
  });
  $('[js-input-digital]', context).inputmask({
    regex: '[0-9]*',
    showMaskOnHover: false,
    showMaskOnFocus: false
  });
  $('[js-inputShadow]', context).inputShadow();
  $('[js-input-number]', context).attr('data-is-inputmask', true).each(function (inputIndex, input) {
    var $input = $(input);
    var minValue = typeof $input.data('min') !== 'undefined' ? parseInt($input.data('min')) : $input.prop('min').length > 0 ? $input.prop('min') : 1;
    var maxValue = typeof $input.data('max') !== 'undefined' ? parseInt($input.data('max')) : $input.prop('max').length > 0 ? $input.prop('max') : 999999999;
    var value = parseInt($input.val().replace(/[^0-9.]/g, ''));

    $input.off('.input-number').on({
      'input.input-number': function inputInputNumber(e) {
        setValue();
      }
    }).inputmask({
      mask: '9{*}',
      repeat: maxValue.length,
      greedy: true,
      regex: '[0-9]*',
      showMaskOnHover: false,
      showMaskOnFocus: false,
      isComplete: function isComplete(buffer, opts) {
        return Inputmask.isValid(buffer.join(''), { regex: opts.regex });
      }
    });

    function setValue() {
      var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : parseInt($input.val().replace(/[^0-9.]/g, ''));

      if (val < minValue) {
        value = minValue;
        $input.val(value).trigger('change');
      } else if (val > maxValue) {
        value = maxValue;
        $input.val(value).trigger('change');
      } else {
        value = val;
      }

      return value;
    }
  });
  $('[data-inputmask]', context).attr('data-is-inputmask', true).inputmask({
    oncomplete: function oncomplete() {
      $(this).trigger('maskComplete');
    },
    onincomplete: function onincomplete() {
      $(this).trigger('maskIncomplete');
    }
  });
});

app.bind('default burger', function (context) {
  var $btn = $('[js-header-menu]', context);
  var $btnClose = $('[js-header-close]', context);

  $btn.on('click', function () {
    Layer.open('main/menu');
  });

  $btnClose.on('click', function () {
    Layer.close('main/menu', {});
  });
});

app.bind('default toggler basic', function (context) {
  $('[js-toggler]', context).toggler();
});

app.bind('default', function (context) {
  $('[js-menu-open]', context).on('click', function () {
    Layer.open('main/menu');
  });
  $('[js-menu-close]', context).on('click', function () {
    Layer.closeAll();
  });
});

app.bind('default fancybox', function (context) {
  $('[data-fancybox]', context).fancybox({
    buttons: ['close'],
    animationEffect: false
  });
});

app.bind('default auth', function (context) {
  $('[js-auth-open]', context).each(function () {
    var $root = $(this);
    $root.on('click', function (e) {
      e.preventDefault();
      Layer.open('main/auth');
    });
  });

  $('[js-reg-open]', context).each(function () {
    var $root = $(this);
    $root.on('click', function (e) {
      e.preventDefault();
      Layer.open('main/reg');
    });
  });

  $('[js-recovery-open]', context).each(function () {
    var $root = $(this);
    $root.on('click', function (e) {
      e.preventDefault();
      Layer.open('main/recovery');
    });
  });
});

app.bind('default', function (context) {
  var $input = $('[js-input-payment]', context);
  var $togglers = $('[js-toggle-payment]', context);

  $togglers.on('change', function () {
    $input.val('');
  });

  $input.on('blur', function () {
    if ($input.val() > 0) {
      $togglers.each(function () {
        $(this).prop('checked', false);
      });
    } else if ($input.val() < 1 && $input.val().trim() !== '') {
      $input.val(10);
      $togglers.each(function () {
        $(this).prop('checked', false);
      });
    }
  });
});

app.bind('default orders-actions', function (context) {
  $('[js-card]', context).each(function () {
    var $card = $(this);
    var cardID = $card.data('id');
    var $links = $card.find('[js-card-order-link]');
    $links.each(function (indexLink, link) {
      var $link = $(link);
      var layerID = $link.data('layer');
      $link.on('click', function () {
        Layer.open(layerID, { id: cardID });
      });
    });
  });
});

app.bind('default', function () {
  $('[js-button-reload]').on('click', function () {
    $(this).addClass('is-load');
  });
});

app.bind('default', function (context) {
  $('[js-question-button]', context).each(function () {
    var $button = $(this);
    var delay = $button.data('delay') || false;
    $button.on('click', function () {
      if (delay) {
        Layer.closeAll();
        setTimeout(function () {
          Layer.open('main/question');
        }, 500);
      } else {
        Layer.open('main/question');
      }
    });
  });
});

app.bind('default', function (context) {
  $('[js-feedback-button]', context).each(function () {
    var $button = $(this);
    $button.on('click', function () {
      Layer.open('main/feedback');
    });
  });
});

app.bind('default', function (context) {
  var $rating = $('[js-rating]', context);
  var options = {
    max_value: 5,
    initial_value: 4,
    cursor: 'pointer',
    step_size: 1
  };
  $rating.rate(options);

  $rating.on('change', function (ev, data) {
    $('[js-input-rate]').val(data.to);
  });

  var $ratingReadOnly = $('[js-rating-read]', context);
  console.log($ratingReadOnly);
  $ratingReadOnly.each(function () {
    var $root = $(this);
    var rating = $root.data('rating');

    var options = {
      max_value: 5,
      initial_value: rating,
      cursor: 'default',
      step_size: 1,
      readonly: true
    };
    $root.rate(options);
  });
});
'use strict';

app.bind('default formus', function (context) {
  $('[js-formus]', context).each(function () {
    var $form = $(this);
    var method = $form.attr('method');
    var url = $form.attr('action');
    var reload = $form.data('reload') ? $form.data('reload') : false;
    var request = void 0;
    $form.off('.formus').on('submit.formus', function (e) {
      e.preventDefault();
      $form.addClass('is-load');
      request && request.abort();

      request = $.ajax({
        url: url,
        type: method,
        dataType: 'html',
        data: $form.serialize(),
        success: function success(data) {},
        complete: function complete() {
          if (!reload) {
            $form.removeClass('is-load');
            $form.addClass('is-complete');
            request = null;
          } else {
            document.location.reload(true);
          }
        }
      });
    });
  });
});
'use strict';

app.bind('default', function (context) {
  var $map = $('[js-map]', context);
  if ($map.length) {
    ymaps.ready(init);
  }

  function init() {
    var myMap = new ymaps.Map($map[0], {
      // Координаты центра карты.
      // Порядок по умолчанию: «широта, долгота».
      // Чтобы не определять координаты центра карты вручную,
      // воспользуйтесь инструментом Определение координат.
      center: [55.76, 37.64],
      // Уровень масштабирования. Допустимые значения:
      // от 0 (весь мир) до 19.
      zoom: 7,
      controls: []
    });

    var coords = $map.data('coords');
    myMap.behaviors.disable('scrollZoom');

    var geoObjects = [];

    coords.split(';').forEach(function (item) {
      var a = item.replace('[', '');
      var b = a.replace(']', '');
      var c = [];
      c.push(parseFloat(b.split(',')[0]));
      c.push(parseFloat(b.split(',')[1]));
      var myPlacemark = new ymaps.Placemark(c, {
        hintContent: '',
        balloonContent: ''
      });
      geoObjects.push(myPlacemark);
    });

    var clusterer = new ymaps.Clusterer();

    clusterer.add(geoObjects);
    myMap.geoObjects.add(clusterer);

    myMap.setBounds(myMap.geoObjects.getBounds(), { checkZoomRange: true });
  }
});
'use strict';

app.bind('default', function (context) {

  var $window = $(window);

  var $grid = void 0;

  function initMasonry() {
    $grid = $('[js-masonry]').masonry({
      itemSelector: '[js-masonry-item]',
      columnWidth: '[js-masonry-item]'
    });

    $grid.imagesLoaded().progress(function () {
      $grid.masonry('layout');
    });
  }

  $window.on('load', function () {
    initMasonry();
  });
});
'use strict';

app.bind('default', function (context) {
  var $officeAddresses = $('[js-toggle-box]', context);
  $officeAddresses.each(function () {
    var $officeAddress = $(this);
    var $officeAddressClose = $officeAddress.find('[js-toggle-box-close]');
    var timeout = void 0;
    $officeAddress.on('mouseenter', function () {
      clearTimeout(timeout);
      $officeAddress.addClass('is-open');
    });
    $officeAddress.on('click', function () {
      clearTimeout(timeout);
      $officeAddress.addClass('is-open');
    });
    $officeAddress.on('mouseleave', function () {
      timeout = setTimeout(function () {
        $officeAddress.removeClass('is-open');
      }, 1500);
    });
    $officeAddress.on('click', function (e) {
      e.stopPropagation();
    });
    $officeAddressClose.on('click', function (e) {
      e.stopPropagation();
      $officeAddress.removeClass('is-open');
    });
    $('body').on('click', function () {
      $officeAddress.removeClass('is-open');
    });
  });
});
'use strict';

app.bind('default slider', function (context) {

  var $sliders = $('[js-gallery]', context);
  $sliders.each(function (index, item) {
    var $slider = $(item);

    var $sliderMain = $(item).find('[js-gallery-main]');
    var $sliderMainList = $slider.find('[js-gallery-main-list]');
    var $sliderMainNav = $slider.find('[js-gallery-main-nav]');

    $sliderMainList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: true,
      dots: false,
      margin: 15,
      navContainer: $sliderMainNav,
      navText: [],
      items: 1,
      onInitialized: function onInitialized() {
        $sliderMain.addClass('is-init');
      },
      onTranslate: function onTranslate(item) {
        var $this = $(item.currentTarget);
        $this.addClass('is-lock');
      },
      onTranslated: function onTranslated(item) {
        var $this = $(item.currentTarget);
        $this.removeClass('is-lock');
        var pos = $this.find('.owl-item.active [data-index]').data('index');
        $sliderPreviewList.find('[data-index]').removeClass('is-active').filter('[data-index=' + pos + ']').addClass('is-active');
        $sliderPreviewList.trigger('to.owl.carousel', pos - 1);
      }
    });

    var $sliderPreview = $(item).find('[js-gallery-preview]');
    var $sliderPreviewList = $slider.find('[js-gallery-preview-list]');
    var $sliderPreviewItem = $slider.find('[js-gallery-preview-item]');

    $sliderPreviewList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: false,
      dots: false,
      margin: 20,
      navText: [],
      items: 3,
      responsive: {
        0: {
          items: 2,
          nav: false
        },
        1200: {
          items: 3
        }
      },
      onInitialized: function onInitialized() {
        $sliderPreview.addClass('is-init');
      }
    });

    $sliderPreviewItem.off('.slider').on('click.slider', function () {
      var pos = $(this).data('index');
      $sliderPreviewList.find('[data-index]').removeClass('is-active').filter('[data-index=' + pos + ']').addClass('is-active');
      $sliderMainList.trigger('to.owl.carousel', pos - 1);
    });
  });
});
'use strict';

app.bind('default', function (context) {
  var $tabs = $('[js-tabs]', context);
  $tabs.each(function () {
    var $tab = $(this);
    var $tabAction = $tab.find('[js-tab-action]');
    var $tabPage = $tab.find('[js-tab-page]');

    $tabAction.on('click', function () {
      $tabAction.removeClass('is-active');
      $(this).addClass('is-active');
      var i = $(this).data('target');
      $tabPage.removeClass('is-active').filter('[data-target=' + i + ']').addClass('is-active');
    });
  });
});
'use strict';

app.bind('default', function (context) {
  var $textToggles = $('[js-toggle-text]', context);
  $textToggles.each(function () {
    var $textToggle = $(this);
    var $textToggleAction = $textToggle.find('[js-toggle-text-action]');
    $textToggleAction.on('click', function () {
      $textToggle.toggleClass('is-open');
    });
  });
});
'use strict';

app.bind('default', function (context) {
  var $windows = $('[js-windows-item]', context);
  var $input = $('[js-window-input]');

  $('[js-windows]').each(function () {
    var $windowItems = $(this).find('[js-windows-item]');
    $windowItems.each(function () {
      var $windowItem = $(this);
      var $link = $windowItem.find('.windows-item__link');
      var $windowItemImgs = $windowItem.find('[js-img]');

      $link.on('click', function (e) {
        e.stopPropagation();
        $windowItems.not($windowItem).removeClass('is-open');
        $windowItem.toggleClass('is-open');
      });
      $windowItemImgs.each(function () {
        var $windowItemImg = $(this);
        $windowItemImg.on('click', function () {
          var $imgBox = $('[js-windows-img-main-box]');
          var $img = $(this);
          $input.val($img.data('id'));
          $input.change();
          console.log($input.val());
          $windowItems.removeClass('is-open');
          $('[js-windows-img-additional-box]').removeClass('is-active');
          $imgBox.removeClass('is-active');
          if ($img.closest('[js-windows-img-main-box]').length) {
            $('[js-img]').filter('[data-id=' + $img.data('id') + ']').closest('[js-windows-img-additional-box]').addClass('is-active');
            $img.closest($imgBox).addClass('is-active');
          } else {
            var $imgClone = $(this).clone(true);
            $img.closest('[js-windows-img-additional-box]').addClass('is-active');
            $img.closest($windowItem).find($imgBox).addClass('is-active').html($imgClone);
          }
        });
      });

      $windowItem.on('click', function (e) {
        e.stopPropagation();
      });

      $windowItem.on('mouseenter', function () {
        if ($(window).width() < 1000) {
          return false;
        }
        $windowItem.removeClass('is-open');
        $(this).addClass('is-open');
      });
      $windowItem.on('mouseleave', function () {
        if ($(window).width() < 1000) {
          return false;
        }
        $windowItem.removeClass('is-open');
      });

      $('body').on('click', function () {
        $windowItems.removeClass('is-open');
      });
    });
  });
});
//# sourceMappingURL=common.js.map
