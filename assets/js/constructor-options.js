OPTIONS_CONSTRUCTOR = [
  {
    id: 1,
    slug: 'okno',
    title: 'Окно',
    active: false,
    colors: [
      {
        active: true,
        title: 'Белые',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/okno/belyj.png'
      }, {
        active: false,
        title: 'Золотой дуб',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/okno/zolotoj-dub.png'
      }
    ]
  },
  {
    id: 2,
    slug: 'otkosi',
    title: 'Откосы',
    active: false,
    colors: [
      {
        active: true,
        title: 'Венге',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/otkosi/otkosy-venge.png'
      }, {
        active: false,
        title: 'Вилья',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/otkosi/otkosy-vilya.png'
      }
    ]
  },
  {
    id: 3,
    slug: 'podokonnik',
    title: 'Подконник',
    active: false,
    colors: [
      {
        active: true,
        title: 'Венге',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/podokonnik/podokonnik-venge.png'
      },
      {
        active: false,
        title: 'Вилья',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/podokonnik/podokonnik-vilya.png'
      }
    ]
  }, {
    id: 4,
    slug: 'pol',
    title: 'Пол',
    active: false,
    colors: [
      {
        active: true,
        title: 'Ванильный дуб',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/pol/pol-dub-vanilnyj.png'
      }, {
        active: false,
        title: 'Венге',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/pol/pol-venge.png'
      }
    ]
  }, {
    id: 5,
    slug: 'steni',
    title: 'Стены',
    active: false,
    colors: [
      {
        active: true,
        title: 'Стена 1',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/steni/steny-1.png'
      }, {
        active: true,
        title: 'Стена 2',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/steni/steny-2.png'
      }
    ]
  },
  {
    id: 6,
    slug: 'shtori',
    title: 'Шторы',
    active: false,
    colors: [
      {
        active: true,
        title: 'Штора 2',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/shtori/shtora-2.png'
      }, {
        active: true,
        title: 'Штора 3',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/shtori/shtora-3.png'
      }
    ]
  },
  {
    id: 7,
    slug: 'radiator',
    title: 'Радиатор',
    active: false,
    colors: [
      {
        active: true,
        title: 'Радиатор 1',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/radiator/radiator-1.png'
      },
      {
        active: true,
        title: 'Радиатор 2',
        icon: '/wp-content/themes/origin/assets/images/constructor/assets/colors/color.png',
        image: '/wp-content/themes/origin/assets/images/constructor/assets/radiator/radiator-2.png'
      }
    ]
  }
]