<?php
/**
 * Template Name: Шаблон страницы подоконника
 * @package wordpress
 * @subpackage origin
 * @since 1.0
 */
get_header() ?>
<div class="list-sections">
    <div class="list-sections__list">
        <div class="list-sections__list-item">
            <div class="still block">
                <div class="still__wrapper block-wrapper">
                    <h1 class="still__title">
                        Подоконники Crystallit и&nbsp;Estera
                    </h1>
                    <div class="still__desc">
                        <div class="still__desc-wrapper">
                            <div class="still__desc-main">
                                Подоконники марок Crystallit и&nbsp;Estera притягивают внимание и&nbsp;являются
                                украшением любого интерьера. Благодаря множеству вариантов декора будет
                                удовлетворен самый взысательный вкус. Большая толщина внутренних стенок
                                подоконника обеспечивает повышенную
                                прочность, а&nbsp;ультрасовременной пленка из&nbsp;акриловых смол DST,
                                нанесенная на&nbsp;поверхность&nbsp;&mdash; защиту от&nbsp;механических
                                повреждений и&nbsp;долговечность. Доступна матовая или глянцевая поверхность на&nbsp;выбор.
                            </div>
                            <div class="still__desc-side">
                                <a href="https://www.youtube.com/watch?v=zZUbXAVV-HA&t=1126s" data-fancybox=""
                                   class="video-item"
                                   style="background-image: url('http://placehold.it/456x308')">
                                    <img src="http://placehold.it/456x308" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="still__advantages">
                        <div class="still__advantages-list">
                            <div class="still__advantages-item">
                                <div class="still__advantage">
                                    <div class="still__advantage-icon">
                                        <img src=<?php bloginfo('template_url'); ?>/assets/images/still/icon-1.svg"
                                             alt="">
                                    </div>
                                    <div class="still__advantage-desc">Срок эксплуатации до 70 лет</div>
                                </div>
                            </div>
                            <div class="still__advantages-item">
                                <div class="still__advantage">
                                    <div class="still__advantage-icon">
                                        <img src=<?php bloginfo('template_url'); ?>/assets/images/still/icon-2.svg"
                                             alt="">
                                    </div>
                                    <div class="still__advantage-desc">Не выцветает на солнце</div>
                                </div>
                            </div>
                            <div class="still__advantages-item">
                                <div class="still__advantage">
                                    <div class="still__advantage-icon">
                                        <img src=<?php bloginfo('template_url'); ?>/assets/images/still/icon-3.svg"
                                             alt="">
                                    </div>
                                    <div class="still__advantage-desc">Повышенная термостойкость</div>
                                </div>
                            </div>
                            <div class="still__advantages-item">
                                <div class="still__advantage">
                                    <div class="still__advantage-icon">
                                        <img src=<?php bloginfo('template_url'); ?>/assets/images/still/icon-4.svg"
                                             alt="">
                                    </div>
                                    <div class="still__advantage-desc">Стойкость к истиранию</div>
                                </div>
                            </div>
                            <div class="still__advantages-item">
                                <div class="still__advantage">
                                    <div class="still__advantage-icon">
                                        <img src=<?php bloginfo('template_url'); ?>/assets/images/still/icon-5.svg"
                                             alt="">
                                    </div>
                                    <div class="still__advantage-desc">Высокая прочность</div>
                                </div>
                            </div>
                            <div class="still__advantages-item">
                                <div class="still__advantage">
                                    <div class="still__advantage-icon">
                                        <img src=<?php bloginfo('template_url'); ?>/assets/images/still/icon-6.svg"
                                             alt="">
                                    </div>
                                    <div class="still__advantage-desc">Экологически безопасен</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="still__cols">
                        <div class="still__cols-item">
                            <div class="still__col">
                                <div class="still__col-side">
                                    <div class="still__col-img still__col-img_left"
                                         style="background-image: url('http://placehold.it/720x336')"></div>
                                </div>
                                <div class="still__col-main">
                                    <div class="still__col-desc">
                                        <b>Подоконник Crystallit</b> — на ваш выбор матовая или глянцевая
                                        поверхность, более 30 декоров.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="still__cols-item">
                            <div class="still__col">
                                <div class="still__col-side">
                                    <div class="still__col-img still__col-img_right"
                                         style="background-image: url('http://placehold.it/720x336')"></div>
                                </div>
                                <div class="still__col-main still__col-main_ml_auto">
                                    <div class="still__col-desc">
                                        <b>Подоконник Estera</b> — прекрасный дизайн закругленного носика и
                                        каменные декоры создают впечатление подоконника из натурального камня.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="still__videos">
                        <div class="still__videos-side">
                            <div class="still__videos-title">
                                Видео с испытаниями подоконников Crystallit и Estera
                            </div>
                        </div>
                        <div class="still__videos-main">
                            <div class="video-list">
                                <div class="video-list__list">
                                    <div class="video-list__list-item">
                                        <a href="https://www.youtube.com/watch?v=zZUbXAVV-HA&t=1126s"
                                           data-fancybox="" class="video-item"
                                           style="background-image: url('http://placehold.it/456x308')">
                                            <img src="http://placehold.it/456x308" alt="">
                                        </a>
                                    </div>
                                    <div class="video-list__list-item">
                                        <a href="https://www.youtube.com/watch?v=zZUbXAVV-HA&t=1126s"
                                           data-fancybox="" class="video-item"
                                           style="background-image: url('http://placehold.it/456x308')">
                                            <img src="http://placehold.it/456x308" alt="">
                                        </a>
                                    </div>
                                    <div class="video-list__list-item">
                                        <a href="https://www.youtube.com/watch?v=zZUbXAVV-HA&t=1126s"
                                           data-fancybox="" class="video-item"
                                           style="background-image: url('http://placehold.it/456x308')">
                                            <img src="http://placehold.it/456x308" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $items = get_field('still_prices'); ?>
                    <?php if ($items): ?>
                        <div class="still__table">
                            <div class="still__table-main">
                                <div class="still__table-title">
                                    Цены и форма отпуска
                                </div>
                            </div>
                            <div class="still__table-side">
                                <div class="still__table-main">
                                    <table>
                                        <tbody>
                                        <tr></tr>
                                        <tr>
                                            <td class="title">Глубина, мм</td>
                                            <td class="title">Crystallit, ₽/м</td>
                                            <td class="title">Estera, ₽/м</td>
                                        </tr>
                                        <?php foreach ($items as $item): ?>
                                            <tr>
                                                <td><?php echo $item['depth']; ?></td>
                                                <td><?php echo $item['crystallit']; ?></td>
                                                <td><?php echo $item['estera']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="still__slider">
                        <div class="still__slider-side">
                            <div class="still__slider-title">
                                Декоры Crystallit
                            </div>
                        </div>
                        <div class="still__slider-main">
                            <div js-still class="still-slider">
                                <div js-still-slider class="still-slider__wrapper">
                                    <div js-still-slider-nav class="still-slider__nav"></div>
                                    <div js-still-slider-list class="still-slider__list">
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="still__slider">
                        <div class="still__slider-side">
                            <div class="still__slider-title">
                                Декоры Crystallit
                            </div>
                        </div>
                        <div class="still__slider-main">
                            <div js-still class="still-slider">
                                <div js-still-slider class="still-slider__wrapper">
                                    <div js-still-slider-nav class="still-slider__nav"></div>
                                    <div js-still-slider-list class="still-slider__list">
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="still-slider__list-item">
                                            <div class="still-slider__item">
                                                <div class="still-slider__item-side">
                                                    <div class="still-slider__item-img"
                                                         style="background-image: url(http://placehold.it/128x128)"></div>
                                                </div>
                                                <div class="still-slider__item-main">
                                                    <div class="still-slider__item-title">
                                                        Антрацит
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $gallery = get_field('gallery'); ?>
                    <?php if ($gallery): ?>
                        <div class="still__gallery">
                            <div class="still__gallery-side">
                                <div class="still__gallery-title">
                                    Фотогалерея подоконников
                                </div>
                            </div>
                            <div class="still__gallery-main">
                                <div js-masonry class="still__gallery-list">
                                    <?php foreach ($gallery as $item): ?>
                                        <a href="<?php echo $item['url']; ?>" data-fancybox js-masonry-item
                                           class="still__gallery-item">
                                            <img src="<?php echo $item['url']; ?>" alt="<?php echo $item['alt']; ?>"
                                                 title="<?php echo $item['title']; ?>">
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>
