<?php
/**
 * Регистрация стилей и скриптов
 */
function wp_enqueue_js_css()
{
    wp_enqueue_style('google-fonts', '//fonts.googleapis.com/css?family=Roboto:300,400,500,700', false);
    wp_enqueue_style('main-style', get_template_directory_uri() . '/style.css');

    wp_enqueue_style('vendor-style', get_template_directory_uri() . '/assets/css/vendor.css');
    wp_enqueue_style('common-style', get_template_directory_uri() . '/assets/css/common.css');
    wp_enqueue_style('fixes-style', get_template_directory_uri() . '/assets/css/fixes.css');
    wp_enqueue_style('constructor-style', get_template_directory_uri() . '/assets/css/constructor.css');


    wp_enqueue_script('constructor-options', get_template_directory_uri() . '/assets/js/constructor-options.js');
    wp_enqueue_script('vendor-script', get_template_directory_uri() . '/assets/js/vendor.js');
    wp_enqueue_script('common-script', get_template_directory_uri() . '/assets/js/common.js');
}

add_action('wp_enqueue_scripts', 'wp_enqueue_js_css');

function origin_send_mail($header, $message)
{
    $send_to_email = get_option('feedback_email_input_setting');
    if (!$send_to_email || !$header || !$message) {
        return false;
    }

    if (wp_mail($send_to_email, $header, $message)) {
        return true;
    } else {
        return false;
    }
}

/**
 * Отправка заявки на почту
 */
function wp_send_request()
{
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $desc = $_POST['desc'];

    if (!$name || !$phone || !$desc) {
        echo 400;
        wp_die();
    }

    $header = 'Заявка на сайте';
    $message = "Имя: $name\r\nПочта или телефон: $phone\r\nВопрос: $desc";

    if (origin_send_mail($header, $message)) {
        echo 200;
    } else {
        echo 400;
    }
    wp_die();
}

add_action('wp_ajax_wp_send_request', 'wp_send_request');
add_action('wp_ajax_nopriv_wp_send_request', 'wp_send_request');

/**
 * Отправка обратного звонка на почту
 */
function wp_send_callback()
{
    $phone = $_POST['phone'];

    if (!$phone) {
        echo 400;
        wp_die();
    }

    $header = 'Заявка на обратный звонок';
    $message = "Телефон: $phone";

    if (origin_send_mail($header, $message)) {
        echo 200;
    } else {
        echo 400;
    }
    wp_die();
}

add_action('wp_ajax_wp_send_callback', 'wp_send_callback');
add_action('wp_ajax_nopriv_wp_send_callback', 'wp_send_callback');

/**
 * Отправка заказа на почту
 */
function wp_send_order()
{
    $data = json_decode(file_get_contents('php://input'), true);
    $result = "";
    $index = 0;
    foreach ($data['items'] as $item) {
        //---->Тип окна
        $result .= 'Изделие № ' . ++$index;
        $result .= 'Ширина окна: ' . $item['width'] . '\r\n';
        $result .= 'Высота окна: ' . $item['height'] . '\r\n';
        $result .= 'Ширина створки: ' . $item['width_sash'] . '\r\n';
        //---->Параметры
        $result .= 'Профиль: ' . $item['profile'] . '\r\n';
        $result .= 'Стеклопакет: ' . $item['glass'] . '\r\n';
        $result .= 'Кашировка: ' . $item['kaschieren'] . '\r\n';
        $result .= 'Тонировка: ' . $item['tinted'] . '\r\n';
        //---->Монтаж и отделка
        $result .= 'Монтаж: ' . $item['install'] . '\r\n';
        $result .= 'Подоконник: ' . $item['still'] . '\r\n';
        $result .= 'Тип дома: ' . $item['type'] . '\r\n';
        $result .= 'Отделка откосов: ' . $item['decking'] . '\r\n';
        $result .= 'Наружный слив: ' . $item['drain'] . '\r\n';


        $result .= '\r\nСтоимость изделия: ' . $item['product_price'] . '\r\n';
        $result .= 'Стоимость монтажа: ' . $item['install_price'] . '\r\n';
        $result .= 'Стоимость отделки: ' . $item['install_trim'] . '\r\n';
    }

    //Итоговая стоимость
    $result .= '\r\n\r\nИтог: ' . $item['total'];
    $result .= '\r\n\r\nТелефон: ' . $item['phone'];
    $header = 'Заявка на изделие';

    if (origin_send_mail($header, $result)) {
        echo 200;
    } else {
        echo 400;
    }
    wp_die();
}

add_action('wp_ajax_wp_send_order', 'wp_send_order');
add_action('wp_ajax_nopriv_wp_send_order', 'wp_send_order');


/**
 * Создание отзыва
 */
function wp_send_feedback()
{
    //Имя
    $name = $_POST['name'];
    //номер договора
    $doc = $_POST['doc'];
    //почта
    $email = $_POST['email'];
    //телефон
    $phone = $_POST['phone'];
    //ссылка на соц. сеть
    $social = $_POST['social'];
    //оценка
    $rate = $_POST['rate'];
    //текст отзыва
    $desc = $_POST['desc'];

    if (!$name || !$doc || (!$email && !$phone) || !$rate || !$desc) {
        echo 400;
        wp_die();
    }

    $new_post = array(
        'post_title' => 'Договор #' . $doc,
        'post_content' => $desc,
        'meta_input' => array(
            'wpcf-feedback_name' => $name,
            'wpcf-feedback_doc' => $doc,
            'wpcf-feedback_email' => $email,
            'wpcf-feedback_phone' => $phone,
            'wpcf-feedback_social' => $social,
            'wpcf-feedback_rate' => $rate,
        ),
        'post_status' => 'private',
        'post_type' => 'feedbacks'
    );
    $pid = wp_insert_post($new_post);
    if ($pid) {
        $header = 'Отзыв на сайте';
        $message = "Имя: $name\r\nНомер договора: $doc\r\nПочта: $email\r\nТелефон: $phone\r\n"
            . "Социальная сеть: $social\r\nОтзыв: $desc\r\nОценка: $rate\r\n";
        origin_send_mail($header, $message);
    }

    echo 200;
    wp_die();
}

add_action('wp_ajax_wp_send_feedback', 'wp_send_feedback');
add_action('wp_ajax_nopriv_wp_send_feedback', 'wp_send_feedback');

function origin_image_sizes()
{
    add_image_size('project-slider-size', 200, 200, true);
}

add_action('after_setup_theme', 'origin_image_sizes');

function add_query_vars_filter($vars)
{
    $vars[] = "paged";
    $vars[] = "type";
    return $vars;
}

add_filter('query_vars', 'add_query_vars_filter');


require get_parent_theme_file_path('/inc/customizer.php');
require get_parent_theme_file_path('/inc/utils.php');
require get_parent_theme_file_path('/inc/repository.php');