<?php
/**
 * Template Name: Шаблон страницы отзывов
 * @package wordpress
 * @subpackage origin
 * @since 1.0
 */
get_header() ?>
<div class="body__content">
    <div class="list-sections">
        <div class="list-sections__list">
            <div class="list-sections__list-item list-sections__list-item_reviews">
                <div class="reviews block">
                    <div class="reviews__wrapper block-wrapper">
                        <div class="reviews__side">
                            <h1 class="reviews__title">
                                Отзывы
                            </h1>
                            <div class="reviews__action">
                                <button js-feedback-button class="button">
                                    <span class="button__content">
                                        <span class="button__title">
                                            Добавить отзыв
                                        </span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div class="reviews__main">
                            <div class="reviews__list">
                                <?php $feedbacks = origin_get_feedbacks(); ?>
                                <?php while ($feedbacks->have_posts()) : $feedbacks->the_post(); ?>
                                    <div class="reviews__list-item">
                                        <div class="review-item">
                                            <div class="review-item__wrapper">
                                                <div class="review-item__major">
                                                    <div class="review-item__major-wrapper">
                                                        <?php if (types_render_field('feedback_image', array('raw' => 'true'))): ?>
                                                            <div class="review-item__side">
                                                                <div class="review-item__avatar" style="background-image: url(<?php echo types_render_field('feedback_image', array('raw' => 'true')); ?>)"></div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <div class="review-item__main">
                                                            <div class="review-item__header">
                                                                <div class="review-item__header-wrapper">
                                                                    <div class="review-item__header-main">
                                                                        <div class="review-item__title review-item__title_vk">
                                                                            <span class="review-item__title-caption"> <?php echo types_render_field('feedback_name', array('raw' => 'true')); ?>
                                                                                </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="review-item__header-side">
                                                                        <div class="review-item__doc">
                                                                            Договор № <?php echo types_render_field('feedback_doc', array('raw' => 'true')); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="review-item__body">
                                                                <div class="review-item__rating">
                                                                    <div class="rating rating_small" js-rating-read data-rating="<?php echo types_render_field('feedback_rate', array('raw' => 'true')); ?>"></div>
                                                                </div>
                                                                <div class="review-item__desc">
                                                                    <?php the_content(); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if ($response): ?>
                                                    <div class="review-item__minor">
                                                        <div class="review-item__company">
                                                            <div class="review-item__company-main">
                                                                <?php echo $response; ?>
                                                            </div>
                                                            <div class="review-item__company-side">
                                                                Отдел качества компании «Оригинал»
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile;
                                wp_reset_query(); ?>
                            </div>
                        </div>
                        <div class="pagination">
                            <div class="pagination__list">
                                <a href="#" class="prev page-numbers"></a>
                                <a href="#" class="page-numbers current">1</a>
                                <a href="#" class="page-numbers">2</a>
                                <a href="#" class="next page-numbers"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php get_footer() ?>
