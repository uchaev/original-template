<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
</div> <!--body__content-->
<div class="body__footer">
    <div class="footer block">
        <div class="footer__wrapper block-wrapper">
            <div class="footer-main footer__main">
                <div class="footer-main__cols">
                    <div class="footer-main__col footer-main__col_uno">
                        <div class="footer-nav footer-nav_large">
                            <div class="footer-nav__list footer-nav__list_scroll">
                                <?php $bottomMenu1 = origin_get_bottom_menu_level1(); ?>
                                <?php if ($bottomMenu1): foreach ($bottomMenu1 as $item): ?>
                                    <div class="footer-nav__item">
                                        <a href=" <?php echo $item->url; ?>" class="footer-nav__item-link">
                                            <?php echo $item->title; ?>
                                        </a>
                                    </div>
                                <?php endforeach; endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer-main__col footer-main__col_dos">
                        <div class="footer-nav">
                            <div class="footer-nav__list footer-nav__list_scroll">
                                <?php $bottomMenu2 = origin_get_bottom_menu_level2(); ?>
                                <?php if ($bottomMenu2): foreach ($bottomMenu2 as $item): ?>
                                    <div class="footer-nav__item">
                                        <a href=" <?php echo $item->url; ?>" class="footer-nav__item-link">
                                            <?php echo $item->title; ?>
                                        </a>
                                    </div>
                                <?php endforeach; endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer-main__col footer-main__col_tres">
                        <div class="footer-nav">
                            <div class="footer-nav__list footer-nav__list_scroll">
                                <?php $bottomMenu3 = origin_get_bottom_menu_level3(); ?>
                                <?php if ($bottomMenu3): foreach ($bottomMenu3 as $item): ?>
                                    <div class="footer-nav__item">
                                        <a href=" <?php echo $item->url; ?>" class="footer-nav__item-link">
                                            <?php echo $item->title; ?>
                                        </a>
                                    </div>
                                <?php endforeach; endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer-main__col footer-main__col_cuatro">
                        <?php
                        $vk = origin_vk_input_setting();
                        $inst = origin_instagram_input_setting();
                        ?>
                        <?php if ($vk || $inst): ?>
                            <div class="footer-main__socials">
                                <div class="social social_large">
                                    <div class="social__wrapper">
                                        <div class="social__side">
                                            <div class="social__title">
                                                Наши соцсети:
                                            </div>
                                        </div>
                                        <div class="social__main">
                                            <div class="social__list">
                                                <?php if ($vk): ?>
                                                    <div class="social__list-item">
                                                        <a href="<?php echo $vk; ?>" target="_blank"
                                                           class="social-badge social-badge_vk"></a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if ($inst): ?>
                                                    <div class="social__list-item">
                                                        <a href="<?php echo $inst; ?>" target="_blank"
                                                           class="social-badge social-badge_instagram"></a>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="footer-main__actions">
                            <?php $bottomMenu4 = origin_get_bottom_menu_level4(); ?>
                            <?php if ($bottomMenu4): foreach ($bottomMenu4 as $item): ?>
                                <div class="footer-main__actions-item">
                                    <button class="button button_full button_outline">
                                        <span class="button__content">
                                            <span class="button__title">
                                                <?php echo $item->title; ?>
                                            </span>
                                        </span>
                                    </button>
                                </div>
                            <?php endforeach; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-side footer__side">
                <div class="footer-side__wrapper">
                    <div class="footer-side__main">
                        <div class="footer-side__logo">
                            <div class="footer-side__logo-main">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/logo-footer.png"
                                     alt='Логотип оригинал'
                                     class="footer-side__logo-img">
                            </div>
                            <div class="footer-side__logo-side">
                                <div class="footer-side__logo-caption">
                                    завод светопрозрачных конструкций
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-side__side">
                        <div class="footer-side__copyright">
                            © 2018. Сайт сделан в <a href="http://siteroad.su" target="_blank"
                                                     class="footer-side__copyright-link">Siteroad</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> <!-- body__wrapper-->
<?php wp_footer(); ?>
<script>
  app.start()
</script>
<div js-layer-actions class="layer layer_overlay layer_border_full layer_link_small" data-id="main/question">
    <div class="layer__inside">
        <a js-close-layer class="layer-close" role="button" href="#"></a>
        <div class="layer__header">
            <div class="layer__title">Вопрос специалисту</div>
        </div>
        <div class="layer__content">
            <form action="/wp-admin/admin-ajax.php?action=wp_send_request" js-formus method="post" class="formus">
                <div class="formus__content formus__content_visible sections-list sections-list_row_xsmall">
                    <div class="sections-list__list">
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <input name="name" class="field__input" placeholder="Имя">
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <input type="text" name="phone" required class="field__input" placeholder="Телефон или почта">
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <textarea name="desc" class="field__input" placeholder="Вопрос" rows="6"> </textarea>
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small sections-list__item_center">
                            <button class="button button_full" type="submit">
                                <span class="button__content">
                                    <span class="button__title">
                                        Задать вопрос
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="formus formus__content formus__content_hidden sections-list sections-list_row_xsmall">
                    <div class="sections-list__list">
                        <div class="sections-list__item sections-list__item_row-small sections-list__item_center">
                            <p class="-align_center">
                                Наши менеджеры свяжутся с Вами в ближайшее время
                            </p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div js-layer-actions class="layer layer_overlay layer_border_full layer_link_small" data-id="main/feedback">
    <div class="layer__inside">
        <a js-close-layer class="layer-close" role="button" href="#"></a>
        <div class="layer__header">
            <div class="layer__title">Ваш отзыв</div>
        </div>
        <div class="layer__content">
            <form action="/wp-admin/admin-ajax.php?action=wp_send_feedback" js-formus method="post" class="formus">
                <input type="hidden" js-formus-id name="order">
                <div class="formus__content formus__content_visible sections-list sections-list_row_xsmall">
                    <div class="sections-list__list">
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <input name="name" type="text" class="field__input" required placeholder="Имя">
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <input name="doc" type="number" required class="field__input" placeholder="Номер договора">
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <input name="email" type="email" required class="field__input" placeholder="Почта">
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <input js-phone js-phonemask
                                               data-is-inputmask="true" required
                                               placeholder="Телефон"
                                               type="phone" name="phone"
                                               class="field__input">
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <input name="social" type="text" required class="field__input" placeholder="Социальная сеть">
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <label js-inputShadow class="field field_dark">
                                        <textarea name="desc" class="field__input" placeholder="Отзыв" rows="6"> </textarea>
                                        <span class="field__error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small">
                            <div class="list-fields__item">
                                <div class="list-fields__item-main">
                                    <div js-rating class="rating"></div>
                                    <input js-input-rate name="rate" type="hidden" value="4"/>
                                </div>
                            </div>
                        </div>
                        <div class="sections-list__item sections-list__item_row-small sections-list__item_center">
                            <button class="button button_full" type="submit">
                                <span class="button__content">
                                    <span class="button__title">
                                        Отправить
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="formus formus__content formus__content_hidden sections-list sections-list_row_xsmall">
                    <div class="sections-list__list">
                        <div class="sections-list__item sections-list__item_row-small sections-list__item_center">
                            <p class="-align_center">
                                Спасибо за отзыв, после модерации он будет опубликован
                            </p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div js-overlayer-menu class="layer layer_menu" data-id="main/menu">
    <div class="layer__inside">
        <div class="layer__header">
            <div class="header-mobile header-mobile_visible block">
                <div class="header-mobile__wrapper">
                    <div class="header-mobile__main">
                        <a href="/" class="header-mobile__logo">
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/logo.svg"
                                 class="header-mobile__logo-img" alt="Логотип оригинал">
                        </a>
                    </div>
                    <div class="header-mobile__side">
                        <div class="header-mobile__actions">
                            <div class="header-mobile__actions-item">
                                <div js-office-addresses class="office-box">
                                    <div class="office-box__side">
                                        <a href="#" class="link link_small link_dashed">ОФИСЫ В БАРНАУЛЕ</a>
                                    </div>
                                    <div class="office-box__main">
                                        <div js-office-addresses-close class="office-box__close"></div>
                                        <div class="office-box__sections">
                                            <?php
                                            $offices = origin_offices();
                                            ?>
                                            <?php if ($offices): foreach ($offices as $office): ?>
                                                <div class="office-box__section">
                                                    <?php echo $office['address'] ?>
                                                    <?php if ($office['phone_main']): ?>
                                                        <a href="tel:83852<?php echo $office['phone_main_raw']; ?>">
                                                            (38-52)&nbsp;<?php echo $office['phone_main']; ?>
                                                        </a>
                                                    <?php endif; ?>
                                                    <?php if ($office['phone_addit']): ?>
                                                        <a href="tel:83852<?php echo $office['phone_addit_raw']; ?>">
                                                            (38-52)&nbsp;<?php echo $office['phone_addit']; ?>
                                                        </a>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach; endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header-mobile__actions-item">
                                <a href="tel:83852<?php echo origin_phone_raw_input_setting(); ?>"
                                   class="header-mobile__phone">
                                </a>
                            </div>
                            <div class="header-mobile__actions-item">
                                <div js-menu-close class="header-mobile__menu header-mobile__menu_close"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layer__content">
            <div class="block">
                <div class="header-middle header-middle_layer">
                    <div class="header-middle__wrapper">
                        <div class="header-middle__right">
                            <div class="header-middle__actions">
                                <div class="header-middle__actions-item">
                                    <a href="tel:tel:83852<?php echo origin_phone_raw_input_setting(); ?>"
                                       class="header-middle__phone">
                                        <sup class="code">(38-52)</sup> <?php echo origin_phone_input_setting(); ?>
                                    </a>
                                </div>
                                <div class="header-middle__action-item">
                                    <div class="header-middle__callback">
                                        <button js-question-button data-delay="true" class="button button_small">
                                    <span class="button__content">
                                        <span class="button__title">
                                            Задать вопрос
                                        </span>
                                    </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bottom header-bottom_layer">
                    <div class="header-bottom__wrapper">
                        <div class="header-bottom__nav">
                            <div class="header-bottom__nav-list">
                                <?php $topMenuLevel2 = origin_get_top_menu_level2(); ?>
                                <?php if ($topMenuLevel2): foreach ($topMenuLevel2 as $item): ?>
                                    <?php if ($item['childs']): ?>
                                        <div class="header-bottom__nav-item">
                                            <div class="header-bottom__toggle header-bottom__toggle_simple">
                                                <div class="header-bottom__toggle-side">
                                                    <?php echo $item['item']->title; ?>
                                                </div>
                                                <div class="header-bottom__toggle-main">
                                                    <div class="header-bottom__toggle-list">
                                                        <?php foreach ($item['childs'] as $child): ?>
                                                            <div class="header-bottom__toggle-item">
                                                                <a href="<?php echo $child->url; ?>"
                                                                   class="header-bottom__toggle-link">
                                                                    <?php echo $child->title; ?>
                                                                </a>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php else: ?>
                                        <div class="header-bottom__nav-item">
                                            <a href="<?php echo $item['item']->url; ?>"
                                               class="header-bottom__nav-link">
                                                <?php echo $item['item']->title; ?>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-top header-top_layer">
                    <div class="header-top__wrapper">
                        <div class="header-top__main">
                            <div class="header-top__nav">
                                <div class="header-top__nav-list">
                                    <?php $topMenuLevel1 = origin_get_top_menu_level1(); ?>
                                    <?php if ($topMenuLevel1): ?>
                                        <?php
                                        $last = count($topMenuLevel1) - 1;
                                        $index = -1;
                                        ?>
                                        <?php foreach ($topMenuLevel1 as $item): ?>
                                            <div class="header-top__nav-item">
                                                <a href="<?php echo $item->url; ?>" class="header-top__nav-link">
                                                    <?php if ($index == $last): ?>
                                                        <span class="icon"> ❖ </span>
                                                    <?php endif; ?>
                                                    <?php echo $item->title; ?>
                                                </a>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="header-top__side">
                            <div class="social">
                                <div class="social__wrapper">
                                    <div class="social__side">
                                        <div class="social__title">
                                            НАШИ СОЦСЕТИ:
                                        </div>
                                    </div>
                                    <div class="social__main">
                                        <div class="social__list">
                                            <?php
                                            $vk = origin_vk_input_setting();
                                            $inst = origin_instagram_input_setting();
                                            ?>
                                            <?php if ($vk || $inst): ?>
                                                <?php if ($vk): ?>
                                                    <div class="social__list-item">
                                                        <a href="<?php echo $vk; ?>" target="_blank"
                                                           class="social-badge social-badge_vk"></a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if ($inst): ?>
                                                    <div class="social__list-item">
                                                        <a href="<?php echo $inst; ?>" target="_blank"
                                                           class="social-badge social-badge_instagram"></a>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/wp-content/themes/origin/assets/js/constructor-vendor.js"></script>
<script src="/wp-content/themes/origin/assets/js/constructor-common.js"></script>
</body>
</html>
