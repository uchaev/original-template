<?php
/**
 * Главная страница
 */
get_header(); ?>
    <div class="list-sections">
        <div class="list-sections__list">
            <div class="list-sections__list-item list-sections__list-item_article">
                <div class="article">
                    <?php $image = get_field('page_image'); ?>
                    <div class="article__intro"
                         style="background-image: url(<?php echo($image ? $image['url'] : '') ?>)">
                        <div class="article__nav">
                            <div class="article__nav-side">
                                <div class="article__nav-title">
                                    <?php the_title(); ?>
                                </div>
                            </div>
                            <?php get_template_part('template-parts/content', 'breadcrumbs-articles'); ?>
                        </div>
                    </div>
                    <div class="article__content block">
                        <div class="article__content-wrapper block-wrapper">
                            <?php while (have_posts()) : the_post(); ?>
                                <?php the_content(); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
                <div class="list-sections__list-item">
                    <div class="info-cols info-cols_person info-cols_simple info-cols_bordered info-cols_padding-top_large block">
                        <div class="info-cols__wrapper block-wrapper">
                            <div class="info-cols__list">
                                <div class="info-cols__list-item info-cols__list-item_img">
                                    <div class="info-cols__img info-cols__img_small">
                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/woman.png" alt="">
                                    </div>
                                </div>
                                <div class="info-cols__list-item info-cols__list-item_desc">
                                    <div class="info-cols__major">
                                        <div class="info-cols__title">
                                            Помочь в выборе окна?
                                        </div>
                                    </div>
                                    <div class="info-cols__side">
                                        <div class="header-middle__actions">
                                            <div class="header-middle__action-item">
                                                <div class="header-middle__callback">
                                                    <button class="button">
                                                    <span class="button__content">
                                                        <span class="button__title">
                                                         Заказать консультацию
                                                        </span>
                                                    </span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="header-middle__actions-item">
                                                <a href="tel:83852<?php echo origin_phone_raw_input_setting(); ?>"
                                                   class="header-middle__phone">
                                                    <sup class="code">(38-52)</sup> <?php echo origin_phone_input_setting(); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-sections__list-item">
                    <div class="steps block">
                        <div class="steps__wrapper block-wrapper">
                            <div class="steps__title">
                                Разобраться самостоятельно
                            </div>
                            <div class="steps__desc">
                                Окно — технически сложная конструкция, поэтому самостоятельно разбраться в деталях и
                                сделать правильный выбор для большинства людей задача не простая. Мы попытались кратко и
                                понятно рассказать вам о каждом элементе окна, чтобы вы обладали основными знаниями и
                                чувствовали
                                себя уверенно.
                            </div>
                            <div class="steps__list">

                                <div class="steps__list-item">
                                    <div class="steps-item">
                                        <div class="steps-item__wrapper">
                                            <div class="steps-item__title">
                                                1.
                                            </div>
                                            <div class="steps-item__desc">
                                                <div class="steps-item__desc-caption">
                                                    Профиль
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="steps__list-item">
                                    <div class="steps-item">
                                        <div class="steps-item__wrapper">
                                            <div class="steps-item__title">
                                                2.
                                            </div>
                                            <div class="steps-item__desc">
                                                <div class="steps-item__desc-caption">
                                                    Стеклопакет
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="steps__list-item">
                                    <div class="steps-item">
                                        <div class="steps-item__wrapper">
                                            <div class="steps-item__title">
                                                3.
                                            </div>
                                            <div class="steps-item__desc">
                                                <div class="steps-item__desc-caption">
                                                    Фурнитура
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="steps__list-item">
                                    <div class="steps-item">
                                        <div class="steps-item__wrapper">
                                            <div class="steps-item__title">
                                                4.
                                            </div>
                                            <div class="steps-item__desc">
                                                <div class="steps-item__desc-caption">
                                                    Цвет
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="steps__list-item">
                                    <div class="steps-item">
                                        <div class="steps-item__wrapper">
                                            <div class="steps-item__title">
                                                5.
                                            </div>
                                            <div class="steps-item__desc">
                                                <div class="steps-item__desc-caption">
                                                    Расчет стоимости
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="steps__list-item">
                                    <div class="steps-item">
                                        <div class="steps-item__wrapper">
                                            <div class="steps-item__title">
                                                6.
                                            </div>
                                            <div class="steps-item__desc">
                                                <div class="steps-item__desc-caption">
                                                    Монтаж
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>