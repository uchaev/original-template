<?php
/**
 * Template Name: Шаблон страницы лендинга
 * @package wordpress
 * @subpackage origin
 * @since 1.0
 */
get_header() ?>
<div class="list-sections">
    <div class="list-sections__list">
        <?php $topSlider = get_field('landing_slider'); ?>
        <?php if ($topSlider): ?>
            <div class="list-sections__list-item list-sections__list-item_billboards list-sections__list-item_billboards_margin_large">
                <div class="block">
                    <div class="block-wrapper">
                        <div js-billboards class="billboards">
                            <div js-billboards-slider class="billboards__slider">

                                <div class="billboards__slider-additional block">
                                    <div class="billboards__slider-box block-wrapper">
                                        <div js-billboards-slider-nav class="billboards__slider-nav"></div>
                                        <div js-billboards-slider-dots class="billboards__slider-dots"></div>
                                    </div>
                                </div>

                                <div js-billboards-slider-list class="billboards__slider-list">
                                    <?php foreach ($topSlider as $slide): ?>
                                        <?php $title = $slide['landing_slider_title']; ?>
                                        <?php if ($title): ?>
                                            <?php /*echo $title */ ?>
                                        <?php endif; ?>
                                        <?php $desc = $slide['landing_slider_desc']; ?>
                                        <?php if ($desc): ?>
                                            <?php /*echo $desc */ ?>
                                        <?php endif; ?>
                                        <div class="billboards__slider-item">
                                            <a href="<?php echo $slide['landing_slider_url']; ?>"
                                               class="billboards__slider-img"
                                               style="background-image: url('<?php echo $slide['landing_slider_img']['url'] ?>')">
                                                <img src="<?php echo $slide['landing_slider_img']['url'] ?>">
                                            </a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="list-sections__list-item list-sections__list-item_billboards-info">
            <div class="info-cols info-cols_billboard">
                <div class="info-cols__wrapper">
                    <div class="info-cols__list">
                        <div class="info-cols__list-item info-cols__list-item_billboard-desc">
                            <div class="info-cols__box">
                                <div class="info-cols__main">
                                    <div class="info-cols__title">
                                        Завод «Оригинал» производит пластиковые окна более 15 лет
                                    </div>
                                </div>
                                <div class="info-cols__side">
                                    <div class="list-info list-info_billboards">
                                        <div class="list-info__list list-info__list_3x">
                                            <div class="list-info__list-item">
                                                <div class="list-info__item">
                                                    <div class="list-info__item-main">
                                                        <div class="list-info__item-title list-info__item-title_small">
                                                            45000+
                                                        </div>
                                                    </div>
                                                    <div class="list-info__item-side">
                                                        <div class="list-info__item-desc">
                                                            изготовлено окон в&nbsp;2002 года
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-info__list-item">
                                                <div class="list-info__item">
                                                    <div class="list-info__item-main">
                                                        <div class="list-info__item-title list-info__item-title_small">
                                                            8000+
                                                        </div>
                                                    </div>
                                                    <div class="list-info__item-side">
                                                        <div class="list-info__item-desc">
                                                            площадь производства
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-info__list-item">
                                                <div class="list-info__item">
                                                    <div class="list-info__item-main">
                                                        <div class="list-info__item-title list-info__item-title_small">
                                                            72 часа
                                                        </div>
                                                    </div>
                                                    <div class="list-info__item-side">
                                                        <div class="list-info__item-desc">
                                                            срок изготовления заказа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-info__list-item">
                                                <div class="list-info__item">
                                                    <div class="list-info__item-main">
                                                        <div class="list-info__item-title list-info__item-title_small">
                                                            4 города
                                                        </div>
                                                    </div>
                                                    <div class="list-info__item-side">
                                                        <div class="list-info__item-desc">
                                                            география присутствия
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-info__list-item">
                                                <div class="list-info__item">
                                                    <div class="list-info__item-main">
                                                        <div class="list-info__item-title list-info__item-title_small">
                                                            100%
                                                        </div>
                                                    </div>
                                                    <div class="list-info__item-side">
                                                        <div class="list-info__item-desc">
                                                            соответствие ГОСТу
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-info__list-item">
                                                <div class="list-info__item">
                                                    <div class="list-info__item-main">
                                                        <div class="list-info__item-title list-info__item-title_small">
                                                            1500 м²
                                                        </div>
                                                    </div>
                                                    <div class="list-info__item-side">
                                                        <div class="list-info__item-desc">
                                                            площадь производства
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $bottomSlider = get_field('landing_slider2'); ?>
                        <?php if ($bottomSlider): ?>
                            <div class="info-cols__list-item info-cols__list-item_billboard-img">
                                <div class="info-cols__billboard">
                                    <div js-billboards class="billboards">
                                        <div js-billboards-slider class="billboards__slider">
                                            <div class="billboards__slider-additional block">
                                                <div class="billboards__slider-box block-wrapper">
                                                    <div js-billboards-slider-nav
                                                         class="billboards__slider-nav"></div>
                                                    <div js-billboards-slider-dots
                                                         class="billboards__slider-dots"></div>
                                                </div>
                                            </div>
                                            <div js-billboards-slider-list class="billboards__slider-list">
                                                <?php foreach ($bottomSlider as $slide): ?>
                                                    <div class="billboards__slider-item">
                                                        <a href="<?php echo $slide['url']; ?>"
                                                           class="fixes billboards__slider-img"
                                                           style="background-image: url('<?php echo $slide['img']['url'] ?>')">
                                                            <img src="<?php echo $slide['img']['url'] ?>">
                                                        </a>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $profilesSlide = get_field('profiles_system_slide');
        $salamanderSlide = $profilesSlide['profile_salamander'];
        $gealanSlide = $profilesSlide['profile_gealan'];
        $exprofSlide = $profilesSlide['profile_exprof'];
        $orteksSlide = $profilesSlide['profile_orteks'];
        $profilesSlideTitle = $profilesSlide['profiles_title'];
        ?>

        <div class="list-sections__list-item list-sections__list-item_info-tabs">
            <div class="info-tabs block">
                <div class="info-tabs__wrapper block-wrapper">
                    <div class="info-tabs__side">
                        <div class="info-caption">
                            <div class="info-caption__title">
                                <h2>Большой выбор профильных систем</h2>
                            </div>
                            <div class="info-caption__desc">
                                <?php echo $profilesSlideTitle; ?>
                            </div>
                        </div>
                    </div>
                    <div class="info-tabs__main">
                        <div js-tabs class="tabs">
                            <div class="tabs__wrapper">
                                <div class="tabs__side">
                                    <div class="tabs__actions">
                                        <div class="tabs__actions-item">
                                            <div js-tab-action data-target="1" class="tabs__action is-active">
                                                <div class="tabs__action-wrapper"></div>
                                                <div class="tabs__action-icon">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/salamander.jpg"
                                                         alt="Salamander лого">
                                                </div>
                                                <div class="tabs__action-caption">Salamander</div>
                                            </div>
                                        </div>

                                        <div class="tabs__actions-item">
                                            <div js-tab-action data-target="2" class="tabs__action ">
                                                <div class="tabs__action-wrapper"></div>
                                                <div class="tabs__action-icon">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/gealan.png"
                                                         alt="Gealan лого">
                                                </div>
                                                <div class="tabs__action-caption">Gealan</div>
                                            </div>
                                        </div>

                                        <div class="tabs__actions-item">
                                            <div js-tab-action data-target="3" class="tabs__action ">
                                                <div class="tabs__action-wrapper"></div>
                                                <div class="tabs__action-icon">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/exprof.png"
                                                         alt="Exprof лого">
                                                </div>
                                                <div class="tabs__action-caption">Exprof</div>
                                            </div>
                                        </div>


                                        <div class="tabs__actions-item">
                                            <div js-tab-action data-target="4" class="tabs__action ">
                                                <div class="tabs__action-wrapper"></div>
                                                <div class="tabs__action-icon">
                                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/ortex.png"
                                                         alt="Ortex лого">
                                                </div>
                                                <div class="tabs__action-caption">Ortex</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs__main">
                                    <div class="tabs__pages">
                                        <div js-tab-page data-target="1" class="tabs__pages-item is-active">
                                            <div class="tabs__page">
                                                <div class="info-cols info-cols_tab">
                                                    <div class="info-cols__list">
                                                        <div class="info-cols__list-item">
                                                            <div class="info-cols__img info-cols__img_medium info-cols__img_shadow">
                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/profile-salamander.jpg" alt="Профиль Salamander">
                                                            </div>
                                                        </div>
                                                        <div class="info-cols__list-item">
                                                            <div class="info-cols__header">
                                                                <div class="info-cols__title info-cols__title_small">
                                                                    Salamander
                                                                </div>
                                                            </div>
                                                            <div class="info-cols__body">
                                                                <div class="info-cols__desc info-cols__desc_small">
                                                                    <?php echo $salamanderSlide; ?>
                                                                </div>
                                                            </div>
                                                            <div class="info-cols__footer">
                                                                <div class="list-info list-info_flex-start">
                                                                    <div class="list-info__list">
                                                                        <div class="list-info__list-item">
                                                                            <div class="list-info__item">
                                                                                <a href="#">
                                                                                    <div class="card-shadow">
                                                                                        <div class="card-shadow__title">
                                                                                            Streamline
                                                                                        </div>
                                                                                        <div class="card-shadow__desc">
                                                                                            5 камер, ширина 76мм
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="list-info__list-item">
                                                                            <div class="list-info__item">
                                                                                <a href="#">
                                                                                    <div class="card-shadow">

                                                                                        <div class="card-shadow__badge">
                                                                                            <div class="card-shadow__badge-icon">
                                                                                                ★
                                                                                            </div>
                                                                                            <div class="card-shadow__badge-caption">
                                                                                                Премиум
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="card-shadow__title">
                                                                                            BluEvolution
                                                                                        </div>
                                                                                        <div class="card-shadow__desc">
                                                                                            6 камер, ширина 92мм
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div js-tab-page data-target="2" class="tabs__pages-item ">
                                            <div class="tabs__page">
                                                <div class="info-cols info-cols_tab">
                                                    <div class="info-cols__list">
                                                        <div class="info-cols__list-item">
                                                            <div class="info-cols__img info-cols__img_medium info-cols__img_shadow">
                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/gealan_profile.jpg" alt="Профиль Gealan">
                                                            </div>
                                                        </div>
                                                        <div class="info-cols__list-item">
                                                            <div class="info-cols__header">
                                                                <div class="info-cols__title info-cols__title_small">
                                                                    Gealan
                                                                </div>
                                                            </div>
                                                            <div class="info-cols__body">
                                                                <div class="info-cols__desc info-cols__desc_small">
                                                                    <?php echo $gealanSlide; ?>
                                                                </div>
                                                            </div>
                                                            <div class="info-cols__footer">
                                                                <div class="list-info list-info_flex-start">
                                                                    <div class="list-info__list">

                                                                        <div class="list-info__list-item">
                                                                            <div class="list-info__item">
                                                                                <a href="#">
                                                                                    <div class="card-shadow">
                                                                                        <div class="card-shadow__title">
                                                                                            S8000 IQ
                                                                                        </div>
                                                                                        <div class="card-shadow__desc">
                                                                                            4 камеры, ширина 74мм
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div js-tab-page data-target="3" class="tabs__pages-item ">
                                            <div class="tabs__page">
                                                <div class="info-cols info-cols_tab">
                                                    <div class="info-cols__list">
                                                        <div class="info-cols__list-item">
                                                            <div class="info-cols__img info-cols__img_medium info-cols__img_shadow">
                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/exprof_profile.jpg" alt="Профиль Exprof">
                                                            </div>
                                                        </div>
                                                        <div class="info-cols__list-item">
                                                            <div class="info-cols__header">
                                                                <div class="info-cols__title info-cols__title_small">
                                                                    Exprof
                                                                </div>
                                                            </div>
                                                            <div class="info-cols__body">
                                                                <div class="info-cols__desc info-cols__desc_small">
                                                                    <?php echo $exprofSlide; ?>
                                                                </div>
                                                            </div>
                                                            <div class="info-cols__footer">
                                                                <div class="list-info list-info_flex-start">
                                                                    <div class="list-info__list">
                                                                        <div class="list-info__list-item">
                                                                            <div class="list-info__item">
                                                                                <a href="#">
                                                                                    <div class="card-shadow">
                                                                                        <div class="card-shadow__badge blue">
                                                                                            <div class="card-shadow__badge-caption">
                                                                                                Для дачи
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="card-shadow__title">
                                                                                            Prowin
                                                                                        </div>
                                                                                        <div class="card-shadow__desc">
                                                                                            3 камеры, ширина 58мм
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="list-info__list-item">
                                                                            <div class="list-info__item">
                                                                                <a href="#">
                                                                                    <div class="card-shadow">
                                                                                        <div class="card-shadow__title">
                                                                                            Profecta
                                                                                        </div>
                                                                                        <div class="card-shadow__desc">
                                                                                            5 камер, ширина 70мм
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="list-info__list-item">
                                                                            <div class="list-info__item">
                                                                                <a href="#">
                                                                                    <div class="card-shadow">
                                                                                        <div class="card-shadow__badge">
                                                                                            <div class="card-shadow__badge-icon">
                                                                                                ★
                                                                                            </div>
                                                                                            <div class="card-shadow__badge-caption">
                                                                                                Хит
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="card-shadow__title">
                                                                                            Experta
                                                                                        </div>
                                                                                        <div class="card-shadow__desc">
                                                                                            6 камер, ширина 70мм
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div js-tab-page data-target="4" class="tabs__pages-item ">
                                            <div class="tabs__page">
                                                <div class="info-cols info-cols_tab">
                                                    <div class="info-cols__list">
                                                        <div class="info-cols__list-item">
                                                            <div class="info-cols__img info-cols__img_medium info-cols__img_shadow">
                                                                <img src="<?php bloginfo('template_url'); ?>/assets/images/ortex_profile.jpg" alt="Профиль Ortex">
                                                            </div>
                                                        </div>
                                                        <div class="info-cols__list-item">
                                                            <div class="info-cols__header">
                                                                <div class="info-cols__title info-cols__title_small">
                                                                    Ortex
                                                                </div>
                                                            </div>
                                                            <div class="info-cols__body">
                                                                <div class="info-cols__desc info-cols__desc_small">
                                                                    <?php echo $orteksSlide; ?>
                                                                </div>
                                                            </div>
                                                            <div class="info-cols__footer">
                                                                <div class="list-info list-info_flex-start">
                                                                    <div class="list-info__list">
                                                                        <div class="list-info__list-item">
                                                                            <div class="list-info__item">
                                                                                <a href="">
                                                                                    <div class="card-shadow">
                                                                                        <div class="card-shadow__badge blue">
                                                                                            <div class="card-shadow__badge-caption">
                                                                                                Для дачи
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="card-shadow__title">
                                                                                            Classic
                                                                                        </div>
                                                                                        <div class="card-shadow__desc">
                                                                                            3 камеры, ширина 58мм
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="list-info__list-item">
                                                                            <div class="list-info__item">
                                                                                <a href="">
                                                                                    <div class="card-shadow">
                                                                                        <div class="card-shadow__title">
                                                                                            Optima
                                                                                        </div>
                                                                                        <div class="card-shadow__desc">
                                                                                            5 камер, ширина 70мм
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $priceLand = get_field('price_landing');
        $priceLandTitle = $priceLand['price_land_title'];
        $priceLandOne = $priceLand['price_land_one'];
        $priceLandTwo = $priceLand['price_land_two'];
        $priceLandThree = $priceLand['price_land_three'];
        ?>

        <div class="list-sections__list-item list-sections__list-item_showcase">
            <div class="showcase block">
                <div class="showcase__wrapper block-wrapper">
                    <div class="showcase__header">
                        <div class="info-caption">
                            <div class="info-caption__title">
                                <h2> Честные цены </h2>
                            </div>
                            <div class="info-caption__desc">
                                <?php echo $priceLandTitle; ?>
                            </div>
                        </div>
                    </div>
                    <div class="showcase__body">
                        <div class="showcase__list">
                            <div class="showcase__list-item">
                                <div class="showcase__item-side">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/showcase-1.jpg"
                                         class="showcase__item-img">
                                </div>
                                <div class="showcase__item-main">
                                    <div class="showcase__item-caption">
                                        от <?php echo $priceLandOne; ?> ₽
                                    </div>
                                </div>
                            </div>
                            <div class="showcase__list-item">
                                <div class="showcase__item-side">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/showcase-2.jpg"
                                         class="showcase__item-img">
                                </div>
                                <div class="showcase__item-main">
                                    <div class="showcase__item-caption">
                                        от <?php echo $priceLandTwo; ?> ₽
                                    </div>
                                </div>
                            </div>
                            <div class="showcase__list-item">
                                <div class="showcase__item-side">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/showcase-3.jpg"
                                         class="showcase__item-img">
                                </div>
                                <div class="showcase__item-main">
                                    <div class="showcase__item-caption">
                                        от <?php echo $priceLandThree; ?> ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $calcLink = get_field('landing_calc'); ?>
                    <?php if ($calcLink): ?>
                        <div class="showcase__footer">
                            <div class="showcase-calc">
                                <div class="showcase-calc__wrapper">
                                    <a href="<?php echo $calcLink; ?>" class="showcase-calc__link">
                                        Рассчитать стоимость онлайн
                                    </a>
                                </div>
                                <div class="showcase-calc__img"></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="list-sections__list-item list-sections__list-item_steklopaket">
            <div class="info-cols info-cols_steklopaket info-cols_bordered info-cols_padding-top_large block">
                <div class="info-cols__wrapper block-wrapper">
                    <div class="info-cols__list">
                        <div class="info-cols__list-item">
                            <div class="info-cols__img info-cols__img_small">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/steklopaket.jpg" alt="">
                            </div>
                        </div>
                        <div class="info-cols__list-item">
                            <div class="info-cols__major">
                                <h2>
                                    <a href="#" class="info-cols__title info-cols__title_bordered">
                                        Энергосберегающий стеклопакет
                                    </a>
                                </h2>
                            </div>
                            <div class="info-cols__main info-cols__main_margin-bottom">
                                <div class="info-cols__desc info-cols__desc_small">
                                    Специальное покрытие, которое с&nbsp;одной стороны отражает тепловое
                                    излучение от&nbsp;нагревательных приборов, внутрь помещения, не&nbsp;давая
                                    ему уйти наружу. С&nbsp;другой&nbsp;&mdash; свободно пропускает солнечную
                                    коротковолновую энергию в&nbsp;помещение.
                                    Данное покрытие позволяет сохранять до&nbsp;40&nbsp;% тепла квартиры.
                                    Нанесение покрытия на&nbsp;одно окно стоит около 400&nbsp;₽.
                                    Чаще всего в&nbsp;окнах используется двойной стеклопакет с&nbsp;ТОП
                                    покрытием. При этом толщина одного стекла составляет от&nbsp;4&nbsp;до&nbsp;6&nbsp;мм
                                    в&nbsp;зависимости от&nbsp;уровня шума на&nbsp;улице.
                                </div>
                            </div>
                            <div class="info-cols__side">
                                <div class="list-info">
                                    <div class="list-info__list list-info__list_2x">
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/tone.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Тонировка
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/armor.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Бронирование
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-sections__list-item list-sections__list-item_furnitura">
            <div class="info-cols info-cols_furnitura info-cols_bordered info-cols_padding-top_large block">
                <div class="info-cols__additional">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/furnitura-maco.png" alt="">
                </div>
                <div class="info-cols__wrapper block-wrapper">
                    <div class="info-cols__list">
                        <div class="info-cols__list-item">
                            <div class="info-cols__major">
                                <h2>
                                    <a href="#" class="info-cols__title info-cols__title_bordered">
                                        Фурнитура на 50 лет
                                    </a>
                                </h2>
                            </div>
                            <div class="info-cols__main info-cols__main_margin-bottom">
                                <div class="info-cols__desc info-cols__desc_small">
                                    Некачественная фурнитура доставляет много неудобств и может являться
                                    причиной деформации профиля и замене всего окна.
                                    Оригинал устанавливает выосококачественную австрийскую фурнитуру MACO, срок
                                    службы которой около 60 000 открываний или 50 лет.
                                </div>
                            </div>
                            <div class="info-cols__side">
                                <div class="list-info">
                                    <div class="list-info__list list-info__list_1x">
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper list-info__item-wrapper_flex-start">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/anti-burglary.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-group">
                                                        <div class="list-info__item-caption">
                                                            Противовзломная фурнитура
                                                        </div>
                                                        <div class="list-info__item-desc list-info__item-desc_max-width_large">
                                                            Установка дополнительных элементов, защищающих от&nbsp;грабителей
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper list-info__item-wrapper_flex-start">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/child.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-group">
                                                        <div class="list-info__item-caption">
                                                            Детский замок
                                                        </div>
                                                        <div class="list-info__item-desc list-info__item-desc_max-width_large">
                                                            Блокируется полное открывание, функция
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-sections__list-item list-sections__list-item_crystallit">
            <div class="info-cols info-cols_crystallit info-cols_bordered info-cols_padding-top_large block">
                <div class="info-cols__wrapper block-wrapper">
                    <div class="info-cols__additional info-cols__additional_tablet">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/crystallit.png" alt="">
                    </div>
                    <div class="info-cols__list">
                        <div class="info-cols__list-item"></div>
                        <div class="info-cols__list-item">
                            <div class="info-cols__major">
                                <h2>
                                    <a href="#" class="info-cols__title info-cols__title_bordered">
                                        Подоконники и откосы
                                    </a>
                                </h2>
                            </div>
                            <div class="info-cols__main info-cols__main_margin-bottom">
                                <div class="info-cols__desc info-cols__desc_small">
                                    Мы можем предложить вам на выбор обычный ПВХ-подоконник или подоконник
                                    Crystallit. Подоконники Crystallit — это качество премиум класса и самый
                                    широкий выбор декоров — матовые, глянцевые и с древесной фактурой.
                                </div>
                            </div>
                            <div class="info-cols__side">
                                <div class="list-info">
                                    <div class="list-info__list list-info__list_2x">
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/seventy.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Срок эксплуатации до 70 лет
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/sun.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Не выцветает на солнце
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/friction.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Стойкость к истиранию
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/strenght.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Высокая прочность
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/water.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Абсолютная влагостойкость
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/pb.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Не содержит свинца
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/f.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Не содержит формальдегида
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/temperature.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Повышенная термостойкость
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-sections__list-item list-sections__list-item_schema">
            <div class="schema block">
                <div class="schema__wrapper block-wrapper">
                    <div class="schema__content">
                        <div class="schema__title">
                            <div class="schema__title-caption">
                                <h2> Цвет, форма и декорирование </h2>
                            </div>
                        </div>
                        <div class="schema__img">
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/shema.png" alt="">
                        </div>
                        <div class="schema__list">
                            <div class="schema__info schema__info_1">
                                <div class="schema__info-item">
                                    <div class="schema__info-side">
                                        <div class="schema__info-title">Цветные окна</div>
                                    </div>
                                    <div class="schema__info-main">
                                        <div class="schema__info-desc">
                                            Нанесение декоративной ПВХ пленки на&nbsp;окно из&nbsp;любого
                                            профиля. Широкий выбор цвета и&nbsp;текстуры пленки.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schema__info schema__info_2">
                                <div class="schema__info-item">
                                    <div class="schema__info-side">
                                        <div class="schema__info-title">Декоративная раскладка</div>
                                    </div>
                                    <div class="schema__info-main">
                                        <div class="schema__info-desc">
                                            Декоративные накладки, размещенные внутри и&nbsp;снаружи
                                            стеклопакета, улучшающие архитектурный облик.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schema__info schema__info_3">
                                <div class="schema__info-item">
                                    <div class="schema__info-side">
                                        <div class="schema__info-title">Любая форма окна</div>
                                    </div>
                                    <div class="schema__info-main">
                                        <div class="schema__info-desc">
                                            Для реализации архитектурной идеи окну можно придать любую форму.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="schema__info schema__info_4">
                                <div class="schema__info-item">
                                    <div class="schema__info-side">
                                        <div class="schema__info-title">Аквапечать</div>
                                    </div>
                                    <div class="schema__info-main">
                                        <div class="schema__info-desc">
                                            Нанесение на&nbsp;предметы интерьера специальной пленки с&nbsp;рисунком
                                            дерева, металла и&nbsp;др.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $constLink = get_field('landing_constructor'); ?>
                    <?php if ($constLink): ?>
                        <div class="schema__action">
                            <a href="<?php echo $constLink; ?>" class="button">
                                <span class="button__content">
                                    <span class="button__title">
                                        Конструктор окна
                                    </span>
                                </span>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="list-sections__list-item list-sections__list-item_man">
            <div class="info-cols info-cols_man info-cols_padding-top_large block">
                <div class="info-cols__wrapper block-wrapper">
                    <div class="info-cols__list">
                        <div class="info-cols__list-item">
                            <div class="info-cols__img info-cols__img_small">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/man.png" alt="">
                            </div>
                        </div>
                        <div class="info-cols__list-item">
                            <div class="info-cols__major">
                                <h2>
                                    <a href="#" class="info-cols__title info-cols__title_bordered">
                                        Монтаж <br> от профессионалов
                                    </a>
                                </h2>
                            </div>
                            <div class="info-cols__main info-cols__main_margin-bottom">
                                <div class="info-cols__desc info-cols__desc_small">
                                    С некачественным монтажом даже самая дорогая комплектация окна не будет
                                    работать. А качество монтажа зависит от опыта специалистов. За 16 лет работы
                                    мы накопили большую базу знаний по установке окон, с учетом специфики домов,
                                    оконных проемов, размеров и видов
                                    окон и т.д. Молодые сотрудники проходят обязательное обучение и работают под
                                    наблюдением опытных коллег
                                </div>
                            </div>
                            <div class="info-cols__side">
                                <div class="list-info">
                                    <div class="list-info__list list-info__list_2x">
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/guarantee.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Гарантия на монтаж
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/specialists.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        Собственные специалисты
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/ten.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        10 лет — средний опыт монтажника
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list-info__list-item">
                                            <div class="list-info__item">
                                                <div class="list-info__item-wrapper">
                                                    <div class="list-info__item-icon">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/images/landing/2k.svg" alt="">
                                                    </div>
                                                    <div class="list-info__item-caption">
                                                        2000 проведенных монтажей
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-sections__list-item list-sections__list-item_map">
            <div class="map-box">
                <div js-map data-coords='<?php echo origin_office_positions(); ?>' class="map-box__main"></div>
                <div class="map-inbox block">
                    <div class="map-inbox__wrapper block-wrapper">
                        <div class="map-inbox__info">
                            <div class="map-card">
                                <div class="map-card__wrapper">
                                    <div class="map-card__header">
                                        <div class="map-card__title">
                                            Адреса филиалов
                                        </div>
                                    </div>
                                    <div class="map-card__body">
                                        <div class="map-card__main">
                                            <a href="tel:83852<?php echo origin_phone_raw_input_setting(); ?>"
                                               class="map-card__phone">
                                                <div class="map-card__phone-main">
                                                    (38-52)&nbsp;<?php echo origin_phone_input_setting(); ?>
                                                </div>
                                                <div class="map-card__phone-side">
                                                    Единая справочная служба
                                                </div>
                                            </a>
                                        </div>
                                        <?php
                                        $offices = origin_offices();
                                        ?>
                                        <?php if ($offices): ?>
                                            <div class="map-card__side">
                                                <div class="map-card__list">
                                                    <?php foreach ($offices as $office): ?>
                                                        <div class="map-card__list-item">
                                                            <div class="map-card__item">
                                                                <div class="map-card__item-section">
                                                                    <?php echo $office['address'] ?>
                                                                </div>
                                                                <?php if ($office['phone_main']): ?>
                                                                    <div class="map-card__item-section">
                                                                        (38-52)&nbsp;<?php echo $office['phone_main']; ?>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <?php if ($office['phone_addit']): ?>
                                                                    <div class="map-card__item-section">
                                                                        (38-52)&nbsp;<?php echo $office['phone_addit']; ?>
                                                                    </div>
                                                                <?php endif; ?>

                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="map-card__footer">
                                        <div class="map-card__actions">
                                            <div class="map-card__actions-item">
                                                <button js-question-button class="button button_full">
                                                        <span class="button__content">
                                                            <span class="button__title">
                                                                Заказать консультацию
                                                            </span>
                                                        </span>
                                                </button>
                                            </div>
                                            <?php $calcLink = get_field('landing_calc'); ?>
                                            <?php if ($calcLink): ?>
                                                <div class="map-card__actions-item">
                                                    <a href="<?php echo $calcLink; ?>" class="button button_full button_white">
                                                        <span class="button__content">
                                                            <span class="button__title">
                                                                 Рассчитать стоимость
                                                            </span>
                                                        </span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $feedbackLink = get_field('landing_feedback'); ?>
        <?php if ($feedbackLink): ?>
            <div class="list-sections__list-item list-sections__list-item_person">
                <div class="info-cols info-cols_person info-cols_padding-top_large block">
                    <div class="info-cols__wrapper block-wrapper">
                        <div class="info-cols__list">
                            <div class="info-cols__list-item info-cols__list-item_img">
                                <div class="info-cols__img info-cols__img_small">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/person.jpg"
                                         alt="">
                                </div>
                            </div>
                            <div class="info-cols__list-item info-cols__list-item_desc">
                                <div class="info-cols__major">
                                    <div class="info-cols__title info-cols__title_bordered">
                                        Рассмотрение отзывов <br> и рекламаций
                                    </div>
                                </div>
                                <div class="info-cols__main info-cols__main_margin-bottom">
                                    <div class="info-cols__desc info-cols__desc_small">
                                        <b>Иван Иванович Иванов, начальник отдела качества:</b>
                                        <br>
                                        «Мы всегда стремимся оказывать сервис высокого качества, но, как и все,
                                        можем ошибаться. Если вас что-то не удовлетворило в нашей работе,
                                        напишите
                                        рекламацию на сайте. Она будет рассмотрена мной в течение 3 дней, и вы
                                        гарантированно получите ответ».
                                    </div>
                                </div>
                                <div class="info-cols__side">
                                    <a href="<?php echo $feedbackLink; ?>" class="button">
                                            <span class="button__content">
                                                <span class="button__title">Смотреть отзывы</span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php $projectBlock = get_field('landing_projects'); ?>
        <?php if ($projectBlock['projects']): ?>
            <div class="list-sections__list-item list-sections__list-item_project">
                <div class="projects block">
                    <div class="projects__wrapper block-wrapper">
                        <div class="projects__title">
                            Выполненные проекты
                        </div>
                        <div class="projects__list">
                            <?php foreach ($projectBlock['projects'] as $item): ?>
                                <?php $project = $item['project']; ?>
                                <?php $image = get_field('project_image', $project->ID); ?>
                                <div class="projects__list-item <?php echo $item['selected'] ? 'projects__list-item_full' : '' ?>">
                                    <div class="projects__item">
                                        <div class="projects__item-main">
                                            <div class="projects__item-img">
                                                <img src="<?php echo $item['selected'] ? $image['url'] : $image['sizes']['medium_large'] ?>"
                                                     alt="<?php echo $image['alt'] ?>"
                                                     title="<?php echo $image['title'] ?>">
                                            </div>
                                        </div>
                                        <div class="projects__item-side">
                                            <a href="<?php echo get_permalink($project->ID) ?>"
                                               class="projects__item-link">
                                                <?php echo $project->post_title; ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php $url = $projectBlock['projects_url']; ?>
                        <?php if ($url): ?>
                            <div class="projects__action">
                                <div class="projects__action-btn">
                                    <a href="<?php echo $url; ?>" class="button button_blue button_outline">
                                                <span class="button__content">
                                                    <span class="button__title">
                                                        Смотреть все проекты
                                                    </span>
                                                </span>
                                    </a>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>
