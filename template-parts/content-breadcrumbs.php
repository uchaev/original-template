<?php
/**
 * Шаблон отображения хлебных крошек
 *
 * @package WordPress
 * @subpackage origin
 * @since 1.0
 */
?>
<?php $breadcrumbs = origin_get_breadcrumbs(get_the_ID()); ?>
<?php if ($breadcrumbs): ?>
    <?php foreach ($breadcrumbs as $breadcrumb): ?>
        <a href="<?php echo $breadcrumb['url']; ?>">
            <?php echo $breadcrumb['title']; ?>
        </a>
    <?php endforeach; ?>
    <a href="#">
        <?php echo get_the_title(); ?>
    </a>
<?php endif; ?>
