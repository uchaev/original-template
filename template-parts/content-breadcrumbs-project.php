<?php
/**
 * Шаблон отображения хлебных крошек
 *
 * @package WordPress
 * @subpackage origin
 * @since 1.0
 */
?>
<?php $breadcrumbs = origin_get_breadcrumbs_custom(get_the_ID()); ?>
<?php if ($breadcrumbs): ?>
    <div class="project__breadcrumbs">
        <div class="blu-crumbs blu-crumbs_black">
            <?php foreach ($breadcrumbs as $breadcrumb): $index++; ?>
                <a href="<?php echo $breadcrumb['url']; ?>" class="blu-crumbs__crumb">
                    <?php echo $breadcrumb['title']; ?>
                </a>
                <span class="blu-crumbs__delim">→</span>
            <?php endforeach; ?>
            <a class="blu-crumbs__crumb blu-crumbs__crumb_current">
                <?php the_title(); ?>
            </a>
        </div>
    </div>
<?php endif; ?>