<?php
/**
 * Шаблон отображения хлебных крошек
 *
 * @package WordPress
 * @subpackage origin
 * @since 1.0
 */
?>
<?php $breadcrumbs = origin_get_breadcrumbs(get_the_ID()); ?>
<?php if ($breadcrumbs): ?>
    <div class="article__nav-main">
        <div class="article__nav-list">
            <?php
            $last = count($breadcrumbs);
            $index = 0;
            ?>
            <?php foreach ($breadcrumbs as $breadcrumb): $index++; ?>
                <div class="article__nav-item <?php echo($last == $index ? 'last' : ''); ?>">
                    <a href="<?php echo $breadcrumb['url']; ?>" class="article__nav-link">
                        <?php echo $breadcrumb['title']; ?>
                    </a>
                </div>
            <?php endforeach; ?>
            <span class="article__nav-link">
                <?php the_title(); ?>
            </span>
        </div>
    </div>
<?php endif; ?>