<?php
/**
 * Шаблон отображения контента
 *
 * @package WordPress
 * @subpackage origin
 * @since 1.0
 */
?>
<?php while (have_posts()) : the_post(); ?>
    <?php the_content(); ?>
<?php endwhile;
wp_reset_query(); ?>